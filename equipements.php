<?php
	$titre_page = "Equipements" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');

	$NomEtab = $_SESSION['NomEtab'];
?>
<section>
	<div id="top_section" >
		<h1>Equipements<?php if ($_SESSION['NomEtab'] != NULL){echo ' - '.$NomEtab ;} else {} ; ?></h1>
	</div>
	
	<div id="content">

<br />
		<?php if ($_SESSION['Rang'] == 2) 
		{
				$equipement = $bdd->query('SELECT * FROM equipements WHERE RNE = "'.$_SESSION['RNE'].'"');
	$queryRNE = $bdd->query('SELECT RNE FROM equipements WHERE RNE = "'.$_SESSION['RNE'].'"'); 
	$count = $queryRNE->rowCount();  
	if($count == 1) 
		{	
		while($donnees = $equipement->fetch())
		{
	?>
	<input class="btn" type="submit" value="Modifier les données" onclick="self.location.href='modif_equip.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'">&nbsp;&nbsp;<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_equip_ce.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>
	<table>

	<tr><th><h4>Critères</h4></th><th colspan="2"><h4>Données</h4></th><th><h4>Nombre de points</h4></th></tr>

	<tr><th>Nombre de terminaux (ordinateur fixe, ordinateur portable, tablette)</th><td colspan="2"><?php echo $donnees['nb_terminaux_donnees']; ?></td><td><?php echo $donnees['nb_terminaux_points']; ?> sur 10</td></tr>

	<tr><th>Nombre de terminaux mobiles (ordinateur portable, tablette, poste de classes mobiles)</th><td colspan="2"><?php echo $donnees['nb_terminaux_mobiles_donnees']; ?></td><td><?php echo $donnees['nb_terminaux_mobiles_points']; ?> sur 20</td></tr>

	<tr><th>Nombre de postes de travail en accès libre aux élèves en dehors des cours (hors classe)</th><td colspan="2"><?php echo $donnees['nb_postes_libres_donnees']; ?></td><td><?php echo $donnees['nb_postes_libres_points']; ?> sur 20</td></tr>

	<tr><th>Nombre de terminaux de moins de cinq ans</th><td colspan="2"><?php echo $donnees['nb_terminaux_moins_5_ans_donnees']; ?></td><td><?php echo $donnees['nb_terminaux_moins_5_ans_points']; ?> sur 15</td></tr>

	<tr><th>Nombre de VPI/TNI/TBI</th><td colspan="2"><?php echo $donnees['nb_VPI_TNI_TBI_donnees']; ?></td><td><?php echo $donnees['nb_VPI_TNI_TBI_points']; ?> sur 15</td></tr>

	<tr><th>Nombre de vidéo-projecteurs</th><td colspan="2"><?php echo $donnees['nb_video_projecteur_donnees']; ?></td><td><?php echo $donnees['nb_video_projecteur_points']; ?> sur 5</td></tr>

	<tr><th>Dotation des élèves en terminaux mobiles (ordinateur portable, tablette) par la collectivité</th><td colspan="2"><?php echo $donnees['dotation_eleves_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees['dotation_eleves_terminaux_mobiles_points']; ?> sur 10</td></tr>

	<tr><th>Dotation des enseignants en terminaux mobiles (ordinateur portable, tablette) par la collectivité</th><td colspan="2"><?php echo $donnees['dotation_enseignants_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees['dotation_enseignants_terminaux_mobiles_points']; ?> sur 15</td></tr>

	<tr><th rowspan="7">Equipements particuliers</th><th>Malette MP3/MP4</th><td><?php echo $donnees['equipements_particuliers_malette']; ?></td><td rowspan="7"><?php echo $donnees['equipements_particuliers_points']; ?> sur 15</td></tr>
	<tr><th>Imprimante 3D</th><td><?php echo $donnees['equipements_particuliers_imprimante']; ?></td></tr>
	<tr><th>FabLab</th><td><?php echo $donnees['equipements_particuliers_fablab']; ?></td></tr>
	<tr><th>Visioconférence</th><td><?php echo $donnees['equipements_particuliers_visio']; ?></td></tr>
	<tr><th>Labo Multimédia</th><td><?php echo $donnees['equipements_particuliers_labo']; ?></td></tr>
	<tr><th>Boîtiers de vote</th><td><?php echo $donnees['equipements_particuliers_boitier']; ?></td></tr>
	<tr><th>Visualisateurs</th><td><?php echo $donnees['equipements_particuliers_visu']; ?></td></tr>

	<tr><th>Maintenance des équipements (maintien en condition opérationnelle) par la collectivité</th><td colspan="2"><?php echo $donnees['maintenance_equipement_mod']; ?></td><td><?php echo $donnees['maintenance_equipement_points']; ?> sur 20</td></tr>

	<tr><th>L'EPLE engage-til des moyens propres (budget, décharge, IMP, etc.) sur la maintenance quotidienne des équipements ?</th><td colspan="2"><?php echo $donnees['engagement_EPLE_mod']; ?></td><td><?php echo $donnees['engagement_EPLE_points']; ?> sur 6</td></tr>

	</table>

	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 151</td><td><?php echo $donnees['palier_equip'] ; ?> sur 10</td></tr>
	</table>


		<?php
	}
	 }
	 else {
	 	?><h2>Aucune donnée rentrée pour cet établissement !</h2>
	 	<input class="btn" type="submit" value="Insérer les données" onclick="self.location.href='insert_equip.php?RNE=<?php echo $_SESSION['RNE'] ; ?> '">
	 	
	 	<?php
	 }
		}
	else
	{

if (empty($_GET['RNE']) AND (empty($_POST['RNE'])))
{
?> <h3>Veuillez tout d'abord choisir un établissement sur la page d'accueil</h3> <?php
}
	else
	{
		$RNE = $_GET['RNE'];
		
	$equipement = $bdd->query('SELECT * FROM equipements WHERE RNE = "'.$RNE.'"');
	$queryRNE = $bdd->query('SELECT RNE FROM equipements WHERE RNE = "'.$RNE.'"'); 
	$count = $queryRNE->rowCount();  
	if($count == 1) 
		{	
		while($donnees = $equipement->fetch())
		{
	?>
	<input class="btn" type="submit" value="Modifier les données" onclick="self.location.href='modif_equip.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'">&nbsp;&nbsp;<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_equip.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>
	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Modalité retenue</h4></th><th><h4>Nombre de points</h4></th></tr>

			<tr><th>Nombre moyen d'élèves par terminal</th><td><?php echo $donnees['nb_terminaux_mod']; ?></td><td><?php echo $donnees['nb_terminaux_points']; ?> sur 10</td></tr>

			<tr><th>Nombre moyen d'élèves par terminal mobile</th><td><?php echo $donnees['nb_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees['nb_terminaux_mobiles_points']; ?> sur 20</td></tr>

			<tr><th>Nombre moyen d'élèves par poste de travail en accès libre aux élèves en dehors des heures de cours</th><td><?php echo $donnees['nb_postes_libres_mod']; ?></td><td><?php echo $donnees['nb_postes_libres_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de terminaux de moins de cinq ans</th><td><?php echo $donnees['nb_terminaux_moins_5_ans_mod']; ?>  </td><td><?php echo $donnees['nb_terminaux_moins_5_ans_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de salles d'enseignement équipées d'un VPI/TNI/TBI</th><td><?php echo $donnees['nb_VPI_TNI_TBI_mod']; ?></td><td><?php echo $donnees['nb_VPI_TNI_TBI_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de salles d'enseignement équipées d'un vidéo-projecteur</th><td><?php echo $donnees['nb_video_projecteur_mod']; ?></td><td><?php echo $donnees['nb_video_projecteur_points']; ?> sur 5</td></tr>
			
			<tr><th>Dotation des élèves en terminaux mobiles par la collectivité</th><td><?php echo $donnees['dotation_eleves_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees['dotation_eleves_terminaux_mobiles_points']; ?> sur 10</td></tr>

			<tr><th>Dotation des enseignants en terminaux mobiles par la collectivité</th><td><?php echo $donnees['dotation_enseignants_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees['dotation_enseignants_terminaux_mobiles_points']; ?> sur 15</td></tr>

			<tr><th>Équipements particuliers</th><td>Voir tableau suivant</td><td><?php echo $donnees['equipements_particuliers_points']; ?> sur 15</td></tr>

			<tr><th>Maintenance des équipements par la collectivité</th><td><?php echo $donnees['maintenance_equipement_mod']; ?></td><td><?php echo $donnees['maintenance_equipement_points']; ?> sur 20</td></tr>
			
			<tr><th>L'EPLE engage-t-il des moyens propres sur la maintenance quotidienne des équipements ?</th><td><?php echo $donnees['engagement_EPLE_mod']; ?></td><td><?php echo $donnees['engagement_EPLE_points']; ?> sur 6</td></tr>
	</table>

	<table class="tabledouble">
	<tr>
	<td class="tabledouble">
	<table>
		<tr><th><h4>Equipements particuliers</h4></th><th><h4>Données</h4></th></tr>
		<tr><th>Malette MP3/ MP4</th><td><?php echo $donnees['equipements_particuliers_malette'] ; ?></td></tr>
		<tr><th>Imprimante 3D</th><td><?php echo $donnees['equipements_particuliers_imprimante'] ; ?></td></tr>
		<tr><th>FabLab</th><td><?php echo $donnees['equipements_particuliers_fablab'] ; ?></td></tr>
		<tr><th>Visioconférence</th><td><?php echo $donnees['equipements_particuliers_visio'] ; ?></td></tr>
		<tr><th>Labo Multimédia</th><td><?php echo $donnees['equipements_particuliers_labo'] ; ?></td></tr>
		<tr><th>Boîtiers de vote</th><td><?php echo $donnees['equipements_particuliers_boitier'] ; ?></td></tr>
		<tr><th>Visualisateurs</th><td><?php echo $donnees['equipements_particuliers_visu'] ; ?></td></tr>
	</table>
	</td>
	<td class="tabledouble">
	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 151</td><td><?php echo $donnees['palier_equip'] ; ?> sur 10</td></tr>
	</table>
	</td>
	</tr>
	</table>


		<?php
		}
		?>
<?php }
else
{
	?><h2>Aucune notation effectuée sur cet établissement</h2><br>
	<?php
}
}
	}
?>
 	</div>
</section>
<br>
<?php
	include('pied_de_page.php');
	?>