<?php
	$titre_page = "Nouvel établissement" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');
?>

<div id="contenu">
<section>
	<div id="top_section" >
		<h1>Nouvel établissement</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	
	<div id="content">
	<h2>Veuillez renseigner les informations pour le nouvel établissement</h2>
	<form method="post" action="para_new_etab_php.php">
		<table>
			<tr><th>RNE</th><td><input class="text" type="text" name="rne" /></td></tr>
			<tr><th>Nom</th><td><input class="text" type="text" name="nom" /></td></tr>
			<tr><th>Ville</th><td><input class="text" type="text" name="ville" /></td></tr>
			<tr><th>Type</th><td>
				<select name="type">
					<option selected="selected" value="1">Lycée</option>
					<option value="2">Collège</option>
				</select>
			</td></tr>
			<tr><th>Statut</th><td>
				<select name="statut">
					<option selected="selected" value="1">Public</option>
					<option value="2">Privé</option>
				</select>
			</td></tr>
			<tr><th>Département</th><td>
				<select name="dpt">
					<option value="53">Mayenne</option>
					<option value="44">Loire-Atlantique</option>
					<option value="49">Maine-et-Loire</option>
					<option selected="selected" value="72">Sarthe</option>
					<option value="85">Vendée</option>
				</select>
			</td></tr>
			<tr><th>Nombre d'élèves</th><td><input class="text" type="number" name="nb_eleve" /></td></tr>
			<tr><th>Nombre d'enseignants</th><td><input class="text" type="number" name="nb_ens" /></td></tr>
			<tr><th>Nombre de salles d'enseignements</th><td><input class="text" type="number" name="nb_salle" /></td></tr>
			<tr><th>Adresse WEB de l'établissement</th><td><input class="text" type="url" name="url" /></td></tr>
		</table>
	<input class="btn" type="submit" value="Valider" />
</form>
</div>
</section>
</div>