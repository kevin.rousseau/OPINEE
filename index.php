<?php
	$titre_page = "Index" ;
	$force_erreur = 1 ;
	include('header.php');
	include('en_tete.php');
	
	$error = 0;
	$msg_error = "";
	if(isset($_POST['pseudo']) and isset($_POST['mdp']))							
	{
		if(empty($_POST['pseudo']) or empty($_POST['mdp']))								//On vérifie si les champs sont remplis
		{
			$error = 1;
			$msg_error = "Vous n'avez pas rempli tout les champs, veuillez réessayer" ;
		}
		else																			//On accède à la BDD pour les dernières
		{																				//vérifications
			$pseudo = $_POST['pseudo'];
			$mdp = $_POST['mdp'] ;
			
		if($_POST['connexion_spe'] == 1)
		{
		
			$requete1 = $bdd->query('SELECT * FROM admin WHERE pseudoAdmin = "'.$pseudo.'"') ;
			$donnees1 = $requete1->fetch() ;
		
			if($donnees1['mdpAdmin'] == $mdp)											//Le mot de passe doit correspondre à l'identifiant
			{																				
				$_SESSION['Prenom'] = $donnees1['prenomAdmin']	;					//On initialise les variables sessions
				$_SESSION['Nom'] = $donnees1['nomAdmin'] ;								
				$_SESSION['PseudoAdmin'] = $donnees1['pseudoAdmin'] ;
				$_SESSION['Rang'] = $donnees1['IDrang'] ;
				$_SESSION['RNE'] = NULL ;
				$_SESSION['NomEtab'] = NULL ;
				$_SESSION['date_modif'] = NULL ;
				$_SESSION['co'] = NULL ;
				
			}
			else																		//Sinon erreur
			{
				$error = 1;
				$msg_error = "L'identifiant ou le mot de passe est incorrect." ;
			}
		}

		elseif($_POST['connexion_spe'] == 2 )
		{
			$requete1 = $bdd->query('SELECT * FROM ce WHERE pseudoCE = "'.$pseudo.'"') ;
			$donnees1 = $requete1->fetch() ;
			$requete2 = $bdd->query('SELECT * FROM etablissements WHERE RNE = ( SELECT RNE FROM ce WHERE pseudoCE = "'.$pseudo.'")') ;
			$donnees2 = $requete2->fetch() ;
		
			if($donnees1['mdpCE'] == $mdp)											//Le mot de passe doit correspondre à l'identifiant
			{
				$_SESSION['Prenom'] = $donnees1['prenomCE'] ;						//On initialise les variables sessions
				$_SESSION['Nom'] = $donnees1['nomCE'] ;
				$_SESSION['RNE'] = $donnees1['RNE'] ;
				$_SESSION['PseudoCE'] = $donnees1['pseudoCE'] ;
				$_SESSION['Rang'] = $donnees1['IDrang'] ;
				$_SESSION['NomEtab'] = $donnees2['nom'] ;
				$_SESSION['co'] = NULL ;
			}
			else																		//Sinon erreur
			{
				$error = 1;
				$msg_error = "L'identifiant ou le mot de passe est incorrect." ;
			}
		}
		elseif($_POST['connexion_spe'] == 3)
		{
		
		$requete1 = $bdd->query('SELECT * FROM DAN WHERE pseudoDAN = "'.$pseudo.'"') ;
			$donnees1 = $requete1->fetch() ;
		
			if($donnees1['mdpDAN'] == $mdp)											
			{
				$_SESSION['Prenom'] = $donnees1['prenomDan']	;					//On initialise les variables sessions
				$_SESSION['Nom'] = $donnees1['nomDan'] ;	
				$_SESSION['PseudoDAN'] = $donnees1['pseudoDAN'] ;
				$_SESSION['Rang'] = $donnees1['IDrang'] ;
				$_SESSION['RNE'] = NULL ;
				$_SESSION['NomEtab'] = NULL ;
				$_SESSION['date_modif'] = NULL ;
				$_SESSION['co'] = NULL ;
			}
			else																		
			{
				$error = 1;
				$msg_error = "L'identifiant ou le mot de passe est incorrect." ;
			}
		
		}
		elseif($_POST['connexion_spe'] == 4 )
		{
			$requete1 = $bdd->query('SELECT * FROM CI WHERE pseudoCI = "'.$pseudo.'"') ;
			$donnees1 = $requete1->fetch() ;
		
			if($donnees1['mdpCI'] == $mdp)											//Le mot de passe doit correspondre à l'identifiant
			{
				$_SESSION['Prenom'] = $donnees1['prenomCI']	;					//On initialise les variables sessions
				$_SESSION['Nom'] = $donnees1['nomCI'] ;	
				$_SESSION['PseudoCI'] = $donnees1['pseudoCI'] ;
				$_SESSION['Rang'] = $donnees1['IDrang'] ;
				$_SESSION['RNE'] = NULL ;
				$_SESSION['NomEtab'] = NULL ;
				$_SESSION['date_modif'] = NULL ;
				$_SESSION['co'] = NULL ;
			}
			else																		//Sinon erreur
			{
				$error = 1;
				$msg_error = "L'identifiant ou le mot de passe est incorrect." ;
			}
		}
	}
}
?>

<div id="contenu">
<section>

	<div id="top_section" >
		<h1>Connexion</h1>
	</div>
	
	<div id="content">
		<?php
		
		if((!isset($_POST['pseudo']) and !isset($_POST['mdp'])) OR $error == 1)
		{
		?>
			<form action="index.php" method="post" class="connexion">
		
			<h3>Veuillez saisir vos identifiants</h3>
		
			<table class="connexion">
				<tr>
			<td><p><label for="pseudo">Identifiant : </td><td><input class="text" type="text" name="pseudo"/></p></td></tr>
			<tr><td><p><label for="mdp">Mot de passe : </td><td><input class="text" type="password" name="mdp" /></p></td></tr>
			</table>
				
			<p><select name="connexion_spe">
					<option value="1">Administrateur</option>
					<option value="2">Chef d'etablissement</option>
					<option value="3">DAN</option>
					<option value="4">CI</option>
				</select></p><br /><br />
			<p class="submit"><input class="btn" type="submit" value="Se connecter" /></p><br><br>
		
			</form>
		<?php
			if($error == 1)
			{
		?>

				<div id="msg_error">
					<?php echo '<p>'.$msg_error.'</p>' ; ?>
				</div>
		
		<?php
			}
		}
		elseif($error == 0 and isset($_POST['pseudo']) and isset($_POST['mdp']))
		{
		?>			
			<div id="msg_correct">
				<p><h2>Bienvenue sur le site de notation des etablissements</h2>
				</p>
			</div>
			
		<?php
		
		header('refresh:3;url=indexco.php');
		}
		?>
		
	</div>

</section>
</div>
<?php
	include('pied_de_page.php');
	?>