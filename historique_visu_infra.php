<?php
	$titre_page = "Historique_visu_infra" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
?>

<section>
	<div id="top_section" >
		<h1>Comparaison des données</h1>
	</div>
	
	<div id="content">

<?php 

	$date = $_SESSION['date_modif'];

		$hist = $bdd->query('SELECT * FROM historic WHERE RNE = "'.$_SESSION['RNE'].'" AND date_modif = "'.$date.'"');
		$new = $bdd->query('SELECT * FROM Infrastructures WHERE RNE = "'.$_SESSION['RNE'].'"');

	while($donnees = $new->fetch())
				{


		while($donnees1 = $hist->fetch())
				{
					
?>

<h3><a HREF="Historique_visu">Equipements</a>&nbsp;&nbsp;&nbsp;&nbsp;Infrastructures&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_serv">Services</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_pilo">Pilotage</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_form">Formation</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="historique_visu_uti">Utilisations</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="historique_visu_usa">Usages</a></h3>

		<table>
			<tr><th><h4>Critères</h4></th><th><h4>Ancienne modalité</h4></th><th><h4>Nouvelle modalité</h4></th><th><h4>Ancien nombre de points</h4></th><th><h4>Nouveau nombre de points</h4></th></tr>

			<tr><th>Proportion de salles d'enseignements avec au moins un accès réseau</th><td><?php echo $donnees1['proportion_acces_reseau_salles_mod']; ?></td><td><?php echo $donnees['proportion_acces_reseau_salles_mod']; ?></td><td><?php echo $donnees1['proportion_acces_reseau_salles_points']; ?> sur 20</td><td><?php echo $donnees['proportion_acces_reseau_salles_points']; ?> sur 20</td></tr>

			<tr><th>Débit d'accès à Internet</th><td><?php echo $donnees1['debit_acces_internet_mod']; ?></td><td><?php echo $donnees['debit_acces_internet_mod']; ?></td><td><?php echo $donnees1['debit_acces_internet_points']; ?> sur 20</td><td><?php echo $donnees['debit_acces_internet_points']; ?> sur 20</td></tr>

			<tr><th>Débit du réseau interne</th><td><?php echo $donnees1['debit_reseau_interne_mod']; ?></td><td><?php echo $donnees['debit_reseau_interne_mod']; ?></td><td><?php echo $donnees1['debit_reseau_interne_points']; ?> sur 20</td><td><?php echo $donnees['debit_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la qualité du débit Internet par rapport aux besoins</th><td><?php echo $donnees1['evaluation_qualite_debit_internet_mod']; ?></td><td><?php echo $donnees['evaluation_qualite_debit_internet_mod']; ?></td><td><?php echo $donnees1['evaluation_qualite_debit_internet_points']; ?> sur 20</td><td><?php echo $donnees['evaluation_qualite_debit_internet_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la qualité du débit du réseau interne par rapport aux besoins</th><td><?php echo $donnees1['evaluation_qualite_debit_reseau_interne_mod']; ?></td><td><?php echo $donnees['evaluation_qualite_debit_reseau_interne_mod']; ?></td><td><?php echo $donnees1['evaluation_qualite_debit_reseau_interne_points']; ?> sur 20</td><td><?php echo $donnees['evaluation_qualite_debit_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th>Réseau pédagogique</th><td colspan="2">Voir tableau suivant</td><td><?php echo $donnees1['reseau_pedagogique_points']; ?> sur 20</td><td><?php echo $donnees['reseau_pedagogique_points']; ?> sur 20</td></tr>

			<tr><th>Proportion des espaces (hors salle de classe) couverts par WiFi</th><td><?php echo $donnees1['proportion_wifi_mod']; ?></td><td><?php echo $donnees['proportion_wifi_mod']; ?></td><td><?php echo $donnees1['proportion_wifi_points']; ?> sur 10</td><td><?php echo $donnees['proportion_wifi_points']; ?> sur 10</td></tr>

			<tr><th>Proportion de salles d'enseignement couvertes par un réseau WiFi</th><td><?php echo $donnees1['proportion_wifi_salles_enseignements_mod']; ?></td><td><?php echo $donnees['proportion_wifi_salles_enseignements_mod']; ?></td><td><?php echo $donnees1['proportion_wifi_salles_enseignements_points']; ?> sur 20</td><td><?php echo $donnees['proportion_wifi_salles_enseignements_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la couverture du réseau WiFi par rapport aux besoins</th><td><?php echo $donnees1['evaluation_reseau_wifi_mod']; ?></td><td><?php echo $donnees['evaluation_reseau_wifi_mod']; ?></td><td><?php echo $donnees1['evaluation_reseau_wifi_points']; ?> sur 10</td><td><?php echo $donnees['evaluation_reseau_wifi_points']; ?> sur 10</td></tr>
			
			<tr><th>Possibilité pour les utilisateurs de recharger les équipements mobiles</th><td><?php echo $donnees1['possibilite_recharger_equipements_mobiles_mod']; ?></td><td><?php echo $donnees['possibilite_recharger_equipements_mobiles_mod']; ?></td><td><?php echo $donnees1['possibilite_recharger_equipements_mobiles_points']; ?> sur 10</td><td><?php echo $donnees['possibilite_recharger_equipements_mobiles_points']; ?> sur 10</td></tr>
	</table>
	
	<table>
		<tr><th><h4>Réseau pédagogique</h4></th><th><h4>Ancienne données</h4></th><th><h4>Nouvelle données</h4></th></tr>
		<tr><th>Pare-feu</th><td><?php echo $donnees1['reseau_pedagogique_pare_feu'] ; ?></td><td><?php echo $donnees['reseau_pedagogique_pare_feu'] ; ?></td></tr>
		<tr><th>Antivirus</th><td><?php echo $donnees1['reseau_pedagogique_antivirus'] ; ?></td><td><?php echo $donnees['reseau_pedagogique_antivirus'] ; ?></td></tr>
		<tr><th>Filtrage</th><td><?php echo $donnees1['reseau_pedagogique_filtrage'] ; ?></td><td><?php echo $donnees['reseau_pedagogique_filtrage'] ; ?></td></tr>
		<tr><th>Contrôle a posteriori des accès</th><td><?php echo $donnees1['reseau_pedagogique_controle'] ; ?></td><td><?php echo $donnees['reseau_pedagogique_controle'] ; ?></td></tr>
	</table>

	<table>
			<tr><th><h4>Ancien nombre de points</h4></th><th><h4>Nouveau nombre de points</h4></th><th><h4>Ancien palier</h4></th><th><h4>Nouveau palier</h4></th></tr>
			<tr><td><?php echo $donnees1['nb_points_total']; ?> sur 170</td><td><?php echo $donnees['nb_points_total']; ?> sur 170</td><td><?php echo $donnees1['palier_infra'] ; ?> sur 10</td><td><?php echo $donnees['palier_infra'] ; ?> sur 10</td></tr>
	</table>

			<?php
			}
		}
		?>			
</div>
</section>
<?php
include('pied_de_page.php');
?>