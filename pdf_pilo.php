<?php
	ob_start() ;
	include('header.php');
	
	$requete1 = $bdd->query('SELECT * FROM pilotage WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$NomEtab = $_SESSION['NomEtab'];

setlocale(LC_TIME, 'fra_fra');
date_default_timezone_set('Europe/Paris');
?>
	<style>
img.img_gauche
{
	width: 25% ;
	height:80%;
	float: left ;
}
		
img.img_droite
{
	width:20% ;
	height:90%;
	float: right ;
	margin-top: -15 ;
}
		

h2
{
	color : #02A4E4;
	text-align:center;
	margin-top: 0px;
}

h3
{
	color: #E54986 ;
	text-align:center;			
}
		
h4
{
	color : #4A51A9;
    margin-bottom: 3px;
    margin-top: 3px;
    text-align:center;
}

h5
{
	color : #4A51A9;
	position: absolute;
	bottom : 13px;
	text-align:right; 
}

.top
{
	height: 50px ;
}

.corps
{
		width: 95%;
}
		
table
{
	border: 1px #4A51A9 solid;
	border-collapse : collapse ;
	width: 79%;
	text-align:center;
}

th
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:78%;
	margin-top: -10px;
}

th.mod
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:30%;
}

th.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:19%;
}

th.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:90%;
}

th.donnees
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:45%;
}

th.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:100%;
}

th.part_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:105%;
}

th.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:40%;
}

th.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:10%;
}

th.domaine
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

td
{
	border: 1px #4A51A9 solid ;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnees
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:45%;
}

td.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:10%;
}

td.vide
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:7%;
}

td.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:80%;
}

td.part_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:90%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:90%;
}

td.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:10%;
}

	</style>
	<page backtop="70px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Pilotage - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th><h4>Critères</h4></th>
						<th class="mod"><h4>Modalité retenue</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete1->fetch())
			{
		?>

			<tr><th>Volet numérique au sein du projet d'établissement</th><td><?php echo $donnees['volet_numerique_projet_mod']; ?></td><td class="points"><?php echo $donnees['volet_numerique_projet_points']; ?> sur 10</td></tr>
			
			<tr><th>Charte d'usage du numérique</th><td><?php echo $donnees['charte_usage_numerique_mod']; ?></td><td class="points"><?php echo $donnees['charte_usage_numerique_points']; ?> sur 20</td></tr>
			
			<tr><th>Commissions numérique ou conseil pédagogique traitant des sujets numériques</th><td><?php echo $donnees['commission_numerique_mod']; ?></td><td class="points"><?php echo $donnees['commission_numerique_points']; ?> sur 20</td></tr>
			
			<tr><th>Personnel(s) référent(s) numérique(s) pour les usages pédagogiques du numérique</th><td><?php echo $donnees['personnels_referents_numeriques_usages_pedagogiques_mod']; ?></td><td class="points"><?php echo $donnees['personnels_referents_numeriques_usages_pedagogiques_points']; ?> sur 15</td></tr>
			
			<tr><th>Utilisation d'Indemnités de Mission Particulière (IMP) pour favoriser les usages du numérique</th><td><?php echo $donnees['utilisation_IMP_usages_numeriques_mod']; ?></td><td class="points"><?php echo $donnees['utilisation_IMP_usages_numeriques_points']; ?> sur 10</td></tr>
			
			<tr><th>Démarche "zéro papier inutile"</th><td><?php echo $donnees['demarche_zero_papier_inutile_mod']; ?></td><td class="points"><?php echo $donnees['demarche_zero_papier_inutile_points']; ?> sur 10</td></tr>
			
			<tr><th>Usage privilégié du numérique par l'équipe de direction et l'équipe administrative</th><td><?php echo $donnees['usage_privilegie_numerique_direction_administration_mod']; ?></td><td class="points"><?php echo $donnees['usage_privilegie_numerique_direction_administration_points']; ?> sur 10</td></tr>
			
			<tr><th>Équipements numériques personnels des professeurs acceptés sur le réseau de l'établissement</th><td><?php echo $donnees['equipements_numeriques_personnels_enseignants_acceptes_mod']; ?></td><td class="points"><?php echo $donnees['equipements_numeriques_personnels_enseignants_acceptes_points']; ?> sur 20</td></tr>
			
			<tr><th>Équipements numériques personnels des élèves acceptés (pour des usages pédagogiques)<br>sur le réseau de l'établissement</th><td><?php echo $donnees['equipements_numeriques_personnels_eleves_acceptes_mod']; ?></td><td class="points"><?php echo $donnees['equipements_numeriques_personnels_eleves_acceptes_points']; ?> sur 5</td></tr>
			
			<tr><th>Sécurité - Identifiants personnels d'accès au réseau</th><td><?php echo $donnees['securite_id_personnels_acces_reseau_mod']; ?></td><td class="points"><?php echo $donnees['securite_id_personnels_acces_reseau_points']; ?> sur 20</td></tr>

			<tr><th>Actions d'information et d'accompagnement des usagers au numérique</th><td>Voir tableau suivant</td><td class="points"><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_points']; ?> sur 18</td></tr>
		
			<tr><th>Assistance aux usagers</th><td>Voir tableau suivant</td><td class="points"><?php echo $donnees['assistance_usagers_points']; ?> sur 18</td></tr>
			
			<tr><th>Suivi statistique des taux d'utilisation des équipements et des services</th><td><?php echo $donnees['suivi_statistique_taux_utilisation_equipements_services_mod']; ?></td><td class="points"><?php echo $donnees['suivi_statistique_taux_utilisation_equipements_services_points']; ?> sur 10</td></tr>
			
			<tr><th>Mise en place et suivi des "Services en Ligne"</th><td><?php echo $donnees['mise_en_place_suivi_services_en_ligne_mod']; ?></td><td class="points"><?php echo $donnees['mise_en_place_suivi_services_en_ligne_points']; ?> sur 20</td></tr>
			
			<tr><th>Échanges entre le chef d'établissement et les corps d'inspection<br>pour le développement des usages du numérique</th><td><?php echo $donnees['echanges_chef_etablissement_inspection_developpement_mod']; ?></td><td class="points"><?php echo $donnees['echanges_chef_etablissement_inspection_developpement_points']; ?> sur 15</td></tr>

				</tbody>
			</table>
			<br>

			<table align="center" border="none">
			<tr>
			<td border="none">
				<table>
					<thead>
						<tr>
							<th class="part_pilo"><h4>Actions d'information et d'accompagnement des usagers au numérique</h4></th><th class="donnees"><h4>Données</h4></th>
						</tr>
					</thead>
					<tbody>
						<tr><th class="part_pilo">Avec les moyens de l'établissement (personnel référent)</th><td class="donnees"><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_etab']; ?></td></tr>
						<tr><th class="part_pilo">Par les services académiques</th><td class="donnees"><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_aca']; ?></td></tr>
						<tr><th class="part_pilo">Par la collectivité</th><td class="donnees"><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_collec']; ?></td></tr>
					</tbody>
				</table>
			</td>
	<td border="none" class="vide">
	</td>
	<td border="none">
				<table>
					<thead>
						<tr>
							<th class="part_pilo"><h4>Assistance aux usagers</h4></th><th class="donnees"><h4>Données</h4></th>
						</tr>
					</thead>
					<tbody>
						<tr><th class="part_pilo">Avec les moyens de l'établissement (personnel référent)</th><td class="donnees"><?php echo $donnees['assistance_usagers_etab']; ?></td></tr>
						<tr><th class="part_pilo">Par les services académiques</th><td class="donnees"><?php echo $donnees['assistance_usagers_aca']; ?></td></tr>
						<tr><th class="part_pilo">Par la collectivité</th><td class="donnees"><?php echo $donnees['assistance_usagers_collec']; ?></td></tr>
					</tbody>
				</table>
	</td>
	<td border="none" class="vide">
	</td>
	<td border="none">
	<table>
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 221</td><td><?php echo $donnees['palier_pilo'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	</td>
	</tr>
	</table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>
<?php
	
	$content = ob_get_clean() ;
	//die($content) ;
	require('html2pdf/html2pdf.class.php');
	try{
		$pdf = new HTML2PDF('L','A4','fr') ;
		$pdf->pdf->SetDisplayMode('fullpage') ;
		$pdf->writeHTML($content) ;
		$pdf->Output('Equipement.pdf') ;
	}catch (HTML2PDF_exception $e){
		die($e) ;
	}
	
?>
