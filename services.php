<?php
	$titre_page = "Services" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');

	$NomEtab = $_SESSION['NomEtab'];
?>

<section>
	<div id="top_section" >
		<h1>Services<?php if ($_SESSION['NomEtab'] != NULL){echo ' - '.$NomEtab ;} else {} ; ?></h1>
	</div>
	
	<div id="content">
<br />
		<?php if ($_SESSION['Rang'] == 2) 
		{
				$serv = $bdd->query('SELECT * FROM services WHERE RNE = "'.$_SESSION['RNE'].'"');
				$queryRNE = $bdd->query('SELECT RNE FROM services WHERE RNE = "'.$_SESSION['RNE'].'"'); 
	$count = $queryRNE->rowCount();  
		if($count == 1) 
			{	
		while($donnees = $serv->fetch())
		{
	?>
		<input class="btn" type="submit" value="Modifier les données" onclick="self.location.href='modif_serv.php'">&nbsp;&nbsp;<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_serv_ce.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>
	<table>
			<tr><th><h4>Critères</h4></th><th colspan="2"><h4>Données</h4></th><th><h4>Nombre de points</h4></th></tr>

			<?php if ($donnees['ENT_mod'] == "OUI")
			{
				?>
			<tr><th rowspan="3">ENT - Espace Numérique de Travail - issu d'un projet académique et/ou des collectivités associées</th><td colspan="2"><?php echo $donnees['ENT_mod']; ?></td><td rowspan="3"><?php echo $donnees['ENT_points']; ?> sur 24</td></tr>
			<tr><th>Déploiement à tous les utilisateurs</th><td><?php echo $donnees['ENT_utilisateur']; ?></td></tr>
			<tr><th>Déploiement de tous les services</th><td><?php echo $donnees['ENT_services']; ?></td></tr>

			<?php
			}
			else
			{
				?>

			<tr><th>ENT - Espace Numérique de Travail - issu d'un projet académique et/ou des collectivités associées</th><td colspan="2"><?php echo $donnees['ENT_mod']; ?></td><td><?php echo $donnees['ENT_points']; ?> sur 24</td></tr>

			<?php
			}
			?>

			<tr><th>Site web de l'établissement</th><td colspan="2"><?php echo $donnees['site_web_etablissement_mod']; ?></td><td><?php echo $donnees['site_web_etablissement_points']; ?> sur 10</td></tr>

			<tr><th>Notes, absences, emploi du temps</th><td colspan="2"><?php echo $donnees['notes_abscence_mod']; ?></td><td><?php echo $donnees['notes_abscence_points']; ?> sur 10</td></tr>
			
			<tr><th>Appel dématérialisé des élèves au début de l'heure de cours</th><td colspan="2"><?php echo $donnees['appel_dematerialise_mod']; ?></td><td><?php echo $donnees['appel_dematerialise_points']; ?> sur 15</td></tr>
			
			<tr><th>Messagerie interne</th><td colspan="2"><?php echo $donnees['messagerie_interne_mod']; ?></td><td><?php echo $donnees['messagerie_interne_points']; ?> sur 10</td></tr>
			
			<tr><th>LSUN - Livret Scolaire Unique Numérique<br>LPC - Livret Personnel de Compétences</th><td colspan="2"><?php echo $donnees['LSUN_LPC_mod']; ?></td><td><?php echo $donnees['LSUN_LPC_points']; ?> sur 15</td></tr>
			
			<tr><th>Service de réservation de ressources</th><td colspan="2"><?php echo $donnees['resa_ressources_mod']; ?></td><td><?php echo $donnees['resa_ressources_points']; ?> sur 5</td></tr>
			
			<tr><th>Abonnements à des services d'information et de documentation en ligne</th><td colspan="2"><?php echo $donnees['abonnements_services_information_en_ligne_mod']; ?></td><td><?php echo $donnees['abonnements_services_information_en_ligne_points']; ?> sur 15</td></tr>
			
			<tr><th>Folios</th><td colspan="2"><?php echo $donnees['folios_mod']; ?></td><td><?php echo $donnees['folios_points']; ?> sur 15</td></tr>
			
			<tr><th>Abonnements à des ressources numériques pédagogiques éditoriales</th><td colspan="2"><?php echo $donnees['abonnements_ressources_numeriques_pedagogiques_mod']; ?></td><td><?php echo $donnees['abonnements_ressources_numeriques_pedagogiques_points']; ?> sur 15</td></tr>
			
			<tr><th>Proportion de ressources numériques par rapport aux ressources papiers</th><td colspan="2"><?php echo $donnees['proportion_ressources_numeriques_papiers_mod']; ?></td><td><?php echo $donnees['proportion_ressources_numeriques_papiers_points']; ?> sur 20</td></tr>
	</table>

		<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 154</td><td><?php echo $donnees['palier_serv'] ; ?> sur 10</td></tr>
	</table>

		<?php
	}
	 }
			 else {
	 	?><h2>Aucune donnée rentrée pour cet établissement !</h2>
	 	<input class="btn" type="submit" value="Insérer les données" onclick="self.location.href='insert_serv.php?RNE=<?php echo $_SESSION['RNE'] ; ?> '">
	 	<?php
	 }
		}

		// Si l'utilisateur connecté n'est pas un chef d'établissement

	else
	{

if (empty($_GET['RNE']) AND (empty($_POST['RNE'])))
{
?> <h3>Veuillez tout d'abord choisir un établissement sur la page d'accueil</h3> <?php
}
	else
	{
		$RNE2 = $_GET['RNE'];
		$_SESSION['RNE'] = $_GET['RNE'];
		
	$serv = $bdd->query('SELECT * FROM services WHERE RNE = "'.$RNE2.'"');
	$queryRNE = $bdd->query('SELECT RNE FROM services WHERE RNE = "'.$RNE.'"'); 
	$count = $queryRNE->rowCount();  
	if($count == 1) 
		{
		while($donnees = $serv->fetch())
		{
	?>
		<input class="btn" type="submit" value="Modifier les données" onclick="self.location.href='modif_serv.php'">&nbsp;&nbsp;<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_serv.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>
	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Modalité retenue</h4></th><th><h4>Nombre de points</h4></th></tr>

			<tr><th>ENT - Espace Numérique de Travail - issu d'un projet académique et/ou des collectivités associées</th><td><?php echo $donnees['ENT_mod']; ?></td><td><?php echo $donnees['ENT_points']; ?> sur 24</td></tr>

			<tr><th>Site web de l'établissement</th><td><?php echo $donnees['site_web_etablissement_mod']; ?></td><td><?php echo $donnees['site_web_etablissement_points']; ?> sur 10</td></tr>

			<tr><th>Notes, absences, emploi du temps</th><td><?php echo $donnees['notes_abscence_mod']; ?></td><td><?php echo $donnees['notes_abscence_points']; ?> sur 10</td></tr>
			
			<tr><th>Appel dématérialisé des élèves au début de l'heure de cours</th><td><?php echo $donnees['appel_dematerialise_mod']; ?></td><td><?php echo $donnees['appel_dematerialise_points']; ?> sur 15</td></tr>
			
			<tr><th>Messagerie interne</th><td><?php echo $donnees['messagerie_interne_mod']; ?></td><td><?php echo $donnees['messagerie_interne_points']; ?> sur 10</td></tr>
			
			<tr><th>"LSUN - Livret Scolaire Unique Numérique<br>LPC - Livret Personnel de Compétences"</th><td><?php echo $donnees['LSUN_LPC_mod']; ?></td><td><?php echo $donnees['LSUN_LPC_points']; ?> sur 15</td></tr>
			
			<tr><th>Service de réservation de ressources</th><td><?php echo $donnees['resa_ressources_mod']; ?></td><td><?php echo $donnees['resa_ressources_points']; ?> sur 5</td></tr>
			
			<tr><th>Abonnements à des services d'information et de documentation en ligne</th><td><?php echo $donnees['abonnements_services_information_en_ligne_mod']; ?></td><td><?php echo $donnees['abonnements_services_information_en_ligne_points']; ?> sur 15</td></tr>
			
			<tr><th>Folios</th><td><?php echo $donnees['folios_mod']; ?></td><td><?php echo $donnees['folios_points']; ?> sur 15</td></tr>
			
			<tr><th>Abonnements à des ressources numériques pédagogiques éditoriales</th><td><?php echo $donnees['abonnements_ressources_numeriques_pedagogiques_mod']; ?></td><td><?php echo $donnees['abonnements_ressources_numeriques_pedagogiques_points']; ?> sur 15</td></tr>
			
			<tr><th>Proportion de ressources numériques par rapport aux ressources papiers</th><td><?php echo $donnees['proportion_ressources_numeriques_papiers_mod']; ?></td><td><?php echo $donnees['proportion_ressources_numeriques_papiers_points']; ?> sur 20</td></tr>
	</table>

<table class="tabledouble">
<tr>
<td class="tabledouble">
	<?php if ($donnees['ENT_mod'] == "OUI")
			{
				?>
				<table>
				<tr><th><h4>ENT</h4></th><th><h4>Données</h4></th></tr>
				<tr><th>Déploiement à tous les utilisateurs</th><td><?php echo $donnees['ENT_utilisateur']; ?></td></tr>
				<tr><th>Déploiement de tous les services</th><td><?php echo $donnees['ENT_services']; ?></td></tr>
				</table>

				<?php
			}
			else
			{

			}
			?>
</td>
<td class="tabledouble">
	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 154</td><td><?php echo $donnees['palier_serv'] ; ?> sur 10</td></tr>
	</table>
</td>
</tr>
</table>
		<?php
		}
		}
		else
{
	?><h2>Aucune notation effectuée sur cet établissement</h2><br>
	<?php
}
	}
}
?>
 	</div>
</section>
<?php
	include('pied_de_page.php');
	?>