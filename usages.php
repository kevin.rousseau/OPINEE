<?php
	$titre_page = "Usages" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');

	$NomEtab = $_SESSION['NomEtab'];
?>

<section>
	<div id="top_section" >
		<h1>Usages<?php if ($_SESSION['NomEtab'] != NULL){echo ' - '.$NomEtab ;} else {} ; ?></h1>
	</div>
	
	<div id="content">
 <br />
		<?php if ($_SESSION['Rang'] == 2) 
		{
				$usa = $bdd->query('SELECT * FROM usages WHERE RNE = "'.$_SESSION['RNE'].'"');
			$queryRNE = $bdd->query('SELECT RNE FROM usages WHERE RNE = "'.$_SESSION['RNE'].'"'); 
	$count = $queryRNE->rowCount();  
		if($count == 1) 
			{
		while($donnees = $usa->fetch())
		{
	?>
		<input class="btn" type="submit" value="Modifier les données" onclick="self.location.href='modif_usa.php'">&nbsp;&nbsp;<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_usa_ce.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>
	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Données</h4></th><th><h4>Nombre de points</h4></th></tr>

			<tr><th>Pourcentage d'élèves hors classes de 3ème ayant une validation partielle du B2i Collège</th><td><?php echo $donnees['pourcentage_eleves_validation_partielle_B2i_mod']; ?></td><td><?php echo $donnees['pourcentage_eleves_validation_partielle_B2i_points']; ?> sur 5</td></tr>

			<tr><th>Proportion de professeurs impliqués dans la certification des compétences numériques des élèves (B2i)</th><td><?php echo $donnees['proportion_enseignants_impliques_certification_competence_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_impliques_certification_competence_points']; ?> sur 5</td></tr>

			<tr><th>Nombre de disciplines impliquées dans la validation des compétences du B2i</th><td><?php echo $donnees['nb_discipline_validation_competences_B2i_mod']; ?></td><td><?php echo $donnees['nb_discipline_validation_competences_B2i_points']; ?> sur 10</td></tr>

			<tr><th>Proportion de professeurs développant des usages pédagogiques du numérique</th><td><?php echo $donnees['proportion_enseignants_developpant_usage_numerique_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_developpant_usage_numerique_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de manuels scolaires numériques dans l'EPLE</th><td><?php echo $donnees['proportion_manuels_scolaires_numeriques_EPLE_mod']; ?></td><td><?php echo $donnees['proportion_manuels_scolaires_numeriques_EPLE_points']; ?> sur 15</td></tr>

			<tr><th>Usages de services et ressources pédagogiques numériques institutionnels (D'Col, EFS, éduthèque, Fondamentaux, etc.)</th><td><?php echo $donnees['usages_services_ressources_numeriques_mod']; ?></td><td><?php echo $donnees['usages_services_ressources_numeriques_points']; ?> sur 15</td></tr>

			<tr><th>Usages d'une messagerie institutionnelle (académique ou dans l'ENT) pour les échanges professionnels</th><td><?php echo $donnees['usages_messagerie_institutionnelle_echanges_professionnel_mod']; ?></td><td><?php echo $donnees['usages_messagerie_institutionnelle_echanges_professionnel_points']; ?> sur 20</td></tr>

			<tr><th>Usages de la messagerie de l'ENT pour les échanges des professeurs avec les familles</th><td><?php echo $donnees['usages_messagerie_ENT_enseignants_famille_mod']; ?></td><td><?php echo $donnees['usages_messagerie_ENT_enseignants_famille_points']; ?> sur 15</td></tr>

			<tr><th>Usages d'espaces de stockage ou de partage de documents pédagogiques</th><td><?php echo $donnees['usages_espaces_stockages_partage_documents_pedagogiques_mod']; ?></td><td><?php echo $donnees['usages_espaces_stockages_partage_documents_pedagogiques_points']; ?> sur 10</td></tr>

			<tr><th>Usages d'outils numériques pour des pratiques collaboratives entre ensiegnants</th><td><?php echo $donnees['usages_outils_numeriques_entre_enseignants_mod']; ?></td><td><?php echo $donnees['usages_outils_numeriques_entre_enseignants_points']; ?> sur 15</td></tr>

			<tr><th>Usages d'outils numériques pour des pratiques collaboratives des enseignants avec leurs élèves</th><td><?php echo $donnees['usages_outils_numeriques_entre_enseignants_eleves_mod']; ?></td><td><?php echo $donnees['usages_outils_numeriques_entre_enseignants_eleves_points']; ?> sur 20</td></tr>

			<tr><th>Usages connus de réseaux sociaux dans le cadre pédagogique</th><td><?php echo $donnees['usages_reseaux_sociaux_pedagogiques_mod']; ?></td><td><?php echo $donnees['usages_reseaux_sociaux_pedagogiques_points']; ?> sur 15</td></tr>

			<tr><th>Usages connus de services de publication de type blog pour l'enseignement</th><td><?php echo $donnees['usages_services_publication_pour_enseignement_mod']; ?></td><td><?php echo $donnees['usages_services_publication_pour_enseignement_points']; ?> sur 15</td></tr>

			<tr><th>Création de médias numériques (journal, radio, vidéo, exposition) - connue et validée par le chef d'établissement</th><td><?php echo $donnees['creation_medias_numeriques_mod']; ?></td><td><?php echo $donnees['creation_medias_numeriques_points']; ?> sur 20</td></tr>

			<tr><th>Utilisation d'outils et de ressources numériques pour le développement de la pratique de l'oral (baladodiffusion, labo mutimédia, etc.)</th><td><?php echo $donnees['utilisation_outils_ressources_numeriques_dvlp_oral_mod']; ?></td><td><?php echo $donnees['utilisation_outils_ressources_numeriques_dvlp_oral_points']; ?> sur 20</td></tr>

			<tr><th>Usages de services numériques de suivi de la maîtrise des compétences (ex : LPC, LSUN)</th><td><?php echo $donnees['usages_services_numeriques_suivi_competences_mod']; ?></td><td><?php echo $donnees['usages_services_numeriques_suivi_competences_points']; ?> sur 20</td></tr>

			<tr><th>Usages du numérique à des fins de personnalisation des parcours et d'individualisation des enseignements</th><td><?php echo $donnees['usages_numeriques_personnalisation_parcours_individu_mod']; ?></td><td><?php echo $donnees['usages_numeriques_personnalisation_parcours_individu_points']; ?> sur 20</td></tr>

			<tr><th>Usages du numérique dans le cadre de la liaison inter-degré</th><td><?php echo $donnees['usages_numeriques_liaison_inter_degre_mod']; ?></td><td><?php echo $donnees['usages_numeriques_liaison_inter_degre_points']; ?> sur 20</td></tr>
	</table>

		<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 280</td><td><?php echo $donnees['palier_usa'] ; ?> sur 10</td></tr>
	</table>

		<?php
		}
			}
			else {
	 	?>

	 	<h2>Aucune donnée rentrée pour cet établissement !</h2>
	 	<input class="btn" type="submit" value="Insérer les données" onclick="self.location.href='insert_usa.php?RNE=<?php echo $_SESSION['RNE'] ; ?> '">

	 	<?php
	 }
		}
	else
	{

if (empty($_GET['RNE']) AND (empty($_POST['RNE'])))
{
?> <h3>Veuillez tout d'abord choisir un établissement sur la page d'accueil</h3> <?php
}
	else
	{
		$RNE = $_GET['RNE'];
		$_SESSION['RNE'] = $_GET['RNE'];
		
	$usa = $bdd->query('SELECT * FROM usages WHERE RNE = "'.$RNE.'"');
	$queryRNE = $bdd->query('SELECT RNE FROM usages WHERE RNE = "'.$RNE.'"'); 
	$count = $queryRNE->rowCount();  
	if($count == 1) 
		{
		while($donnees = $usa->fetch())
		{
	?>
		<input class="btn" type="submit" value="Modifier les données" onclick="self.location.href='modif_usa.php'">&nbsp;&nbsp;<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_usa.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>
	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Modalité retenue</h4></th><th><h4>Nombre de points</h4></th></tr>
			
			<tr><th>Pourcentage d'élèves hors classes de 3ème ayant une validation partielle du B2i Collège</th><td><?php echo $donnees['pourcentage_eleves_validation_partielle_B2i_mod']; ?></td><td><?php echo $donnees['pourcentage_eleves_validation_partielle_B2i_points']; ?> sur 5</td></tr>

			<tr><th>Proportion de professeurs impliqués dans la certification des compétences numériques des élèves (B2i)</th><td><?php echo $donnees['proportion_enseignants_impliques_certification_competence_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_impliques_certification_competence_points']; ?> sur 5</td></tr>

			<tr><th>Nombre de disciplines impliquées dans la validation des compétences du B2i</th><td><?php echo $donnees['nb_discipline_validation_competences_B2i_mod']; ?></td><td> <?php echo $donnees['nb_discipline_validation_competences_B2i_points']; ?> sur 10</td></tr>

			<tr><th>Proportion de professeurs développant des usages pédagogiques du numérique</th><td><?php echo $donnees['proportion_enseignants_developpant_usage_numerique_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_developpant_usage_numerique_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de manuels scolaires numériques dans l'EPLE</th><td><?php echo $donnees['proportion_manuels_scolaires_numeriques_EPLE_mod']; ?></td><td> <?php echo $donnees['proportion_manuels_scolaires_numeriques_EPLE_points']; ?> sur 15</td></tr>

			<tr><th>Usages de services et ressources pédagogiques numériques institutionnels (D'Col, EFS, éduthèque, Fondamentaux, etc.)</th><td><?php echo $donnees['usages_services_ressources_numeriques_mod']; ?></td><td> <?php echo $donnees['usages_services_ressources_numeriques_points']; ?> sur 15</td></tr>

			<tr><th>Usages d'une messagerie institutionnelle (académique ou dans l'ENT) pour les échanges professionnels</th><td><?php echo $donnees['usages_messagerie_institutionnelle_echanges_professionnel_mod']; ?></td><td><?php echo $donnees['usages_messagerie_institutionnelle_echanges_professionnel_points']; ?> sur 20</td></tr>

			<tr><th>Usages de la messagerie de l'ENT pour les échanges des professeurs avec les familles (si ENT)</th><td><?php echo $donnees['usages_messagerie_ENT_enseignants_famille_mod']; ?></td><td><?php echo $donnees['usages_messagerie_ENT_enseignants_famille_points']; ?> sur 15</td></tr>

			<tr><th>Usages d'espaces de stockage ou de partage de documents pédagogiques</th><td><?php echo $donnees['usages_espaces_stockages_partage_documents_pedagogiques_mod']; ?></td><td><?php echo $donnees['usages_espaces_stockages_partage_documents_pedagogiques_points']; ?> sur 10</td></tr>

			<tr><th>Usages d'outils numériques pour des pratiques collaboratives entre ensiegnants</th><td><?php echo $donnees['usages_outils_numeriques_entre_enseignants_mod']; ?></td><td> <?php echo $donnees['usages_outils_numeriques_entre_enseignants_points']; ?> sur 15</td></tr>

			<tr><th>Usages d'outils numériques pour des pratiques collaboratives des enseignants avec leurs élèves</th><td><?php echo $donnees['usages_outils_numeriques_entre_enseignants_eleves_mod']; ?></td><td><?php echo $donnees['usages_outils_numeriques_entre_enseignants_eleves_points']; ?> sur 20</td></tr>

			<tr><th>Usages connus de réseaux sociaux dans le cadre pédagogique</th><td><?php echo $donnees['usages_reseaux_sociaux_pedagogiques_mod']; ?></td><td><?php echo $donnees['usages_reseaux_sociaux_pedagogiques_points']; ?> sur 15</td></tr>

			<tr><th>Usages connus de services de publication de type blog pour l'enseignement</th><td><?php echo $donnees['usages_services_publication_pour_enseignement_mod']; ?></td><td><?php echo $donnees['usages_services_publication_pour_enseignement_points']; ?> sur 15</td></tr>

			<tr><th>Création de médias numériques (journal, radio, vidéo, exposition) - connue et validée par le chef d'établissement</th><td><?php echo $donnees['creation_medias_numeriques_mod']; ?></td><td><?php echo $donnees['creation_medias_numeriques_points']; ?> sur 20</td></tr>

			<tr><th>Utilisation d'outils et de ressources numériques pour le développement de la pratique de l'oral (baladodiffusion, labo mutimédia, etc.)</th><td><?php echo $donnees['utilisation_outils_ressources_numeriques_dvlp_oral_mod']; ?></td><td><?php echo $donnees['utilisation_outils_ressources_numeriques_dvlp_oral_points']; ?> sur 20</td></tr>

			<tr><th>Usages de services numériques de suivi de la maîtrise des compétences (ex : LPC, LSUN, etc.)</th><td><?php echo $donnees['usages_services_numeriques_suivi_competences_mod']; ?></td><td><?php echo $donnees['usages_services_numeriques_suivi_competences_points']; ?> sur 20</td></tr>

			<tr><th>Usages du numérique à des fins de personnalisation des parcours et d'individualisation des enseignements</th><td><?php echo $donnees['usages_numeriques_personnalisation_parcours_individu_mod']; ?></td><td><?php echo $donnees['usages_numeriques_personnalisation_parcours_individu_points']; ?> sur 20</td></tr>

			<tr><th>Usages du numérique dans le cadre de la liaison inter-degré</th><td><?php echo $donnees['usages_numeriques_liaison_inter_degre_mod']; ?></td><td><?php echo $donnees['usages_numeriques_liaison_inter_degre_points']; ?> sur 20</td></tr>
	</table>

	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 280</td><td><?php echo $donnees['palier_usa'] ; ?> sur 10</td></tr>
	</table>

		<?php
		}
	}
else
{
	?><h2>Aucune notation effectuée sur cet établissement !</h2><br>
	<?php
}
}
}
?>
 	</div>
</section>
<br>
<?php
	include('pied_de_page.php');
	?>

