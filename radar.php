<?php
	$titre_page = "Radar" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');

    $NomEtab = $_SESSION['NomEtab'];
?>
<section>
	<div id="top_section" >
		<h1>Radar<?php if ($_SESSION['NomEtab'] != NULL){echo ' - '.$NomEtab ;} else {} ; ?></h1>
	</div>
	
	<div id="content">
<br /><br /><br />
<canvas id="myChart" width="110%" height="60%">
<?php
	if (empty($_SESSION['RNE']))
{
?> <h3>Veuillez tout d'abord choisir un établissement sur la page d'accueil</h3> <?php
}
	else
	{
		$resume = $bdd->query(' SELECT palier_equip, palier_form, palier_infra, palier_pilo, palier_serv, palier_usa, palier_uti FROM equipements, formation, infrastructures, pilotage, services, usages, utilisations WHERE equipements.RNE = "'.$_SESSION['RNE'].'" AND formation.RNE = "'.$_SESSION['RNE'].'" AND infrastructures.RNE = "'.$_SESSION['RNE'].'" AND pilotage.RNE = "'.$_SESSION['RNE'].'" AND services.RNE = "'.$_SESSION['RNE'].'" AND usages.RNE = "'.$_SESSION['RNE'].'" AND utilisations.RNE = "'.$_SESSION['RNE'].'"');

		$moyenne = $bdd->query(' SELECT AVG(palier_equip) AS moy_equip, AVG(palier_form) AS moy_form, AVG(palier_infra) AS moy_infra, AVG(palier_pilo) AS moy_pilo, AVG(palier_serv) AS moy_serv, AVG(palier_usa) AS moy_usa, AVG(palier_uti) AS moy_uti FROM equipements, formation, infrastructures, pilotage, services, usages, utilisations');

		while($donnees = $resume->fetch())
		{
			while($donnees2 = $moyenne->fetch())
			{
	?>
<script>
// On récupere l’objet canvas dans une variable 
var ctx = document.getElementById("myChart");
//On crée une nouvelle variable de type Chart
var myChart = new Chart(ctx, {
	//On indique les paramètres du graphique
    type: 'radar',
    data: {
	labels: ["Equipements", "Infrastructures", "Services", "Pilotage", "Formation", "Utilisations", "Usages"],
    datasets: [
        {
            label: "Palier des différents domaines du <?php echo $NomEtab ; ?>",
            backgroundColor: "rgba(229,73,134,0.2)",
            borderColor: "rgba(229,73,134,1)",
            //Point sur le graphique
            pointBackgroundColor: "rgba(229,73,134,1)",
            pointBorderColor: "rgba(229,73,134,1)",
            pointHoverBackgroundColor: "rgba(229,73,134,1)",
            pointHoverBorderColor: "rgba(229,73,134,1)",
            data: [<?php echo $donnees['palier_equip']; ?>, <?php echo $donnees['palier_infra']; ?>, <?php echo $donnees['palier_serv']; ?>, <?php echo $donnees['palier_pilo']; ?>, <?php echo $donnees['palier_form']; ?>, <?php echo $donnees['palier_uti']; ?>, <?php echo $donnees['palier_usa']; ?>]
        },
        {
            label: "Moyenne de tout les établissements ",
            backgroundColor: "rgba(10,10,10,0.2)",
            borderColor: "rgba(10,10,10o,1)",
            //Point sur le graphique
            pointBackgroundColor: "rgba(10,10,10,1)",
            pointBorderColor: "rgba(10,10,10,1)",
            pointHoverBackgroundColor: "rgba(10,10,10,1)",
            pointHoverBorderColor: "rgba(10,10,10,1)",
            data: [<?php echo $donnees2['moy_equip']; ?>, <?php echo $donnees2['moy_infra']; ?>, <?php echo $donnees2['moy_serv']; ?>, <?php echo $donnees2['moy_pilo']; ?>, <?php echo $donnees2['moy_form']; ?>, <?php echo $donnees2['moy_uti']; ?>, <?php echo $donnees2['moy_usa']; ?>]
        }
    ]
},    options: {
            scale: {
                ticks: {
                    beginAtZero: true,
                    min: 0,
                    max:10
                }
            }
    }
});
</script>
<?php
}
}
}
?>
</canvas>
 	</div>
</section>
<?php
    include('pied_de_page.php');
    ?>