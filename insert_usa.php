<?php
	$titre_page = "Insert_usa" ;
	include('header.php');
	include('menu.php');
?>

	<section>
	<div id="top_section" >
		<h1>Insertions des données</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	
	<div id="content">
	<?php
		if(!empty($msg_error))
		{
			echo '<div id="msg_error_2">'.$msg_error.'</div>' ;
		}

		else
		{

		$RNE = $_SESSION['RNE'];

	 ?>
		<br />

<table>

	<form method="post" action="insert_usa_php.php?RNE=<?php echo $RNE ; ?>" enctype="multipart/form-data">
	
			<tr><th><h4>Critères</h4></th><th><h4>Données</h4></th></tr>

			<tr><th>Pourcentage d'élèves hors classes de 3ème ayant une validation partielle du B2i Collège</th><td>
				<select name="validation">
				<option selected="selected" value="1">Plus de 30%</option>
				<option value="2">Strictement moins de 30%</option>
				</select>
			</td></tr>

			<tr><th>Proportion de professeurs impliqués dans la certification des compétences numériques des élèves (B2i)</th><td>
				<select name="implication">
				<option selected="selected" value="1">Plus de 50%</option>
				<option value="2">Plus de 25% et strictement moins de 50%</option>
				<option value="3">Strictement moins de 25%</option>
				</select>
			</td></tr>

			<tr><th>Nombre de disciplines impliquées dans la validation des compétences du B2i</th><td>
				<select name="discipline">
				<option selected="selected" value="1">4 ou plus</option>
				<option value="2">1 à 3</option>
				<option value="3">0</option>
				</select>
			</td></tr>

			<tr><th>Proportion de professeurs développant des usages pédagogiques du numérique</th><td>
				<select name="usages_prof">
				<option selected="selected" value="1">Plus de 75%</option>
				<option value="2">Plus de 50% et strictement moins de 75%</option>
				<option value="3">Plus de 25% et strictement moins de 50%</option>
				<option value="4">Strictement moins de 25%</option>
				</select>
			</td></tr>

			<tr><th>Proportion de manuels scolaires numériques dans l'EPLE</th><td>
				<select name="manuel">
				<option selected="selected" value="1">100%</option>
				<option value="2">Plus de 50% et strictement moins de 100%</option>
				<option value="3">Plus de 25% et strictement moins de 50%</option>
				<option value="4">Strictement moins de 25%</option>
				</select>
			</td></tr>

			<tr><th>Usages de services et ressources pédagogiques numériques institutionnels (D'Col, EFS, éduthèque, Fondamentaux, etc.)</th><td>
				<select name="institution">
				<option selected="selected" value="1">Oui, régulièrement</option>
				<option value="2">Oui, occasionnellement</option>
				<option value="3">Non</option>
				</select>
			</td></tr>

			<tr><th>Usages d'une messagerie institutionnelle (académique ou dans l'ENT) pour les échanges professionnels</th><td>
				<select name="messagerie">
				<option selected="selected" value="1">Généralisés</option>
				<option value="2">Limités</option>
				<option value="3">Rares</option>
				</select>
			</td></tr>

			<tr><th>Usages de la messagerie de l'ENT pour les échanges des professeurs avec les familles</th><td>
				<select name="messagerie_ENT">
				<option selected="selected" value="1">Généralisés</option>
				<option value="2">Limités</option>
				<option value="3">Rares</option>
				<option value="4">Pas d'ENT</option>
				</select>
			</td></tr>

			<tr><th>Usages d'espaces de stockage ou de partage de documents pédagogiques</th><td>
				<select name="espace">
				<option selected="selected" value="1">Généralisés</option>
				<option value="2">Limités</option>
				<option value="3">Rares</option>
				</select>
			</td></tr>

			<tr><th>Usages d'outils numériques pour des pratiques collaboratives entre ensiegnants</th><td>
				<select name="outils">
				<option selected="selected" value="1">Généralisés</option>
				<option value="2">Limités</option>
				<option value="3">Rares</option>
				</select>
			</td></tr>

			<tr><th>Usages d'outils numériques pour des pratiques collaboratives des enseignants avec leurs élèves</th><td>
				<select name="outils_eleves">
				<option selected="selected" value="1">Généralisés</option>
				<option value="2">Limités</option>
				<option value="3">Rares</option>
				</select>
			</td></tr>

			<tr><th>Usages connus de réseaux sociaux dans le cadre pédagogique</th><td>
				<select name="reseaux">
				<option selected="selected" value="1">Généralisés</option>
				<option value="2">Limités</option>
				<option value="3">Rares</option>
				</select>
			</td></tr>

			<tr><th>Usages connus de services de publication de type blog pour l'enseignement</th><td>
				<select name="publication">
				<option selected="selected" value="1">Généralisés</option>
				<option value="2">Limités</option>
				<option value="3">Rares</option>
				</select>
			</td></tr>

			<tr><th>Création de médias numériques (journal, radio, vidéo, exposition) - connue et validée par le chef d'établissement</th><td>
				<select name="medias">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Utilisation d'outils et de ressources numériques pour le développement de la pratique de l'oral (baladodiffusion, labo mutimédia, etc.)</th><td>
				<select name="ressource">
				<option selected="selected" value="1">Limités et pas seulement en LVE</option>
				<option value="2">Généralisés mais exclusivement en LVE</option>
				<option value="3">Limités et pas seulement en LVE</option>
				<option value="4">Limités et exclusivement en LVE</option>
				<option value="5">Rares</option>
				</select>
			</td></tr>

			<tr><th>Usages de services numériques de suivi de la maîtrise des compétences (ex : LPC, LSUN)</th><td>
				<select name="service">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Usages du numérique à des fins de personnalisation des parcours et d'individualisation des enseignements</th><td>
				<select name="personnalisation">
				<option selected="selected" value="1">Généralisés</option>
				<option value="2">Limités</option>
				<option value="3">Rares</option>
				</select>
			</td></tr>

			<tr><th>Usages du numérique dans le cadre de la liaison inter-degré</th><td>
				<select name="liaison">
				<option selected="selected" value="1">Généralisés</option>
				<option value="2">Limités</option>
				<option value="3">Rares</option>
				</select>
			</td></tr>
	</table>

		<input class="btn" type="submit" value="Valider les informations" />
		<input type="hidden" name="RNE" value=<?php echo $RNE ?> />
		</form>
		<?php } ?>
 	</div>
</section>