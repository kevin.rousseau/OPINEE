<?php
	ob_start() ;
	include('header.php');
	
	$requete1 = $bdd->query('SELECT * FROM formation WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$NomEtab = $_SESSION['NomEtab'];

setlocale(LC_TIME, 'fra_fra');
date_default_timezone_set('Europe/Paris');
?>
	<style>
img.img_gauche
{
	width: 25% ;
	height:80%;
	float: left ;
}
		
img.img_droite
{
	width:20% ;
	height:90%;
	float: right ;
	margin-top: -15 ;
}
		

h2
{
	color : #02A4E4;
	text-align:center;
	margin-top: 0px;
}

h3
{
	color: #E54986 ;
	text-align:center;			
}
		
h4
{
	color : #4A51A9;
    margin-bottom: 3px;
    margin-top: 3px;
    text-align:center;
}

h5
{
	color : #4A51A9;
	position: absolute;
	bottom : 13px;
	text-align:right; 
}

.top
{
	height: 50px ;
}

.corps
{
		width: 95%;
}
		
table
{
	border: 1px #4A51A9 solid;
	border-collapse : collapse ;
	width: 79%;
	text-align:center;
}

th
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:78%;
	margin-top: -10px;
}

th.mod
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:30%;
}

th.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:19%;
}

th.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:90%;
}

th.donnees
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:45%;
}

th.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:100%;
}

th.part_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:105%;
}

th.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:40%;
}

th.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:10%;
}

th.domaine
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

td
{
	border: 1px #4A51A9 solid ;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnees
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:45%;
}

td.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:10%;
}

td.vide
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:7%;
}

td.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:80%;
}

td.part_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:90%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:90%;
}

td.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:10%;
}

	</style>
	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Formation - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th><h4>Critères</h4></th>
						<th class="mod"><h4>Modalité retenue</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete1->fetch())
			{
		?>

			<tr><th>Formation au numérique et/ou aux aspects juridiques du numérique d'un ou de plusieurs membres<br>de l'équipe de direction (au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_direction_mod']; ?></td><td class="points"><?php echo $donnees['formation_numerique_direction_points']; ?> sur 20</td></tr>

			<tr><th>Formation au numérique d'un ou plusieurs membres du personnel de vie scolaire<br>(au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_vie_scolaire_mod']; ?></td><td class="points"><?php echo $donnees['formation_numerique_vie_scolaire_points']; ?> sur 20</td></tr>

			<tr><th>Formation au numérique d'un ou de plusieurs membres du personnel administratif et/ou technique<br>(au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_administration_personnel_technique_mod']; ?></td><td class="points"><?php echo $donnees['formation_numerique_administration_personnel_technique_points']; ?> sur 20</td></tr>

			<tr><th>Formation de l'équipe enseignante aux usages pédagogiques du numérique sur site et<br>par un formateur académique (au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_enseignants_mod']; ?></td><td class="points"><?php echo $donnees['formation_numerique_enseignants_points']; ?> sur 15</td></tr>

			<tr><th>Animations ponctuelles autour des usages du numérique par un référent numérique (personne ressource)<br>dans l'établissement  (au cours des trois dernières années) :</th><td><?php echo $donnees['animations_ponctuelles_numeriques_mod']; ?></td><td class="points"><?php echo $donnees['animations_ponctuelles_numeriques_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de professeurs ayant suivi une formation (PAF, M@gistère) aux usages pédagogiques<br>du numérique(au cours des trois dernières années)</th><td><?php echo $donnees['proportion_enseignants_suivi_formation_numerique_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_suivi_formation_numerique_points']; ?> sur 15</td></tr>

			<tr><th>Actions d'information et de sensibilisation aux usages responsables du numérique et à l'Éducation aux Médias<br>et à l'Information (EMI) à destination des enseignants (au cours des trois dernières années)</th><td><?php echo $donnees['actions_informations_sensibilisations_EMI_enseignants_mod']; ?></td><td class="points"><?php echo $donnees['actions_informations_sensibilisations_EMI_enseignants_points']; ?> sur 10</td></tr>

			<tr><th>Proportion d'enseignants mutualisant leurs pratiques du numérique et/ou collaborant<br>via un ou des réseau(x) numériques)</th><td><?php echo $donnees['proportion_enseignants_mutualisant_pratique_numerique_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_mutualisant_pratique_numerique_points']; ?> sur 15</td></tr>

			<tr><th>Identification dans l'établissement d'une personne chargée de la veille et de la curation du numérique<br>à destination des enseignants</th><td><?php echo $donnees['identification_personne_veille_numerique_pour_enseignants_mod']; ?></td><td class="points"><?php echo $donnees['identification_personne_veille_numerique_pour_enseignants_points']; ?> sur 15</td></tr>

			<tr><th>Formation des parents aux usages des services numériques</th><td><?php echo $donnees['formation_parent_usages_numeriques_mod']; ?></td><td class="points"><?php echo $donnees['formation_parent_usages_numeriques_points']; ?> sur 15</td></tr>

			<tr><th>Formation des parents aux usages responsables des services numériques</th><td><?php echo $donnees['formation_parents_usages_responsables_numeriques_mod']; ?></td><td class="points"><?php echo $donnees['formation_parents_usages_responsables_numeriques_points']; ?> sur 15</td></tr>

				</tbody>
			</table>
			<br><br>

			<table align="center" border="none">
			<tr>
	<td border="none">
	<table>
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 175</td><td><?php echo $donnees['palier_form'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	</td>
	</tr></table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>
<?php
	
	$content = ob_get_clean() ;
	//die($content) ;
	require('html2pdf/html2pdf.class.php');
	try{
		$pdf = new HTML2PDF('L','A4','fr') ;
		$pdf->pdf->SetDisplayMode('fullpage') ;
		$pdf->writeHTML($content) ;
		$pdf->Output('Equipement.pdf') ;
	}catch (HTML2PDF_exception $e){
		die($e) ;
	}
	
?>
