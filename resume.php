<?php
	$titre_page = "Resume" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');

	$NomEtab = $_SESSION['NomEtab'];
?>

<section>
	<div id="top_section" >
		<h1>Resume<?php if ($_SESSION['NomEtab'] != NULL){echo ' - '.$NomEtab ;} else {} ; ?></h1>
	</div>
	
	<div id="content">
	<br />
		<?php if ($_SESSION['Rang'] == 2) 
		{
				$resume = $bdd->query(' SELECT palier_equip, palier_form, palier_infra, palier_pilo, palier_serv, palier_usa, palier_uti FROM equipements, formation, infrastructures, pilotage, services, usages, utilisations WHERE equipements.RNE = "'.$_SESSION['RNE'].'" AND formation.RNE = "'.$_SESSION['RNE'].'" AND infrastructures.RNE = "'.$_SESSION['RNE'].'" AND pilotage.RNE = "'.$_SESSION['RNE'].'" AND services.RNE = "'.$_SESSION['RNE'].'" AND usages.RNE = "'.$_SESSION['RNE'].'" AND utilisations.RNE = "'.$_SESSION['RNE'].'"');

		while($donnees = $resume->fetch())
		{
	?>
	<table>
			<tr><th><h4>Domaine</h4></th><th><h4>Palier</h4></th></tr>

			<tr><th>Equipements</th><td><?php echo $donnees['palier_equip']; ?> sur 10</td></tr>

			<tr><th>Infrastructures</th><td><?php echo $donnees['palier_infra']; ?> sur 10</td></tr>

			<tr><th>Services</th><td><?php echo $donnees['palier_serv']; ?> sur 10</td></tr>

			<tr><th>Pilotage</th><td><?php echo $donnees['palier_pilo']; ?> sur 10</td></tr>

			<tr><th>Formation</th><td><?php echo $donnees['palier_form']; ?> sur 10</td></tr>

			<tr><th>Utilisations</th><td><?php echo $donnees['palier_uti']; ?> sur 10</td></tr>

			<tr><th>Usages</th><td><?php echo $donnees['palier_usa']; ?> sur 10</td></tr>
	</table>

	<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_resume_ce.php'"><br><br>
		<?php
		}
			}
	else
	{

if (empty($_SESSION['RNE']))
{
?> <h3>Veuillez tout d'abord choisir un établissement sur la page d'accueil</h3> <?php
}
	else
	{
		$RNE2 = $_GET['RNE'];
		$_SESSION['RNE'] = $_GET['RNE'];
		
				$resume = $bdd->query(' SELECT palier_equip, palier_form, palier_infra, palier_pilo, palier_serv, palier_usa, palier_uti FROM equipements, formation, infrastructures, pilotage, services, usages, utilisations WHERE equipements.RNE = "'.$_SESSION['RNE'].'" AND formation.RNE = "'.$_SESSION['RNE'].'" AND infrastructures.RNE = "'.$_SESSION['RNE'].'" AND pilotage.RNE = "'.$_SESSION['RNE'].'" AND services.RNE = "'.$_SESSION['RNE'].'" AND usages.RNE = "'.$_SESSION['RNE'].'" AND utilisations.RNE = "'.$_SESSION['RNE'].'"');
			?>
		<?php
		while($donnees = $resume->fetch())
		{
	?>
	<table>
			<tr><th><h4>Domaine</h4></th><th><h4>Palier</h4></th></tr>

			<tr><th>Equipements</th><td><?php echo $donnees['palier_equip']; ?> sur 10</td></tr>

			<tr><th>Infrastructures</th><td><?php echo $donnees['palier_infra']; ?> sur 10</td></tr>

			<tr><th>Services</th><td><?php echo $donnees['palier_serv']; ?> sur 10</td></tr>

			<tr><th>Pilotage</th><td><?php echo $donnees['palier_pilo']; ?> sur 10</td></tr>

			<tr><th>Formation</th><td><?php echo $donnees['palier_form']; ?> sur 10</td></tr>

			<tr><th>Utilisations</th><td><?php echo $donnees['palier_uti']; ?> sur 10</td></tr>
			
			<tr><th>Usages</th><td><?php echo $donnees['palier_usa']; ?> sur 10</td></tr>
	</table>

	<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_resume.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>
		<?php
		}
	}
}
?>
 	</div>
</section>
<?php
	include('pied_de_page.php');
?>