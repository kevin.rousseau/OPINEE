<?php
	$titre_page = "Modif_etablissement" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');

	if ($_SESSION['Rang'] == 2) 
		{
	if(!empty($_POST['nometab']) and !empty($_POST['villeetab']) and !empty($_POST['nomprincipal']) and !empty($_POST['prenomprincipal']) and !empty($_POST['nb_eleve']) and !empty($_POST['nb_enseignant']) and !empty($_POST['nb_salles']) and !empty($_POST['site']))
	{
	
		$nom = $_POST['nometab'] ;
		$ville = $_POST['villeetab'] ;
		$nomprinc = $_POST['nomprincipal'] ;
		$prenomprinc = $_POST['prenomprincipal'] ;
		$eleve = $_POST['nb_eleve'] ;
		$enseignant = $_POST['nb_enseignant'] ;
		$salles = $_POST['nb_salles'] ;
		$URL = $_POST['site'] ;


		$requete1 = $bdd->prepare('UPDATE etablissements SET nom = :nometab, ville = :villeetab, nb_eleves = :nbeleve, nb_enseignants = :nbprof, nb_salles_enseignements = :salles, URL_etablissement = :site WHERE RNE = "'.$_SESSION['RNE'].'"');
		$requete1->execute(array(
			'nometab' => $nom,
			'villeetab' => $ville,
			'nbeleve' => $eleve,
			'nbprof' => $enseignant,
			'salles' => $salles,
			'site' => $URL
				));	

		$requete2 = $bdd->prepare('UPDATE ce SET prenomCE = :prenom, nomCE = :nom WHERE RNE = "'.$_SESSION['RNE'].'"');
		$requete2->execute(array(
			'nom' => $nomprinc,
			'prenom' => $prenomprinc
			));

		header('refresh:0;url=confirm_modif_info.php') ;
	}
	else
	{
		header('refresh:0;url=modif_info.php') ;
	}
}
else 		//Si la personne connectée n'est pas un chef d'etablissement
{

	$RNE = $_POST['RNE'];

	if(!empty($_POST['nometab']) and !empty($_POST['villeetab']) and !empty($_POST['nomprincipal']) and !empty($_POST['prenomprincipal']) and !empty($_POST['nb_eleve']) and !empty($_POST['nb_enseignant']) and !empty($_POST['nb_salles']) and !empty($_POST['site']))
	{
	
		$nom = $_POST['nometab'] ;
		$ville = $_POST['villeetab'] ;
		$nomprinc = $_POST['nomprincipal'] ;
		$prenomprinc = $_POST['prenomprincipal'] ;
		$eleve = $_POST['nb_eleve'] ;
		$enseignant = $_POST['nb_enseignant'] ;
		$salles = $_POST['nb_salles'] ;
		$URL = $_POST['site'] ;


		$requete1 = $bdd->prepare('UPDATE etablissements SET nom = :nometab, ville = :villeetab, nb_eleves = :nbeleve, nb_enseignants = :nbprof, nb_salles_enseignements = :salles, URL_etablissement = :site WHERE RNE = "'.$_POST['RNE'].'"');
		$requete1->execute(array(
			'nometab' => $nom,
			'villeetab' => $ville,
			'nbeleve' => $eleve,
			'nbprof' => $enseignant,
			'salles' => $salles,
			'site' => $URL
				));	

		$requete2 = $bdd->prepare('UPDATE ce SET prenomCE = :prenom, nomCE = :nom WHERE RNE = "'.$_POST['RNE'].'"');
		$requete2->execute(array(
			'nom' => $nomprinc,
			'prenom' => $prenomprinc
			));
			header("refresh:0;url=confirm_modif_info.php?RNE=".$RNE."") ;
	}
	else
	{
		header('refresh:0;url=modif_info.php') ;
	}


}
?>
</div>
</section>