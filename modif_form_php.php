<?php
	$titre_page = "Modif_form" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');
	
	$RNE = $_POST['RNE'];

	if(!empty($_POST['formation_numerique_direction']) and !empty($_POST['formation_numerique_vie_scolaire']) and !empty($_POST['formation_numerique_administration_personnel_technique']) and !empty($_POST['formation_numerique_enseignants']) and !empty($_POST['animations_ponctuelles_numeriques']) and !empty($_POST['proportion_enseignants_suivi_formation_numerique']) and !empty($_POST['actions_informations_sensibilisations_numeriques_EMI_enseignants']) and !empty($_POST['proportion_enseignants_mutualisant_pratique_numerique']) and !empty($_POST['identification_personne_veille_numerique_pour_enseignants']) and !empty($_POST['formation_parent_usages_numeriques']) and !empty($_POST['formation_parents_usages_responsables_numeriques']) and !empty($_POST['nb_points_total']) and !empty($_POST['palier_form']))
	{
	
		$form = $_POST['formation_numerique_direction'] ;
		$form_num = $_POST['formation_numerique_vie_scolaire'] ;
		$form_adm = $_POST['formation_numerique_administration_personnel_technique'] ;
		$form_ens = $_POST['formation_numerique_enseignants'] ;
		$anim = $_POST['animations_ponctuelles_numeriques'] ;
		$pro = $_POST['proportion_enseignants_suivi_formation_numerique'] ;
		$act = $_POST['actions_informations_sensibilisations_numeriques_EMI_enseignants'] ;
		$prop_ens = $_POST['proportion_enseignants_mutualisant_pratique_numerique'] ;
		$id = $_POST['identification_personne_veille_numerique_pour_enseignants'] ;
		$form_par = $_POST['formation_parent_usages_numeriques'] ;
		$form_parent = $_POST['formation_parents_usages_responsables_numeriques'] ;
		$points = $_POST['nb_points_total'] ;
		$palier = $_POST['palier_form'] ;


		$requete1 = $bdd->prepare('UPDATE formation SET formation_numerique_direction = :formation, formation_numerique_vie_scolaire = :form_vie, formation_numerique_administration_personnel_technique = :form_admin, formation_numerique_enseignants = :form_enseignant, animations_ponctuelles_numeriques = :animation, proportion_enseignants_suivi_formation_numerique = :proportion, actions_informations_sensibilisations_numeriques_EMI_enseignants = :action, proportion_enseignants_mutualisant_pratique_numerique = :proportion_ens, identification_personne_veille_numerique_pour_enseignants = :identifiant, formation_parent_usages_numeriques = :form_parent,  formation_parents_usages_responsables_numeriques = :form_parent_resp, nb_points_total = :pts, palier_form = :pale WHERE RNE = "'.$_POST['RNE'].'"');

		$requete1->execute(array(
			'formation' => $form,
			'form_vie' => $form_num,
			'form_admin' => $form_adm,
			'form_enseignant' => $form_ens,
			'animation' => $anim,
			'proportion' => $pro,
			'action' => $act,
			'proportion_ens' => $prop_ens,
			'identifiant' => $id,
			'form_parent' => $form_par,
			'form_parent_resp' => $form_parent,
			'pts' => $points,
			'pale' => $palier
				));	

			header("refresh:0;url=confirm_modif_form.php?RNE=".$RNE."") ;
	}
	else
	{
		header('refresh:0;url=modif_form.php') ;
	}
?>
</div>
</section>