<?php
	ob_start() ;
	include('header.php');
	
	$requete1 = $bdd->query('SELECT * FROM equipements WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$requete2 = $bdd->query('SELECT * FROM infrastructures WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$requete3 = $bdd->query('SELECT * FROM services WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$requete4 = $bdd->query('SELECT * FROM pilotage WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$requete5 = $bdd->query('SELECT * FROM formation WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$requete6 = $bdd->query('SELECT * FROM utilisations WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$requete7 = $bdd->query('SELECT * FROM usages WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$resume = $bdd->query(' SELECT palier_equip, palier_form, palier_infra, palier_pilo, palier_serv, palier_usa, palier_uti FROM equipements, formation, infrastructures, pilotage, services, usages, utilisations WHERE equipements.RNE = "'.$_SESSION['RNE'].'" AND formation.RNE = "'.$_SESSION['RNE'].'" AND infrastructures.RNE = "'.$_SESSION['RNE'].'" AND pilotage.RNE = "'.$_SESSION['RNE'].'" AND services.RNE = "'.$_SESSION['RNE'].'" AND usages.RNE = "'.$_SESSION['RNE'].'" AND utilisations.RNE = "'.$_SESSION['RNE'].'"');
	$NomEtab = $_SESSION['NomEtab'];

setlocale(LC_TIME, 'fra_fra');
date_default_timezone_set('Europe/Paris');
?>

	<!-- 
	Page Equipements
	-->

<style>
img.img_gauche
{
	width: 25% ;
	height:80%;
	float: left ;
}
		
img.img_droite
{
	width:20% ;
	height:90%;
	float: right ;
	margin-top: -15 ;
}
		

h2
{
	color : #02A4E4;
	text-align:center;
	margin-top: 0px;
}

h3
{
	color: #E54986 ;
	text-align:center;			
}
		
h4
{
	color : #4A51A9;
    margin-bottom: 3px;
    margin-top: 3px;
    text-align:center;
}

h5
{
	color : #4A51A9;
	position: absolute;
	bottom : 13px;
	text-align:right; 
}

.top
{
	height: 50px ;
}

.corps
{
		width: 95%;
}
		
table
{
	border: 1px #4A51A9 solid;
	border-collapse : collapse ;
	width: 79%;
	text-align:center;
}

th
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:78%;
	margin-top: -10px;
}

th.mod
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:30%;
}

th.critere
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:80%;
}

th.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:19%;
}

th.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:1%;
}

th.donnee
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:15%;
}

th.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:5%;
}

th.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:10%;
}

th.domaine
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

th.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

td
{
	border: 1px #4A51A9 solid ;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnee
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:20%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:1%;
}

td.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:5%;
}

td.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:5%;
}

td.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:10%;
}

/*
Equipements
*/

th.critere_equip
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:80%;
}

th.points_equip
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:19%;
}

th.points_total_equip
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

th.donnee_equip
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:15%;
}

th.palier_equip
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:15%;
}

th.part_equip
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

td.points_equip
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnee_equip
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:20%;
}

td.points_equip
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.points_total_equip
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:20%;
}

td.palier_equip
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:15%;
}

td.part_equip
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:5%;
}

/*
Infrastructures
*/

th.critere_infra
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:65%;
}

th.points_infra
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:19%;
}

th.points_total_infra
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

th.donnee_infra
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:40%;
}

th.palier_infra
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:15%;
}

th.part_infra
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

td.points_infra
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnee_infra
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points_infra
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.points_total_infra
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:20%;
}

td.palier_infra
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:15%;
}

td.part_infra
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:5%;
}

/*
Services
*/

td.points_serv
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnee_serv
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points_serv
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.points_total_serv
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:20%;
}

td.palier_serv
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:15%;
}

td.part_serv
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:5%;
}

th.critere_serv
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:65%;
}

th.points_serv
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:19%;
}

th.points_total_serv
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

th.donnee_serv
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:40%;
}

th.palier_serv
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:15%;
}


th.part_serv
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

/*
Pilotage
*/

th.critere_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:65%;
}

th.points_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:19%;
}

th.points_total_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

th.donnee_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:40%;
}

th.palier_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:15%;
}

th.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:10%;
}

th.part_pilo
{
	border: 1px #4A51A9 solid ;
	color : #00A2E1;
	vertical-align: middle ;
	width:30%;
}

td.points_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnee_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.points_total_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:20%;
}

td.palier_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:15%;
}

td.part_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:5%;
}

/*
Formation
*/

	</style>

	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Equipements - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th class="critere_equip"><h4>Critères</h4></th>
						<th colspan="2" class="donnee_equip"><h4>Données</h4></th>
						<th class="points_equip"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete1->fetch())
			{
		?>

	<tr><th class="critere_equip">Nombre de terminaux (ordinateur fixe, ordinateur portable, tablette)</th><td colspan="2" class="donnee_equip"><?php echo $donnees['nb_terminaux_donnees']; ?></td><td class="points_equip"><?php echo $donnees['nb_terminaux_points']; ?> sur 10</td></tr>

	<tr><th class="critere_equip">Nombre de terminaux mobiles (ordinateur portable, tablette, poste de classes mobiles)</th><td colspan="2" class="donnee_equip"><?php echo $donnees['nb_terminaux_mobiles_donnees']; ?></td><td class="points_equip"><?php echo $donnees['nb_terminaux_mobiles_points']; ?> sur 20</td></tr>

	<tr><th class="critere_equip">Nombre de postes de travail en accès libre aux élèves en dehors des cours (hors classe)</th><td colspan="2" class="donnee_equip"><?php echo $donnees['nb_postes_libres_donnees']; ?></td><td class="points_equip"><?php echo $donnees['nb_postes_libres_points']; ?> sur 20</td></tr>

	<tr><th class="critere_equip">Nombre de terminaux de moins de cinq ans</th><td colspan="2" class="donnee_equip"><?php echo $donnees['nb_terminaux_moins_5_ans_donnees']; ?></td><td class="points_equip"><?php echo $donnees['nb_terminaux_moins_5_ans_points']; ?> sur 15</td></tr>

	<tr><th class="critere_equip">Nombre de VPI/TNI/TBI</th><td colspan="2" class="donnee_equip"><?php echo $donnees['nb_VPI_TNI_TBI_donnees']; ?></td><td class="points_equip"><?php echo $donnees['nb_VPI_TNI_TBI_points']; ?> sur 15</td></tr>

	<tr><th class="critere_equip">Nombre de vidéo-projecteurs</th><td colspan="2" class="donnee_equip"><?php echo $donnees['nb_video_projecteur_donnees']; ?></td><td class="points_equip"><?php echo $donnees['nb_video_projecteur_points']; ?> sur 5</td></tr>

	<tr><th class="critere_equip">Dotation des élèves en terminaux mobiles (ordinateur portable, tablette) par la collectivité</th><td colspan="2" class="donnee_equip"><?php echo $donnees['dotation_eleves_terminaux_mobiles_mod']; ?></td><td class="points_equip"><?php echo $donnees['dotation_eleves_terminaux_mobiles_points']; ?> sur 10</td></tr>

	<tr><th class="critere_equip">Dotation des enseignants en terminaux mobiles (ordinateur portable, tablette) par la collectivité</th><td colspan="2" class="donnee_equip"><?php echo $donnees['dotation_enseignants_terminaux_mobiles_mod']; ?></td><td class="points_equip"><?php echo $donnees['dotation_enseignants_terminaux_mobiles_points']; ?> sur 15</td></tr>

	<tr><th rowspan="7" class="critere_equip">Equipements particuliers</th><th class="part_equip">Malette MP3/MP4</th><td class="part_equip"><?php echo $donnees['equipements_particuliers_malette']; ?></td><td rowspan="7" class="points_equip"><?php echo $donnees['equipements_particuliers_points']; ?> sur 15</td></tr>
	<tr><th class="part_equip">Imprimante 3D</th><td class="part_equip"><?php echo $donnees['equipements_particuliers_imprimante']; ?></td></tr>
	<tr><th class="part_equip">FabLab</th><td class="part_equip"><?php echo $donnees['equipements_particuliers_fablab']; ?></td></tr>
	<tr><th class="part_equip">Visioconférence</th><td class="part_equip"><?php echo $donnees['equipements_particuliers_visio']; ?></td></tr>
	<tr><th class="part_equip">Labo Multimédia</th><td class="part_equip"><?php echo $donnees['equipements_particuliers_labo']; ?></td></tr>
	<tr><th class="part_equip">Boîtiers de vote</th><td class="part_equip"><?php echo $donnees['equipements_particuliers_boitier']; ?></td></tr>
	<tr><th class="part_equip">Visualisateurs</th><td class="part_equip"><?php echo $donnees['equipements_particuliers_visu']; ?></td></tr>

	<tr><th class="critere_equip">Maintenance des équipements (maintien en condition opérationnelle) par la collectivité</th><td colspan="2"  class="donnee_equip"><?php echo $donnees['maintenance_equipement_mod']; ?></td><td class="points_equip"><?php echo $donnees['maintenance_equipement_points']; ?> sur 20</td></tr>

	<tr><th class="critere_equip">L'EPLE engage-til des moyens propres (budget, décharge, IMP, etc.) sur la maintenance quotidienne des équipements ?</th><td colspan="2" class="donnee_equip"><?php echo $donnees['engagement_EPLE_mod']; ?></td><td class="points_equip"><?php echo $donnees['engagement_EPLE_points']; ?> sur 6</td></tr>

				</tbody>
			</table>
			<br><br>
	<table align="center">
		<thead>
			<tr>
				<th class="points_total_equip"><h4>Nombre de points</h4></th>
				<th class="palier_equip"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total_equip"><?php echo $donnees['nb_points_total']; ?> sur 151</td><td class="palier_equip"><?php echo $donnees['palier_equip'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
		
	</page>

	<!-- 
	Page Infrastructures
	-->

	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Infrastructures - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th class="critere_infra"><h4>Critères</h4></th>
						<th colspan="2" class="donnee_infra"><h4>Données</h4></th>
						<th class="points_infra"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete2->fetch())
			{
		?>

			<tr><th class="critere_infra">Nombre de salles avec au moins un accès réseau</th><td colspan="2" class="donnee_infra"><?php echo $donnees['proportion_acces_reseau_salles_donnees']; ?></td><td class="points_infra"><?php echo $donnees['proportion_acces_reseau_salles_points']; ?> sur 20</td></tr>

			<tr><th class="critere_infra">Débit d'accès à Internet</th><td colspan="2" class="donnee_infra"><?php echo $donnees['debit_acces_internet_mod']; ?></td><td class="points_infra"><?php echo $donnees['debit_acces_internet_points']; ?> sur 20</td></tr>

			<tr><th class="critere_infra">Débit du réseau interne</th><td colspan="2" class="donnee_infra"><?php echo $donnees['debit_reseau_interne_mod']; ?></td><td class="points_infra"><?php echo $donnees['debit_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th class="critere_infra">Évaluation de la qualité du débit Internet par rapport aux besoins</th><td colspan="2" class="donnee_infra"><?php echo $donnees['evaluation_qualite_debit_internet_mod']; ?></td><td class="points_infra"><?php echo $donnees['evaluation_qualite_debit_internet_points']; ?> sur 20</td></tr>

			<tr><th class="critere_infra">Évaluation de la qualité du débit du réseau interne par rapport aux besoins</th><td colspan="2" class="donnee_infra"><?php echo $donnees['evaluation_qualite_debit_reseau_interne_mod']; ?></td><td class="points_infra"><?php echo $donnees['evaluation_qualite_debit_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th rowspan="4" class="critere_infra">Réseau pédagogique</th><th class="part_infra">Pare-feu</th><td class="part_infra"><?php echo $donnees['reseau_pedagogique_pare_feu']; ?></td><td rowspan="4" class="points_infra"><?php echo $donnees['reseau_pedagogique_points']; ?> sur 20</td></tr>
			<tr><th class="part_infra">Antivirus</th><td class="part_infra"><?php echo $donnees['reseau_pedagogique_antivirus']; ?></td></tr>
			<tr><th class="part_infra">Filtrage</th><td class="part_infra"><?php echo $donnees['reseau_pedagogique_filtrage']; ?></td></tr>
			<tr><th class="part_infra">Contrôle a posteriori des accès</th><td class="part_infra"><?php echo $donnees['reseau_pedagogique_controle']; ?></td></tr>

			<tr><th class="critere_infra">Proportion des espaces (hors salle de classe) couverts par WiFi</th><td colspan="2" class="donnee_infra"><?php echo $donnees['proportion_wifi_mod']; ?></td><td class="points_infra"><?php echo $donnees['proportion_wifi_points']; ?> sur 10</td></tr>

			<tr><th class="critere_infra">Nombre de salles d'enseignement couvertes par un réseau WiFi</th><td colspan="2" class="donnee_infra"><?php echo $donnees['proportion_wifi_salles_enseignements_donnees']; ?></td><td class="points_infra"><?php echo $donnees['proportion_wifi_salles_enseignements_points']; ?> sur 20</td></tr>

			<tr><th class="critere_infra">Évaluation de la couverture du réseau WiFi par rapport aux besoins</th><td colspan="2" class="donnee_infra"><?php echo $donnees['evaluation_reseau_wifi_mod']; ?></td><td class="points_infra"><?php echo $donnees['evaluation_reseau_wifi_points']; ?> sur 10</td></tr>

			<tr><th class="critere_infra">Possibilité pour les utilisateurs de recharger les équipements mobiles</th><td colspan="2" class="donnee_infra"><?php echo $donnees['possibilite_recharger_equipements_mobiles_mod']; ?></td><td class="points_infra"><?php echo $donnees['possibilite_recharger_equipements_mobiles_points']; ?> sur 10</td></tr>

				</tbody>
			</table>
			<br><br>
	<table align="center">
		<thead>
			<tr>
				<th class="points_total_infra"><h4>Nombre de points</h4></th><th class="palier_infra"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total_infra"><?php echo $donnees['nb_points_total']; ?> sur 170</td><td class="palier_infra"><?php echo $donnees['palier_infra'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
		
	</page>

	<!--
	Page Services
	-->

	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Services - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th class="critere_serv"><h4>Critères</h4></th>
						<th colspan="2" class="donnee_serv"><h4>Données</h4></th>
						<th class="points_serv"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete3->fetch())
			{
		?>

			<?php if ($donnees['ENT_mod'] == "OUI")
			{
				?>
			<tr><th rowspan="3" class="critere_serv">ENT - Espace Numérique de Travail - issu d'un projet académique et/ou des collectivités associées</th><td colspan="2" class="donnee_serv"><?php echo $donnees['ENT_mod']; ?></td><td rowspan="3"  class="points_serv"><?php echo $donnees['ENT_points']; ?> sur 24</td></tr>
			<tr><th class="part_serv">Déploiement à tous les utilisateurs</th><td class="part_serv"><?php echo $donnees['ENT_utilisateur']; ?></td></tr>
			<tr><th class="part_serv">Déploiement de tous les services</th><td class="part_serv"><?php echo $donnees['ENT_services']; ?></td></tr>

			<?php
			}
			else
			{
				?>

			<tr><th class="critere_serv">ENT - Espace Numérique de Travail - issu d'un projet académique et/ou des collectivités associées</th><td colspan="2" class="donnee_serv"><?php echo $donnees['ENT_mod']; ?></td><td class="points_serv"><?php echo $donnees['ENT_points']; ?> sur 24</td></tr>

			<?php
			}
			?>

			<tr><th class="critere_serv">Site web de l'établissement</th><td colspan="2" class="donnee_serv"><?php echo $donnees['site_web_etablissement_mod']; ?></td><td class="points_serv"><?php echo $donnees['site_web_etablissement_points']; ?> sur 10</td></tr>

			<tr><th class="critere_serv">Notes, absences, emploi du temps</th><td colspan="2" class="donnee_serv"><?php echo $donnees['notes_abscence_mod']; ?></td><td class="points_serv"><?php echo $donnees['notes_abscence_points']; ?> sur 10</td></tr>
			
			<tr><th class="critere_serv">Appel dématérialisé des élèves au début de l'heure de cours</th><td colspan="2" class="donnee_serv"><?php echo $donnees['appel_dematerialise_mod']; ?></td><td class="points_serv"><?php echo $donnees['appel_dematerialise_points']; ?> sur 15</td></tr>
			
			<tr><th class="critere_serv">Messagerie interne</th><td colspan="2" class="donnee_serv"><?php echo $donnees['messagerie_interne_mod']; ?></td><td class="points_serv"><?php echo $donnees['messagerie_interne_points']; ?> sur 10</td></tr>
			
			<tr><th class="critere_serv">LSUN - Livret Scolaire Unique Numérique<br>LPC - Livret Personnel de Compétences</th><td colspan="2" class="donnee_serv"><?php echo $donnees['LSUN_LPC_mod']; ?></td><td class="points_serv"><?php echo $donnees['LSUN_LPC_points']; ?> sur 15</td></tr>
			
			<tr><th class="critere_serv">Service de réservation de ressources</th><td colspan="2" class="donnee_serv"><?php echo $donnees['resa_ressources_mod']; ?></td><td class="points_serv"><?php echo $donnees['resa_ressources_points']; ?> sur 5</td></tr>
			
			<tr><th class="critere_serv">Abonnements à des services d'information et de documentation en ligne</th><td colspan="2" class="donnee_serv"><?php echo $donnees['abonnements_services_information_en_ligne_mod']; ?></td><td class="points_serv"><?php echo $donnees['abonnements_services_information_en_ligne_points']; ?> sur 15</td></tr>
			
			<tr><th class="critere_serv">Folios</th><td colspan="2" class="donnee_serv"><?php echo $donnees['folios_mod']; ?></td><td class="points_serv"><?php echo $donnees['folios_points']; ?> sur 15</td></tr>
			
			<tr><th class="critere_serv">Abonnements à des ressources numériques pédagogiques éditoriales</th><td colspan="2" class="donnee_serv"><?php echo $donnees['abonnements_ressources_numeriques_pedagogiques_mod']; ?></td><td class="points_serv"><?php echo $donnees['abonnements_ressources_numeriques_pedagogiques_points']; ?> sur 15</td></tr>
			
			<tr><th class="critere_serv">Proportion de ressources numériques par rapport aux ressources papiers</th><td colspan="2" class="donnee_serv"><?php echo $donnees['proportion_ressources_numeriques_papiers_mod']; ?></td><td class="points_serv"><?php echo $donnees['proportion_ressources_numeriques_papiers_points']; ?> sur 20</td></tr>

				</tbody>
			</table>
			<br><br>
	<table align="center">
		<thead>
			<tr>
				<th class="points_total_serv"><h4>Nombre de points</h4></th><th class="palier_serv"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total_serv"><?php echo $donnees['nb_points_total']; ?> sur 154</td><td class="palier_serv"><?php echo $donnees['palier_serv'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
		
	</page>

	<!--
	Page Pilotage
	-->

	<page backtop="60px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Pilotage - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th class="critere_pilo"><h4>Critères</h4></th>
						<th colspan="2" class="donnee_pilo"><h4>Données</h4></th>
						<th class="points_pilo"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete4->fetch())
			{
		?>

			<tr><th class="critere_pilo">Volet numérique au sein du projet d'établissement</th><td colspan="2" class="donnee_pilo"><?php echo $donnees['volet_numerique_projet_mod']; ?></td><td class="points_pilo"><?php echo $donnees['volet_numerique_projet_points']; ?> sur 10</td></tr>
			
			<tr><th class="critere_pilo">Charte d'usage du numérique</th><td colspan="2" class="donnee_pilo"><?php echo $donnees['charte_usage_numerique_mod']; ?></td><td class="points_pilo"><?php echo $donnees['charte_usage_numerique_points']; ?> sur 20</td></tr>
			
			<tr><th class="critere_pilo">Commissions numérique ou conseil pédagogique traitant des sujets numériques</th><td colspan="2" class="donnee_pilo"><?php echo $donnees['commission_numerique_mod']; ?></td><td class="points_pilo"><?php echo $donnees['commission_numerique_points']; ?> sur 20</td></tr>
			
			<tr><th class="critere_pilo">Personnel(s) référent(s) numérique(s) pour les usages pédagogiques du numérique</th><td colspan="2" class="donnee_pilo"><?php echo $donnees['personnels_referents_numeriques_usages_pedagogiques_mod']; ?></td><td class="points_pilo"><?php echo $donnees['personnels_referents_numeriques_usages_pedagogiques_points']; ?> sur 15</td></tr>
			
			<tr><th class="critere_pilo">Utilisation d'Indemnités de Mission Particulière (IMP) pour favoriser les usages du numérique</th><td colspan="2" class="donnee_pilo"><?php echo $donnees['utilisation_IMP_usages_numeriques_mod']; ?></td><td class="points_pilo"><?php echo $donnees['utilisation_IMP_usages_numeriques_points']; ?> sur 10</td></tr>
			
			<tr><th class="critere_pilo">Démarche "zéro papier inutile"</th><td colspan="2" class="donnee_pilo"><?php echo $donnees['demarche_zero_papier_inutile_mod']; ?></td><td class="points_pilo"><?php echo $donnees['demarche_zero_papier_inutile_points']; ?> sur 10</td></tr>
			
			<tr><th class="critere_pilo">Usage privilégié du numérique par l'équipe de direction et l'équipe administrative</th><td colspan="2" class="donnee"><?php echo $donnees['usage_privilegie_numerique_direction_administration_mod']; ?></td><td class="points"><?php echo $donnees['usage_privilegie_numerique_direction_administration_points']; ?> sur 10</td></tr>
			
			<tr><th class="critere_pilo">Équipements numériques personnels des professeurs acceptés<br>sur le réseau de l'établissement</th><td colspan="2" class="donnee_pilo"><?php echo $donnees['equipements_numeriques_personnels_enseignants_acceptes_mod']; ?></td><td class="points_pilo"><?php echo $donnees['equipements_numeriques_personnels_enseignants_acceptes_points']; ?> sur 20</td></tr>
			
			<tr><th class="critere_pilo">Équipements numériques personnels des élèves acceptés<br>(pour des usages pédagogiques)sur le réseau de l'établissement</th><td colspan="2" class="donnee_pilo"><?php echo $donnees['equipements_numeriques_personnels_eleves_acceptes_mod']; ?></td><td class="points_pilo"><?php echo $donnees['equipements_numeriques_personnels_eleves_acceptes_points']; ?> sur 5</td></tr>
			
			<tr><th class="critere_pilo">Sécurité - Identifiants personnels d'accès au réseau</th><td colspan="2" class="donnee_pilo"><?php echo $donnees['securite_id_personnels_acces_reseau_mod']; ?></td><td class="points_pilo"><?php echo $donnees['securite_id_personnels_acces_reseau_points']; ?> sur 20</td></tr>

			<tr><th rowspan="3" class="critere_pilo">Actions d'information et d'accompagnement des usagers au numérique</th><th class="part_pilo">Avec les moyens de l'établissement (personnel référent)</th><td class="part_pilo"><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_etab']; ?></td><td rowspan="3" class="points_pilo"><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_points']; ?> sur 18</td></tr>
			<tr><th class="part_pilo">Par les services académiques</th><td class="part_pilo"><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_aca']; ?></td></tr>
			<tr><th class="part_pilo">Par la collectivité</th><td class="part_pilo"><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_collec']; ?></td></tr>
		
			<tr><th rowspan="3" class="critere_pilo">Assistance aux usagers</th><th class="part_pilo">Avec les moyens de l'établissement (personnel référent)</th><td class="part_pilo"><?php echo $donnees['assistance_usagers_etab']; ?></td><td rowspan="3" class="points_pilo"><?php echo $donnees['assistance_usagers_points']; ?> sur 18</td></tr>
			<tr><th class="part">Par les services académiques</th><td class="part"><?php echo $donnees['assistance_usagers_aca']; ?></td></tr>
			<tr><th class="part">Par la collectivité</th><td class="part"><?php echo $donnees['assistance_usagers_collec']; ?></td></tr>
			
			<tr><th class="critere_pilo">Suivi statistique des taux d'utilisation des équipements et des services</th><td colspan="2" class="donnee_pilo"><?php echo $donnees['suivi_statistique_taux_utilisation_equipements_services_mod']; ?></td><td class="points_pilo"><?php echo $donnees['suivi_statistique_taux_utilisation_equipements_services_points']; ?> sur 10</td></tr>
			
			<tr><th class="critere_pilo">Mise en place et suivi des "Services en Ligne"</th><td colspan="2" class="donnee_pilo"><?php echo $donnees['mise_en_place_suivi_services_en_ligne_mod']; ?></td><td class="points_pilo"><?php echo $donnees['mise_en_place_suivi_services_en_ligne_points']; ?> sur 20</td></tr>
			
			<tr><th class="critere_pilo">Échanges entre le chef d'établissement et les corps d'inspection pour le développement des usages du numérique</th><td colspan="2" class="donnee_pilo"><?php echo $donnees['echanges_chef_etablissement_inspection_developpement_mod']; ?></td><td class="points_pilo"><?php echo $donnees['echanges_chef_etablissement_inspection_developpement_points']; ?> sur 15</td></tr>

				</tbody>
			</table>
			<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
	<table align="center">
		<thead>
			<tr>
				<th class="points_total_pilo"><h4>Nombre de points</h4></th><th class="palier_pilo"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total_pilo"><?php echo $donnees['nb_points_total']; ?> sur 221</td><td class="palier_pilo"><?php echo $donnees['palier_pilo'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
		
	</page>

	<!--
	Page Formation
	-->

<style>
img.img_gauche
{
	width: 25% ;
	height:80%;
	float: left ;
}
		
img.img_droite
{
	width:20% ;
	height:90%;
	float: right ;
	margin-top: -15 ;
}
		

h2
{
	color : #02A4E4;
	text-align:center;
	margin-top: 0px;
}

h3
{
	color: #E54986 ;
	text-align:center;			
}
		
h4
{
	color : #4A51A9;
    margin-bottom: 3px;
    margin-top: 3px;
    text-align:center;
}

.top
{
	height: 50px ;
}

.corps
{
		width: 95%;
}
		
table
{
	border: 1px #4A51A9 solid;
	border-collapse : collapse ;
	width: 79%;
	text-align:center;
}

th
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:78%;
	margin-top: -10px;
}

th.mod
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:30%;
}

th.critere
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:65%;
}

th.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:19%;
}

th.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:80%;
}

th.donnee
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:40%;
}

th.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:50%;
}

th.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:10%;
}

th.domaine
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

th.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

td
{
	border: 1px #4A51A9 solid ;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnee
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:80%;
}

td.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:50%;
}

td.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:5%;
}
	</style>

	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Formation - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th class="critere"><h4>Critères</h4></th>
						<th class="donnee"><h4>Modalité retenue</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete5->fetch())
			{
		?>

			<tr><th class="critere">Formation au numérique et/ou aux aspects juridiques du numérique d'un ou de plusieurs membres de l'équipe de direction (au cours des trois dernières années)</th><td class="donnee"><?php echo $donnees['formation_numerique_direction_mod']; ?></td><td class="points"><?php echo $donnees['formation_numerique_direction_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Formation au numérique d'un ou plusieurs membres du personnel de vie scolaire (au cours des trois dernières années)</th><td class="donnee"><?php echo $donnees['formation_numerique_vie_scolaire_mod']; ?></td><td class="points"><?php echo $donnees['formation_numerique_vie_scolaire_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Formation au numérique d'un ou de plusieurs membres du personnel administratif et/ou technique (au cours des trois dernières années)</th><td class="donnee"><?php echo $donnees['formation_numerique_administration_personnel_technique_mod']; ?></td><td class="points"><?php echo $donnees['formation_numerique_administration_personnel_technique_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Formation de l'équipe enseignante aux usages pédagogiques du numérique sur site et par un formateur académique (au cours des trois dernières années)</th><td class="donnee"><?php echo $donnees['formation_numerique_enseignants_mod']; ?></td><td class="points"><?php echo $donnees['formation_numerique_enseignants_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Animations ponctuelles autour des usages du numérique par un référent numérique (personne ressource) dans l'établissement  (au cours des trois dernières années) :</th><td class="donnee"><?php echo $donnees['animations_ponctuelles_numeriques_mod']; ?></td><td class="points"><?php echo $donnees['animations_ponctuelles_numeriques_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Proportion de professeurs ayant suivi une formation (PAF, M@gistère) aux usages pédagogiques du numérique (au cours des trois dernières années)</th><td class="donnee"><?php echo $donnees['proportion_enseignants_suivi_formation_numerique_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_suivi_formation_numerique_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Actions d'information et de sensibilisation aux usages responsables du numérique et à l'Éducation aux Médias et à l'Information (EMI) à destination des enseignants (au cours des trois dernières années)</th><td class="donnee"><?php echo $donnees['actions_informations_sensibilisations_EMI_enseignants_mod']; ?></td><td class="points"><?php echo $donnees['actions_informations_sensibilisations_EMI_enseignants_points']; ?> sur 10</td></tr>

			<tr><th class="critere">Proportion d'enseignants mutualisant leurs pratiques du numérique et/ou collaborant via un ou des réseau(x) numériques)</th><td class="donnee"><?php echo $donnees['proportion_enseignants_mutualisant_pratique_numerique_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_mutualisant_pratique_numerique_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Identification dans l'établissement d'une personne chargée de la veille et de la curation du numérique à destination des enseignants</th><td class="donnee"><?php echo $donnees['identification_personne_veille_numerique_pour_enseignants_mod']; ?></td><td class="points"><?php echo $donnees['identification_personne_veille_numerique_pour_enseignants_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Formation des parents aux usages des services numériques</th><td class="donnee"><?php echo $donnees['formation_parent_usages_numeriques_mod']; ?></td><td class="points"><?php echo $donnees['formation_parent_usages_numeriques_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Formation des parents aux usages responsables des services numériques</th><td class="donnee"><?php echo $donnees['formation_parents_usages_responsables_numeriques_mod']; ?></td><td class="points"><?php echo $donnees['formation_parents_usages_responsables_numeriques_points']; ?> sur 15</td></tr>

				</tbody>
			</table>
			<br><br>

			<table align="center" border="none">
			<tr>
	<td border="none">
	<table>
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 175</td><td class="palier"><?php echo $donnees['palier_form'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	</td>
	</tr></table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>

	<!--
	Page Utilisations
	-->

<style>
img.img_gauche
{
	width: 25% ;
	height:80%;
	float: left ;
}
		
img.img_droite
{
	width:20% ;
	height:90%;
	float: right ;
	margin-top: -15 ;
}
		

h2
{
	color : #02A4E4;
	text-align:center;
	margin-top: 0px;
}

h3
{
	color: #E54986 ;
	text-align:center;			
}
		
h4
{
	color : #4A51A9;
    margin-bottom: 3px;
    margin-top: 3px;
    text-align:center;
}

.top
{
	height: 50px ;
}

.corps
{
		width: 95%;
}
		
table
{
	border: 1px #4A51A9 solid;
	border-collapse : collapse ;
	width: 79%;
	text-align:center;
}

th
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:78%;
	margin-top: -10px;
}

th.mod
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:30%;
}

th.critere
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:65%;
}

th.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:19%;
}

th.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:80%;
}

th.donnee
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:40%;
}

th.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:50%;
}

th.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:10%;
}

th.domaine
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

th.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

td
{
	border: 1px #4A51A9 solid ;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnee
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:80%;
}

td.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:50%;
}

td.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:5%;
}
	</style>

	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Utilisations - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th class="critere"><h4>Critères</h4></th>
						<th class="donnee"><h4>Modalité retenue</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete6->fetch())
			{
		?>

			<tr><th class="critere">Taux de réservation des salles informatiques</th><td class="donnee"><?php echo $donnees['taux_reservation_salles_informatiques_mod']; ?></td><td class="points"><?php echo $donnees['taux_reservation_salles_informatiques_points']; ?> sur 10</td></tr>

			<tr><th class="critere">Taux de réservation des classes mobiles</th><td class="donnee"><?php echo $donnees['taux_reservation_classes_mobiles_mod']; ?></td><td class="points"><?php echo $donnees['taux_reservation_classes_mobiles_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Proportion d'enseignants utilisant régulièrement des VPI/TNI/TBI</th><td class="donnee"><?php echo $donnees['proportion_enseignants_utilisants_VPI_TNI_TBI_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_utilisants_VPI_TNI_TBI_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Proportion de professeurs remplissant le service de notes au moins mensuellement</th><td class="donnee"><?php echo $donnees['proportion_enseignants_rempli_service_notes_mensuellement_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_rempli_service_notes_mensuellement_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Proportion de professeurs remplissant le cahier de textes en ligne à l'issue de chaque cours</th><td class="donnee"><?php echo $donnees['proportion_enseignants_remplissant_cahier_texte_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_remplissant_cahier_texte_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Proportion de parents d'élèves consultant les services de vie scolaire au moins une fois par semaine</th><td class="donnee"><?php echo $donnees['proportion_parent_consultant_services_vie_scolaire_mod']; ?></td><td class="points"><?php echo $donnees['proportion_parent_consultant_services_vie_scolaire_points']; ?> sur 10</td></tr>

			<tr><th class="critere">Proportion d'élèves se connectant chaque jour au réseau interne</th><td class="donnee"><?php echo $donnees['proportion_eleve_connexion_reseau_interne_mod']; ?></td><td class="points"><?php echo $donnees['proportion_eleve_connexion_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Fréquentation du site public de l'établissement</th><td class="donnee"><?php echo $donnees['frequentation_site_public_etablissements_mod']; ?></td><td class="points"><?php echo $donnees['frequentation_site_public_etablissements_points']; ?> sur 10</td></tr>

			<tr><th class="critere">Équipements numériques utilisés régulièrement par des personnes extérieures à l'établissement</th><td class="donnee"><?php echo $donnees['equipement_numeriques_utilise_regu_personne_exter_mod']; ?></td><td class="points"><?php echo $donnees['equipement_numeriques_utilise_regu_personne_exter_points']; ?> sur 5</td></tr>

				</tbody>
			</table>
			<br><br>

			<table align="center" border="none">
			<tr>
	<td border="none">
	<table>
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 135</td><td class="palier"><?php echo $donnees['palier_uti'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	</td>
	</tr></table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>

	<!--
	Page Usage
	-->

	<style>
img.img_gauche
{
	width: 25% ;
	height:80%;
	float: left ;
}
		
img.img_droite
{
	width:20% ;
	height:90%;
	float: right ;
	margin-top: -15 ;
}
		

h2
{
	color : #02A4E4;
	text-align:center;
	margin-top: 0px;
}

h3
{
	color: #E54986 ;
	text-align:center;			
}
		
h4
{
	color : #4A51A9;
    margin-bottom: 3px;
    margin-top: 3px;
    text-align:center;
}

.top
{
	height: 50px ;
}

.corps
{
		width: 95%;
}
		
table
{
	border: 1px #4A51A9 solid;
	border-collapse : collapse ;
	width: 79%;
	text-align:center;
}

th
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:78%;
	margin-top: -10px;
}

th.mod
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:30%;
}

th.critere
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:80%;
}

th.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:17%;
}

th.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:80%;
}

th.donnee
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:30%;
}

th.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:50%;
}

th.domaine
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

th.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

td
{
	border: 1px #4A51A9 solid ;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnee
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:17%;
}

td.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:80%;
}

td.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:50%;
}

td.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:5%;
}
	</style>

	<page backtop="60px" backleft="-2mm" backright="-2mm">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Usages - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th class="critere"><h4>Critères</h4></th>
						<th class="donnee"><h4>Modalité retenue</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete7->fetch())
			{
		?>

			<tr><th class="critere">Pourcentage d'élèves hors classes de 3ème ayant une validation partielle du B2i Collège</th><td class="donnee"><?php echo $donnees['pourcentage_eleves_validation_partielle_B2i_mod']; ?></td><td class="points"><?php echo $donnees['pourcentage_eleves_validation_partielle_B2i_points']; ?> sur 5</td></tr>

			<tr><th class="critere">Proportion de professeurs impliqués dans la certification des compétences numériques des élèves (B2i)</th><td class="donnee"><?php echo $donnees['proportion_enseignants_impliques_certification_competence_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_impliques_certification_competence_points']; ?> sur 5</td></tr>

			<tr><th class="critere">Nombre de disciplines impliquées dans la validation des compétences du B2i</th><td class="donnee"><?php echo $donnees['nb_discipline_validation_competences_B2i_mod']; ?></td><td class="points"> <?php echo $donnees['nb_discipline_validation_competences_B2i_points']; ?> sur 10</td></tr>

			<tr><th class="critere">Proportion de professeurs développant des usages pédagogiques du numérique</th><td class="donnee"><?php echo $donnees['proportion_enseignants_developpant_usage_numerique_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_developpant_usage_numerique_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Proportion de manuels scolaires numériques dans l'EPLE</th><td class="donnee"><?php echo $donnees['proportion_manuels_scolaires_numeriques_EPLE_mod']; ?></td><td class="points"> <?php echo $donnees['proportion_manuels_scolaires_numeriques_EPLE_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Usages de services et ressources pédagogiques numériques institutionnels (D'Col, EFS, éduthèque, Fondamentaux, etc.)</th><td class="donnee"><?php echo $donnees['usages_services_ressources_numeriques_mod']; ?></td><td class="points"> <?php echo $donnees['usages_services_ressources_numeriques_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Usages d'une messagerie institutionnelle (académique ou dans l'ENT) pour les échanges professionnels</th><td class="donnee"><?php echo $donnees['usages_messagerie_institutionnelle_echanges_professionnel_mod']; ?></td><td class="points"><?php echo $donnees['usages_messagerie_institutionnelle_echanges_professionnel_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Usages de la messagerie de l'ENT pour les échanges des professeurs avec les familles (si ENT)</th><td class="donnee"><?php echo $donnees['usages_messagerie_ENT_enseignants_famille_mod']; ?></td><td class="points"><?php echo $donnees['usages_messagerie_ENT_enseignants_famille_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Usages d'espaces de stockage ou de partage de documents pédagogiques</th><td class="donnee"><?php echo $donnees['usages_espaces_stockages_partage_documents_pedagogiques_mod']; ?></td><td class="points"><?php echo $donnees['usages_espaces_stockages_partage_documents_pedagogiques_points']; ?> sur 10</td></tr>

			<tr><th class="critere">Usages d'outils numériques pour des pratiques collaboratives entre ensiegnants</th><td class="donnee"><?php echo $donnees['usages_outils_numeriques_entre_enseignants_mod']; ?></td><td class="points"> <?php echo $donnees['usages_outils_numeriques_entre_enseignants_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Usages d'outils numériques pour des pratiques collaboratives des enseignants avec leurs élèves</th><td class="donnee"><?php echo $donnees['usages_outils_numeriques_entre_enseignants_eleves_mod']; ?></td><td class="points"><?php echo $donnees['usages_outils_numeriques_entre_enseignants_eleves_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Usages connus de réseaux sociaux dans le cadre pédagogique</th><td class="donnee"><?php echo $donnees['usages_reseaux_sociaux_pedagogiques_mod']; ?></td><td class="points"><?php echo $donnees['usages_reseaux_sociaux_pedagogiques_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Usages connus de services de publication de type blog pour l'enseignement</th><td class="donnee"><?php echo $donnees['usages_services_publication_pour_enseignement_mod']; ?></td><td class="points"><?php echo $donnees['usages_services_publication_pour_enseignement_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Création de médias numériques (journal, radio, vidéo, exposition) connue et validée par le chef d'établissement</th><td class="donnee"><?php echo $donnees['creation_medias_numeriques_mod']; ?></td><td class="points"><?php echo $donnees['creation_medias_numeriques_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Utilisation d'outils et de ressources numériques pour le développement de la pratique de l'oral (baladodiffusion, labo mutimédia, etc.)</th><td class="donnee"><?php echo $donnees['utilisation_outils_ressources_numeriques_dvlp_oral_mod']; ?></td><td class="points"><?php echo $donnees['utilisation_outils_ressources_numeriques_dvlp_oral_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Usages de services numériques de suivi de la maîtrise des compétences (ex : LPC, LSUN, etc.)</th><td class="donnee"><?php echo $donnees['usages_services_numeriques_suivi_competences_mod']; ?></td><td class="points"><?php echo $donnees['usages_services_numeriques_suivi_competences_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Usages du numérique à des fins de personnalisation des parcours et d'individualisation des enseignements</th><td class="donnee"><?php echo $donnees['usages_numeriques_personnalisation_parcours_individu_mod']; ?></td><td class="points" class="points"><?php echo $donnees['usages_numeriques_personnalisation_parcours_individu_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Usages du numérique dans le cadre de la liaison inter-degré</th><td class="donnee"><?php echo $donnees['usages_numeriques_liaison_inter_degre_mod']; ?></td><td class="points"><?php echo $donnees['usages_numeriques_liaison_inter_degre_points']; ?> sur 20</td></tr>

				</tbody>
			</table>
			<br>

			<table align="center" border="none">
			<tr>
	<td border="none">
	<table>
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 280</td><td class="palier"><?php echo $donnees['palier_usa'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	</td>
	</tr></table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>

	<!--
	Page Resume
	-->

	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Palier - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th class="domaine"><h4>Domaine</h4></th>
						<th class="palier_resum"><h4>Palier</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $resume->fetch())
			{
		?>

			<tr><th class="domaine">Equipements</th><td class="palier_resum"><?php echo $donnees['palier_equip']; ?> sur 10</td></tr>

			<tr><th class="domaine">Infrastructures</th><td class="palier_resum"><?php echo $donnees['palier_infra']; ?> sur 10</td></tr>

			<tr><th class="domaine">Services</th><td class="palier_resum"><?php echo $donnees['palier_serv']; ?> sur 10</td></tr>

			<tr><th class="domaine">Pilotage</th><td class="palier_resum"><?php echo $donnees['palier_pilo']; ?> sur 10</td></tr>

			<tr><th class="domaine">Formation</th><td class="palier_resum"><?php echo $donnees['palier_form']; ?> sur 10</td></tr>

			<tr><th class="domaine">Utilisations</th><td class="palier_resum"><?php echo $donnees['palier_uti']; ?> sur 10</td></tr>
			
			<tr><th class="domaine">Usages</th><td class="palier_resum"><?php echo $donnees['palier_usa']; ?> sur 10</td></tr>

				</tbody>
			</table>
			<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>
<?php
	
	$content = ob_get_clean() ;
	//die($content) ;
	require('html2pdf/html2pdf.class.php');
	try{
		$pdf = new HTML2PDF('L','A4','fr') ;
		$pdf->pdf->SetDisplayMode('fullpage') ;
		$pdf->writeHTML($content) ;
		$pdf->Output('Equipement.pdf') ;
	}catch (HTML2PDF_exception $e){
		die($e) ;
	}
	
?>
