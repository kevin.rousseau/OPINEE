<?php
	$titre_page = "Modif_pilo" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');
	
	$RNE = $_POST['RNE'];

	if(!empty($_POST['volet_numerique_projet']) and !empty($_POST['charte_usage_numerique']) and !empty($_POST['commission_numerique']) and !empty($_POST['personnels_referents_numeriques_usages_pedagogiques']) and !empty($_POST['utilisation_IMP_usages_numeriques']) and !empty($_POST['demarche_zero_papier_inutile']) and !empty($_POST['usage_privilegie_numerique_direction_administration']) and !empty($_POST['equipements_numeriques_personnels_enseignants_acceptes']) and !empty($_POST['equipements_numeriques_personnels_eleves_acceptes']) and !empty($_POST['secutite_id_personnels_acces_reseau']) and !empty($_POST['actions_informations_accompagnement_usages_numeriques']) and !empty($_POST['assistance_usagers']) and !empty($_POST['suivi_statistique_taux_utilisation_equipements_services']) and !empty($_POST['mise_en_place_suivi_services_en_ligne']) and !empty($_POST['echanges_chef_etablissement_corps_inspection_developpement']) and !empty($_POST['nb_points_total'])and !empty($_POST['palier_pilo']))
	{
	
		$volet = $_POST['volet_numerique_projet'] ;
		$charte = $_POST['charte_usage_numerique'] ;
		$commission = $_POST['commission_numerique'] ;
		$referents = $_POST['personnels_referents_numeriques_usages_pedagogiques'] ;
		$IMP = $_POST['utilisation_IMP_usages_numeriques'] ;
		$zero_papier = $_POST['demarche_zero_papier_inutile'] ;
		$direction = $_POST['usage_privilegie_numerique_direction_administration'] ;
		$enseignants = $_POST['equipements_numeriques_personnels_enseignants_acceptes'] ;
		$eleves = $_POST['equipements_numeriques_personnels_eleves_acceptes'] ;
		$securite = $_POST['secutite_id_personnels_acces_reseau'] ;
		$actions = $_POST['actions_informations_accompagnement_usages_numeriques'] ;
		$assist = $_POST['assistance_usagers'] ;
		$suivi = $_POST['suivi_statistique_taux_utilisation_equipements_services'] ;
		$suiv_serv = $_POST['mise_en_place_suivi_services_en_ligne'] ;
		$echange = $_POST['echanges_chef_etablissement_corps_inspection_developpement'] ;
		$points = $_POST['nb_points_total'] ;
		$palier = $_POST['palier_pilo'] ;


		$requete1 = $bdd->prepare('UPDATE pilotage SET volet_numerique_projet = :vol, charte_usage_numerique = :cha, commission_numerique = :com, personnels_referents_numeriques_usages_pedagogiques = :ref, utilisation_IMP_usages_numeriques = :IMP, demarche_zero_papier_inutile = :papier, usage_privilegie_numerique_direction_administration = :dir, equipements_numeriques_personnels_enseignants_acceptes = :ens, equipements_numeriques_personnels_eleves_acceptes = :ele, secutite_id_personnels_acces_reseau = :secu, actions_informations_accompagnement_usages_numeriques = :act, assistance_usagers = :assi, suivi_statistique_taux_utilisation_equipements_services = :suiv, mise_en_place_suivi_services_en_ligne = :suivi, echanges_chef_etablissement_corps_inspection_developpement = :ech, nb_points_total = :pts, palier_pilo = :pale WHERE RNE = "'.$_POST['RNE'].'"');

		$requete1->execute(array(
			'vol' => $volet,
			'cha' => $charte,
			'com' => $commission,
			'ref' => $referents,
			'IMP' => $IMP,
			'papier' => $zero_papier,
			'dir' => $direction,
			'ens' => $enseignants,
			'ele' => $eleves,
			'secu' => $securite,
			'act' => $actions,
			'assi' => $assist,
			'suiv' => $suivi,
			'suivi' => $suiv_serv,
			'ech' => $echange,
			'pts' => $points,
			'pale' => $palier
				));	

			header("refresh:0;url=confirm_modif_pilo.php?RNE=".$RNE."") ;
	}
	else
	{
		header('refresh:0;url=modif_pilo.php') ;
	}
?>
</div>
</section>