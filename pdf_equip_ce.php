<?php
	ob_start() ;
	include('header.php');
	
	$requete1 = $bdd->query('SELECT * FROM equipements WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$NomEtab = $_SESSION['NomEtab'];

setlocale(LC_TIME, 'fra_fra');
date_default_timezone_set('Europe/Paris');
?>
	<style>
img.img_gauche
{
	width: 25% ;
	height:80%;
	float: left ;
}
		
img.img_droite
{
	width:20% ;
	height:90%;
	float: right ;
	margin-top: -15 ;
}
		

h2
{
	color : #02A4E4;
	text-align:center;
	margin-top: 0px;
}

h3
{
	color: #E54986 ;
	text-align:center;			
}
		
h4
{
	color : #4A51A9;
    margin-bottom: 3px;
    margin-top: 3px;
    text-align:center;
}

h5
{
	color : #4A51A9;
	position: absolute;
	bottom : 13px;
	text-align:right; 
}

.top
{
	height: 50px ;
}

.corps
{
		width: 95%;
}
		
table
{
	border: 1px #4A51A9 solid;
	border-collapse : collapse ;
	width: 79%;
	text-align:center;
}

th
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:78%;
	margin-top: -10px;
}

th.mod
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:30%;
}

th.critere
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:80%;
}

th.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:19%;
}

th.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

th.donnee
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:15%;
}

th.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:15%;
}

th.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:10%;
}

th.domaine
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

th.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

td
{
	border: 1px #4A51A9 solid ;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnee
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:20%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:20%;
}

td.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:15%;
}

td.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:5%;
}
	</style>
	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Equipements - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th class="critere"><h4>Critères</h4></th>
						<th colspan="2" class="donnee"><h4>Données</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete1->fetch())
			{
		?>

	<tr><th class="critere">Nombre de terminaux (ordinateur fixe, ordinateur portable, tablette)</th><td colspan="2" class="donnee"><?php echo $donnees['nb_terminaux_donnees']; ?></td><td class="points"><?php echo $donnees['nb_terminaux_points']; ?> sur 10</td></tr>

	<tr><th class="critere">Nombre de terminaux mobiles (ordinateur portable, tablette, poste de classes mobiles)</th><td colspan="2" class="donnee"><?php echo $donnees['nb_terminaux_mobiles_donnees']; ?></td><td class="points"><?php echo $donnees['nb_terminaux_mobiles_points']; ?> sur 20</td></tr>

	<tr><th class="critere">Nombre de postes de travail en accès libre aux élèves en dehors des cours (hors classe)</th><td colspan="2" class="donnee"><?php echo $donnees['nb_postes_libres_donnees']; ?></td><td class="points"><?php echo $donnees['nb_postes_libres_points']; ?> sur 20</td></tr>

	<tr><th class="critere">Nombre de terminaux de moins de cinq ans</th><td colspan="2" class="donnee"><?php echo $donnees['nb_terminaux_moins_5_ans_donnees']; ?></td><td class="points"><?php echo $donnees['nb_terminaux_moins_5_ans_points']; ?> sur 15</td></tr>

	<tr><th class="critere">Nombre de VPI/TNI/TBI</th><td colspan="2" class="donnee"><?php echo $donnees['nb_VPI_TNI_TBI_donnees']; ?></td><td class="points"><?php echo $donnees['nb_VPI_TNI_TBI_points']; ?> sur 15</td></tr>

	<tr><th class="critere">Nombre de vidéo-projecteurs</th><td colspan="2" class="donnee"><?php echo $donnees['nb_video_projecteur_donnees']; ?></td><td class="points"><?php echo $donnees['nb_video_projecteur_points']; ?> sur 5</td></tr>

	<tr><th class="critere">Dotation des élèves en terminaux mobiles (ordinateur portable, tablette) par la collectivité</th><td colspan="2" class="donnee"><?php echo $donnees['dotation_eleves_terminaux_mobiles_mod']; ?></td><td class="points"><?php echo $donnees['dotation_eleves_terminaux_mobiles_points']; ?> sur 10</td></tr>

	<tr><th class="critere">Dotation des enseignants en terminaux mobiles (ordinateur portable, tablette) par la collectivité</th><td colspan="2" class="donnee"><?php echo $donnees['dotation_enseignants_terminaux_mobiles_mod']; ?></td><td class="points"><?php echo $donnees['dotation_enseignants_terminaux_mobiles_points']; ?> sur 15</td></tr>

	<tr><th rowspan="7" class="critere">Equipements particuliers</th><th class="part">Malette MP3/MP4</th><td class="part"><?php echo $donnees['equipements_particuliers_malette']; ?></td><td rowspan="7" class="points"><?php echo $donnees['equipements_particuliers_points']; ?> sur 15</td></tr>
	<tr><th class="part">Imprimante 3D</th><td class="part"><?php echo $donnees['equipements_particuliers_imprimante']; ?></td></tr>
	<tr><th class="part">FabLab</th><td class="part"><?php echo $donnees['equipements_particuliers_fablab']; ?></td></tr>
	<tr><th class="part">Visioconférence</th><td class="part"><?php echo $donnees['equipements_particuliers_visio']; ?></td></tr>
	<tr><th class="part">Labo Multimédia</th><td class="part"><?php echo $donnees['equipements_particuliers_labo']; ?></td></tr>
	<tr><th class="part">Boîtiers de vote</th><td class="part"><?php echo $donnees['equipements_particuliers_boitier']; ?></td></tr>
	<tr><th class="part">Visualisateurs</th><td class="part"><?php echo $donnees['equipements_particuliers_visu']; ?></td></tr>

	<tr><th class="critere">Maintenance des équipements (maintien en condition opérationnelle) par la collectivité</th><td colspan="2"  class="donnee"><?php echo $donnees['maintenance_equipement_mod']; ?></td><td class="points"><?php echo $donnees['maintenance_equipement_points']; ?> sur 20</td></tr>

	<tr><th class="critere">L'EPLE engage-til des moyens propres (budget, décharge, IMP, etc.) sur la maintenance quotidienne des équipements ?</th><td colspan="2" class="donnee"><?php echo $donnees['engagement_EPLE_mod']; ?></td><td class="points"><?php echo $donnees['engagement_EPLE_points']; ?> sur 6</td></tr>

				</tbody>
			</table>
			<br><br>
	<table align="center">
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 151</td><td class="palier"><?php echo $donnees['palier_equip'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
		
	</page>
<?php
	
	$content = ob_get_clean() ;
	//die($content) ;
	require('html2pdf/html2pdf.class.php');
	try{
		$pdf = new HTML2PDF('L','A4','fr') ;
		$pdf->pdf->SetDisplayMode('fullpage') ;
		$pdf->writeHTML($content) ;
		$pdf->Output('Equipement.pdf') ;
	}catch (HTML2PDF_exception $e){
		die($e) ;
	}
	
?>
