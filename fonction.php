<?php

	//Fonction permettant de transformer une date "Y-m-d" en "d-m-Y"
	function DateFr($date) //$date : Date(d-m-Y) 
	{
		$date_array = explode('-', $date) ; //On explose la chaîne (les séparateurs étant "-"
		
		$jour = $date_array[2] ;	//On récupère le jour
		$annee = $date_array[0] ;	//Puis l'année
		
		switch($date_array[1])		//Et on traduit le chiffre du mois en chaîne
		{
			case 1:
				$mois = 'Janvier' ;
				break ;
			case 2:
				$mois = 'Février' ;
				break ;
			case 3:
				$mois = 'Mars' ;
				break ;
			case 4:
				$mois = 'Avril' ;
				break ;
			case 5:
				$mois = 'Mai' ;
				break ;
			case 6:
				$mois = 'Juin' ;
				break ;
			case 7:
				$mois = 'Juillet' ;
				break ;
			case 8:
				$mois = 'Août' ;
				break ;
			case 9:
				$mois = 'Septembre' ;
				break ;
			case 10:
				$mois = 'Octobre' ;
				break ;
			case 11:
				$mois = 'Novembre' ;
				break ;
			case 12:
				$mois = 'Décembre' ;
				break ;
		}
		
		$date_fr = $jour.' '.$mois.' '.$annee ;
		
		return $date_fr ;
	}
	
	//Fonction permettant d'obtenir le jour suivant à partir d'une date
	function Jour_suivant($date) //$date : Date(Y_m_d)
	{
		$date_suivant = date('Y-m-d', strtotime($date.'+ 1 days')) ;
		
		return $date_suivant ;
	}
	
	function Formulaire_date($idjour, $idmois, $idannee)
	{
		$j = 1 ;
		echo '<select name="'.$idjour.'">' ;
			while($j <= 31)
			{
				echo '<option value="'.$j.'">'.$j.'</option>'; 
				$j++ ;
			}
		echo '</select>' ;
		
		$m = 1 ;
		echo '<select name="'.$idmois.'">' ;
			while($m <= 12)
			{
				switch($m)		//Et on traduit le chiffre du mois en chaîne
				{
					case 1:
						$mois = 'Janvier' ;
						break ;
					case 2:
						$mois = 'Février' ;
						break ;
					case 3:
						$mois = 'Mars' ;
						break ;
					case 4:
						$mois = 'Avril' ;
						break ;
					case 5:
						$mois = 'Mai' ;
						break ;
					case 6:
						$mois = 'Juin' ;
						break ;
					case 7:
						$mois = 'Juillet' ;
						break ;
					case 8:
						$mois = 'Août' ;
						break ;
					case 9:
						$mois = 'Septembre' ;
						break ;
					case 10:
						$mois = 'Octobre' ;
						break ;
					case 11:
						$mois = 'Novembre' ;
						break ;
					case 12:
						$mois = 'Décembre' ;
						break ;
				}
				
				echo '<option value="'.$m.'">'.$mois.'</option>' ;
				$m++ ;
			}
		echo '</select>' ;
		
		echo '<select name="'.$idannee.'">' ;
			echo '<option value="'.date('Y').'">'.date('Y').'</option>' ;
			echo '<option value="'.(date('Y')+1).'">'.(date('Y')+1).'</option>' ;
		echo '</select>' ;
	}
?>