<?php
	$titre_page = "Paramètres" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');

	$NomEtab = $_SESSION['NomEtab'];
?>

<section>
	<div id="top_section" >
		<h1>Parametres - <?php echo $NomEtab ; ?></h1>
	</div>
	
	<div id="content">
	<br /><br /><br>
	<table class="menu_design">
			<tr>
				<td>
		
					<table class="menu_param">
						<tr>
							<th>Affichage</th>
						</tr>
						<tr>
							<td><a href="para_ce_co.php">CE qui se sont connectés au moins une fois</a></td>
						</tr>
					</table>
					
				</td>
				<td>
				
					<table class="menu_param">
						<tr>
							<th>Insertion</th>
						</tr>
						<tr>
							<td><p><a href="para_new_etab.php">Insérer un nouvel établissement</a></p></td>
						</tr>
						<tr>
							<td><p><a href="para_new_ce.php">Insérer un nouveau chef d'établissement</a></p></td>
						</tr>
						<tr>
							<td><p><a href="para_new_dan.php">Insérer un nouveau DAN</a></p></td>
						</tr>
						<tr>
							<td><p><a href="para_new_ci.php">Insérer un nouveau CI</a></p></td>
						</tr>
						<tr>
							<td><p><a href="para_new_admin.php">Insérer un nouvel Admin</a></p></td>
						</tr>
					</table>
					
				</td>
			</tr>
			<tr>
				<td colspan="2">
				
					<table class="menu_param">
						<tr>
							<th>Modification</th>
						</tr>
						<tr>
							<td><p><a href="para_modif_etab.php">Modifier les données d'un établissement</a></p></td>
						</tr>
						<tr>
							<td><p><a href="para_modif_ce.php">Modifier les données d'un chef d'établissement</a></p></td>
						</tr>
						<tr>
							<td><p><a href="para_modif_dan.php">Modifier les données d'un DAN</a></p></td>
						</tr>
						<tr>
							<td><p><a href="para_modif_ci.php">Modifier les données d'un CI</a></p></td>
						</tr>
						<tr>
							<td><p><a href="para_modif_admin.php">Modifier les données d'un Admin</a></p></td>
						</tr>
					</table>
					
				</td>
			</tr>
		</table>
	</div>
</section>
<?php
	include('pied_de_page.php');
	?>