<?php
	$titre_page = "Nouvel établissement" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');

	if(!empty($_POST['rne']) and !empty($_POST['nom']) and !empty($_POST['ville']) and !empty($_POST['type'])and !empty($_POST['statut']) and !empty($_POST['dpt'])and !empty($_POST['nb_eleve']) and !empty($_POST['nb_ens'])and !empty($_POST['nb_salle']) and !empty($_POST['url']))
	{
	
		$rne = $_POST['rne'] ;
		$nom = $_POST['nom'] ;
		$ville = $_POST['ville'] ;
		$type = $_POST['type'] ;
		$statut = $_POST['statut'] ;
		$dpt = $_POST['dpt'] ;
		$nb_eleve = $_POST['nb_eleve'] ;
		$nb_ens = $_POST['nb_ens'] ;
		$nb_salle = $_POST['nb_salle'] ;
		$url = $_POST['url'] ;

		$requete1 = $bdd->prepare("INSERT INTO etablissements( RNE, nom, ville, type, statut, dpt, nb_eleves, nb_enseignants, nb_salles_enseignements, URL_etablissement) VALUES ( :rne, :nom, :ville, :type, :statut, :dpt, :nb_eleve, :nb_ens, :nb_salle, :url)");
		$requete1->execute(array(
			'rne' => $rne,
			'nom' => $nom,
			'ville' => $ville,
			'type' => $type,
			'statut' => $statut,
			'dpt' => $dpt,
			'nb_eleve' => $nb_eleve,
			'nb_ens' => $nb_ens,
			'nb_salle' => $nb_salle,
			'url' => $url
				));	

			header("refresh:0;url=confirm_new_etab.php") ;
	}
	else
	{
		header("refresh:0;url=para_new_etab.php") ;
	}
?>
