<?php
	$titre_page = "Modif_serv" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');
	
	$RNE = $_POST['RNE'];

	if(!empty($_POST['ENT']) and !empty($_POST['site_web_etablissement']) and !empty($_POST['notes_abscence']) and !empty($_POST['appel_dematerialise']) and !empty($_POST['messagerie_interne']) and !empty($_POST['LSUN_LPC']) and !empty($_POST['resa_ressources']) and !empty($_POST['abonnements_services_information_en_ligne']) and !empty($_POST['folios']) and !empty($_POST['abonnements_ressources_numeriques_pedagogiques']) and !empty($_POST['proportion_ressources_numeriques_papiers']) and !empty($_POST['nb_points_total']) and !empty($_POST['palier_serv']))
	{
	
		$ent = $_POST['ENT'] ;
		$site = $_POST['site_web_etablissement'] ;
		$notes = $_POST['notes_abscence'] ;
		$appel = $_POST['appel_dematerialise'] ;
		$messagerie = $_POST['messagerie_interne'] ;
		$lsun = $_POST['LSUN_LPC'] ;
		$resa = $_POST['resa_ressources'] ;
		$abo = $_POST['abonnements_services_information_en_ligne'] ;
		$folio = $_POST['folios'] ;
		$abo_ress = $_POST['abonnements_ressources_numeriques_pedagogiques'] ;
		$prop = $_POST['proportion_ressources_numeriques_papiers'] ;
		$points = $_POST['nb_points_total'] ;
		$palier = $_POST['palier_serv'] ;


		$requete1 = $bdd->prepare('UPDATE services SET ENT = :ent, site_web_etablissement = :web, notes_abscence = :abscence_notes, appel_dematerialise = :app, messagerie_interne = :messa, LSUN_LPC = :lsun_lpc, resa_ressources = :reservation, abonnements_services_information_en_ligne = :abonnement, folios = :folios, abonnements_ressources_numeriques_pedagogiques = :abonn, proportion_ressources_numeriques_papiers = :proportion, nb_points_total = :pts, palier_serv = :pale WHERE RNE = "'.$_POST['RNE'].'"');

		$requete1->execute(array(
			'ent' => $ent,
			'web' => $site,
			'abscence_notes' => $notes,
			'app' => $appel,
			'messa' => $messagerie,
			'lsun_lpc' => $lsun,
			'reservation' => $resa,
			'abonnement' => $abo,
			'folios' => $folio,
			'abonn' => $abo_ress,
			'proportion' => $prop,
			'pts' => $points,
			'pale' => $palier
				));	

			header("refresh:0;url=confirm_modif_serv.php?RNE=".$RNE."") ;
	}
	else
	{
		header('refresh:0;url=modif_serv.php') ;
	}
?>
</div>
</section>