<?php
	$titre_page = "Modif_etablissement" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');

	$NomEtab = $_SESSION['NomEtab'];
?>

	<section>
	<div id="top_section" >
		<h1>Modifications des informations - <?php echo $NomEtab ; ?></h1>
	</div>
	
	<div id="content">
	<?php
		if(!empty($msg_error))
		{
			echo '<div id="msg_error_2">'.$msg_error.'</div>' ;
		}
	?>

	<?php if($_SESSION['Rang'] == 2)
	{ 
		$RNE = $_SESSION['RNE'];

	$etablissement = $bdd->query('SELECT * FROM etablissements WHERE RNE = "'.$RNE.'"'); 
	$ce = $bdd->query('SELECT * FROM ce WHERE RNE = "'.$RNE.'"'); ?>


	<form method="post" action="modif_info_php.php?RNE=<?php echo $RNE ; ?>" enctype="multipart/form-data">
		<br />

		<table>
		<?php
		while($donnees = $etablissement->fetch())				
		{
			while($donnees2 = $ce->fetch())
			{
	?>
			<tr><th>Nom de l'établissement</th><td><input class="text" type="text" name="nometab" value="<?php echo $donnees['nom']; ?>" /></td></tr>
			<tr><th>Ville de l'établissement</th><td><input class="text" type="text" name="villeetab" value="<?php echo $donnees['ville']; ?>" /></tr>
			<tr><th>Nom du principal de l'établissement</th><td><input class="text" type="text" name="nomprincipal" value="<?php echo $donnees2['nomCE']; ?>" /></td>
			<tr><th>Prénom du principal de l'établissement</th><td><input class="text" type="text" name="prenomprincipal" value="<?php echo $donnees2['prenomCE']; ?>" /></td>
			<tr><th>Nombre d'élèves</th><td><input class="text" type="number" name="nb_eleve" value="<?php echo $donnees['nb_eleves'] ; ?>" /></td>
			<tr><th>Nombre d'enseignants</th><td><input class="text" type="number" name="nb_enseignant" value="<?php echo $donnees['nb_enseignants'] ; ?>" /></td>
			<tr><th>Nombre de salles d'enseignements</th><td><input class="text" type="number" name="nb_salles" value="<?php echo $donnees['nb_salles_enseignements'] ; ?>" /></td>
			<tr><th>Site de l'établissement</th><td><input class="text" type="url" name="site" value="<?php echo $donnees['URL_etablissement'] ; ?>" /></td>
</table>
<?php }
} ?>
		<input class="btn" type="submit" value="Valider les informations" /><br><br>
		</form>

		<?php }
		else
		{
	$RNE = $_POST['RNE'];

	$etablissement = $bdd->query('SELECT * FROM etablissements WHERE RNE = "'.$RNE.'"'); 
	$ce = $bdd->query('SELECT * FROM ce WHERE RNE = "'.$RNE.'"'); ?>

	<form method="post" action="modif_info_php.php" enctype="multipart/form-data">
		<br />

		<table>
		<?php
		while($donnees = $etablissement->fetch())				
		{
			while($donnees2 = $ce->fetch())
			{
	?>
			<tr><th>Nom de l'établissement</th><td><input class="text" type="text" name="nometab" value="<?php echo $donnees['nom']; ?>" /></td></tr>
			<tr><th>Ville de l'établissement</th><td><input class="text" type="text" name="villeetab" value="<?php echo $donnees['ville']; ?>" /></tr>
			<tr><th>Nom du principal de l'établissement</th><td><input class="text" type="text" name="nomprincipal" value="<?php echo $donnees2['nomCE']; ?>" /></td>
			<tr><th>Prénom du principal de l'établissement</th><td><input class="text" type="text" name="prenomprincipal" value="<?php echo $donnees2['prenomCE']; ?>" /></td>
			<tr><th>Nombre d'élèves</th><td><input class="text" type="text" name="nb_eleve" value="<?php echo $donnees['nb_eleves'] ; ?>" /></td>
			<tr><th>Nombre d'enseignants</th><td><input class="text" type="text" name="nb_enseignant" value="<?php echo $donnees['nb_enseignants'] ; ?>" /></td>
			<tr><th>Nombre de salles d'enseignements</th><td><input class="text" type="text" name="nb_salles" value="<?php echo $donnees['nb_salles_enseignements'] ; ?>" /></td>
			<tr><th>Site de l'établissement</th><td><input class="text" type="text" name="site" value="<?php echo $donnees['URL_etablissement'] ; ?>" /></td>
</table>
<?php }
} ?>
		<input class="btn" type="submit" value="Valider les informations" /><br><br>
		<input type="hidden" name="RNE" value=<?php echo $RNE ?> />
		</form>

		<?php } ?>
 	</div>
</section>
<?php
	include('pied_de_page.php');
	?>