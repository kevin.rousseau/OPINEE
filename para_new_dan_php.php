<?php
	$titre_page = "Nouveau DAN" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');

	if(!empty($_POST['nom']) and !empty($_POST['prenom']) and !empty($_POST['pseudo']) and !empty($_POST['mdp']))
	{
	
		$nom = $_POST['nom'] ;
		$prenom = $_POST['prenom'] ;
		$pseudo = $_POST['pseudo'] ;
		$mdp = $_POST['mdp'] ;

		$requete1 = $bdd->prepare("INSERT INTO dan( prenomDan, nomDan, pseudoDan, mdpDan, IDrang, 1ere_connec) VALUES ( :prenom, :nom, :pseudo, :mdp, 3, 0)");
		$requete1->execute(array(
			'nom' => $nom,
			'prenom' => $prenom,
			'pseudo' => $pseudo,
			'mdp' => $mdp
				));	

			header("refresh:0;url=confirm_new_dan.php") ;
	}
	else
	{
		header("refresh:0;url=para_new_dan.php") ;
	}
?>
