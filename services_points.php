<?php
	$titre_page = "Services" ;
	include('header.php');
	include('menu.php');
	include('footer.php');
?>

<section>
	<div id="top_section" >
		<h1>Services</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	
	<div id="content">
<br />
		<?php 
				$serv = $bdd->query('SELECT * FROM services WHERE RNE = "'.$_SESSION['RNE'].'"');

		while($donnees = $serv->fetch())
		{
	?>

	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Nombre de points</h4></th></tr>

			<tr><th>ENT - Espace Numérique de Travail - issu d'un projet académique et/ou des collectivités associées</th><td><?php echo $donnees['ENT_points']; ?> sur 24</td></tr>

			<tr><th>Site web de l'établissement</th><td><?php echo $donnees['site_web_etablissement_points']; ?> sur 10</td></tr>

			<tr><th>Notes, absences, emploi du temps</th><td><?php echo $donnees['notes_abscence_points']; ?> sur 10</td></tr>
			
			<tr><th>Appel dématérialisé des élèves au début de l'heure de cours</th><td><?php echo $donnees['appel_dematerialise_points']; ?> sur 15</td></tr>
			
			<tr><th>Messagerie interne</th><td><?php echo $donnees['messagerie_interne_points']; ?> sur 10</td></tr>
			
			<tr><th>LSUN - Livret Scolaire Unique Numérique<br>LPC - Livret Personnel de Compétences</th><td><?php echo $donnees['LSUN_LPC_points']; ?> sur 15</td></tr>
			
			<tr><th>Service de réservation de ressources</th><td><?php echo $donnees['resa_ressources_points']; ?> sur 5</td></tr>
			
			<tr><th>Abonnements à des services d'information et de documentation en ligne</th><td><?php echo $donnees['abonnements_services_information_en_ligne_points']; ?> sur 15</td></tr>
			
			<tr><th>Folios</th><td><?php echo $donnees['folios_points']; ?> sur 15</td></tr>
			
			<tr><th>Abonnements à des ressources numériques pédagogiques éditoriales</th><td><?php echo $donnees['abonnements_ressources_numeriques_pedagogiques_points']; ?> sur 15</td></tr>
			
			<tr><th>Proportion de ressources numériques par rapport aux ressources papiers</th><td><?php echo $donnees['proportion_ressources_numeriques_papiers_points']; ?> sur 20</td></tr>
	</table>

	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 154</td><td><?php echo $donnees['palier_serv'] ; ?> sur 10</td></tr>
	</table>

		<?php
	}
?>
 	</div>
</section>