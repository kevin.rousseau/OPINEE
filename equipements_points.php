<?php
	$titre_page = "Equipements" ;
	include('header.php');
	include('menu.php');
	include('footer.php');
?>

<section>
	<div id="top_section" >
		<h1>Equipements</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	
	<div id="content">

<br />
		<?php 
				$equipement = $bdd->query('SELECT * FROM equipements WHERE RNE = "'.$_SESSION['RNE'].'"');
		
		while($donnees = $equipement->fetch())
		{
	?>
	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Nombre de points</h4></th></tr>

			<tr><th>Nombre moyen d'élèves par terminal</th><td><?php echo $donnees['nb_terminaux_points']; ?> sur 10</td></tr>

			<tr><th>Nombre moyen d'élèves par terminal mobile</th><td><?php echo $donnees['nb_terminaux_mobiles_points']; ?> sur 20</td></tr>

			<tr><th>Nombre moyen d'élèves par poste de travail en accès libre aux élèves en dehors des heures de cours</th><td><?php echo $donnees['nb_postes_libres_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de terminaux de moins de cinq ans</th><td><?php echo $donnees['nb_terminaux_moins_5_ans_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de salles d'enseignement équipées d'un VPI/TNI/TBI</th><td><?php echo $donnees['nb_VPI_TNI_TBI_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de salles d'enseignement équipées d'un vidéo-projecteur</th><td><?php echo $donnees['nb_video_projecteur_points']; ?> sur 5</td></tr>

			<tr><th>Dotation des élèves en terminaux mobiles par la collectivité</th><td><?php echo $donnees['dotation_eleves_terminaux_mobiles_points']; ?> sur 10</td></tr>

			<tr><th>Dotation des enseignants en terminaux mobiles par la collectivité</th><td><?php echo $donnees['dotation_enseignants_terminaux_mobiles_points']; ?> sur 15</td></tr>

			<tr><th>Équipements particuliers</th><td><?php echo $donnees['equipements_particuliers_points']; ?> sur 15</td></tr>

			<tr><th>Maintenance des équipements par la collectivité</th><td><?php echo $donnees['maintenance_equipement_points']; ?> sur 20</td></tr>

			<tr><th>L'EPLE engage-t-il des moyens propres sur la maintenance quotidienne des équipements ?</th><td><?php echo $donnees['engagement_EPLE_points']; ?> sur 6</td></tr>
	</table>

	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 151</td><td><?php echo $donnees['palier_equip'] ; ?> sur 10</td></tr>
	</table>
		<?php
		}
?>
 	</div>
</section>