<?php
	$titre_page = "Modif_infra" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
?>

	<section>
	<div id="top_section" >
		<h1>Modifications de la notation</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	
	<div id="content">
	<?php
		if(!empty($msg_error))
		{
			echo '<div id="msg_error_2">'.$msg_error.'</div>' ;
		}

		else
		{

		$RNE = $_SESSION['RNE'];
	 ?>
		<br />

	<table>

	<form method="post" action="insert_infra_php.php?RNE=<?php echo $RNE ; ?>" enctype="multipart/form-data">
	
			<tr><th><h4>Critères</h4></th><th colspan="2"><h4>Données</h4></th></tr>

			<tr><th>Nombre de salles avec au moins un accès réseau</th><td colspan="2"><input class="text" type="text" name="proportion_acces_reseau_salles" /></td></tr>

			<tr><th>Débit d'accès à Internet</th><td colspan="2">
				<select name="Debit_internet">
				<option selected="selected" value="1">Moins de 2 Mbit/s</option>
				<option value="2">Strictement plus de 2 et moins de 10 Mbit/s</option>
				<option value="3">Strictement plus de 10 Mbit/s</option>
				</select>
			</td></tr>

			<tr><th>Débit du réseau interne</th><td colspan="2">
				<select name="Debit_reseau">
				<option selected="selected" value="1">Moins de 100 Mbit/s</option>
				<option value="2">Strictement plus de 100 et moins de 1000 Mbit/s</option>
				<option value="3">Strictement plus de 1000 Mbit/s</option>
				</select>
			</td></tr>

			<tr><th>Évaluation de la qualité du débit Internet par rapport aux besoins</th><td colspan="2">
				<select name="Eval_internet">
				<option selected="selected" value="1">Insuffisante</option>
				<option value="2">Suffisante</option>
				</select>
			</td></tr>

			<tr><th>Évaluation de la qualité du débit du réseau interne par rapport aux besoins</th><td colspan="2">
				<select name="Eval_reseau">
				<option selected="selected" value="1">Non réalisée</option>
				<option value="2">Insuffisante</option>
				<option value="3">Suffisante</option>
				</select>
			</td></tr>

			<tr><th rowspan="4">Réseau pédagogique</th>
			<th>Pare-feu</th><td>
				<select name="Pare_feu">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>
			<tr><th>Antivirus</th><td>
				<select name="Antivirus">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>
			<tr><th>Filtrage</th><td>
				<select name="Filtrage">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>
			<tr><th>Contrôle a posteriori des accès</th><td>
				<select name="Controle">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Proportion des espaces (hors salle de classe) couverts par WiFi</th><td colspan="2">
				<select name="proportion_wifi">
				<option selected="selected" value="1">Aucun</option>
				<option value="2">Strictement moins d'un quart</option>
				<option value="3">Entre un quart et la moitié</option>
				<option value="4">Strictement plus de la moitié</option>
				</select>
			</td></tr>

			<tr><th>Nombre de salles d'enseignement couvertes par un réseau WiFi</th><td colspan="2"><input class="text" type="text" name="proportion_wifi_salles_enseignements" /></td></tr>

			<tr><th>Évaluation de la couverture du réseau WiFi par rapport aux besoins</th><td colspan="2">
				<select name="Eval_wifi">
				<option selected="selected" value="1">Insuffisante</option>
				<option value="2">Suffisante</option>
				</select>
			</td></tr>

			<tr><th>Possibilité pour les utilisateurs de recharger les équipements mobiles</th><td colspan="2">
				<select name="Recharge">
				<option selected="selected" value="1">Non</option>
				<option value="2">Oui, partiellement</option>
				<option value="3">Oui, suffisamment et en libre accès</option>
				</select>
			</td></tr>
	</table>
<?php
 }
 ?>
		<input class="btn" type="submit" value="Valider les informations" />
		<input type="hidden" name="RNE" value=<?php echo $RNE ?> />
		</form>
 	</div>
</section>