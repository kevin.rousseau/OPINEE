<?php
	$titre_page = "Nouvel admin" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');
?>

<div id="contenu">
<section>
	<div id="top_section" >
		<h1>Nouvel admin</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	
	<div id="content">
	<h2>Veuillez renseigner les informations pour le nouvel admin</h2>
	<form method="post" action="para_new_admin_php.php">
		<table>
			<tr><th>Nom</th><td><input class="text" type="text" name="nom" /></td></tr>
			<tr><th>Prénom</th><td><input class="text" type="text" name="prenom" /></td></tr>
			<tr><th>Pseudo</th><td><input class="text" type="text" name="pseudo" /></td></tr>
			<tr><th>Mot de passe (provisoire)</th><td><input class="text" type="text" name="mdp" /></td></tr>
		</table>
	<input class="btn" type="submit" value="Valider" />
</form>
</div>
</section>
</div>