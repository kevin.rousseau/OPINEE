<?php
	$titre_page = "Modif_form" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');

	$NomEtab = $_SESSION['NomEtab'];
?>

	<section>
	<div id="top_section" >
		<h1>Modifications de la notation - <?php echo $NomEtab ; ?></h1>
	</div>
	
	<div id="content">
	<?php
		if(!empty($msg_error))
		{
			echo '<div id="msg_error_2">'.$msg_error.'</div>' ;
		}

		else
		{

		$RNE = $_SESSION['RNE'];
	 ?>
		<br />

	<table>

		<form method="post" action="modif_form_php.php?RNE=<?php echo $RNE ; ?>" enctype="multipart/form-data">
		
			<tr><th><h4>Critères</h4></th><th><h4>Données</h4></th></tr>

			<tr><th>Formation au numérique et/ou aux aspects juridiques du numérique d'un ou de plusieurs membres de l'équipe de direction (au cours des trois dernières années)</th><td>
				<select name="juridique">
				<option selected="selected" value="1">Oui, aux deux aspects</option>
				<option value="2">Oui, aux aspects juridiques du numérique</option>
				<option value="3">Oui, à l'utilisation d'outils numériques ou aux usages du numérique</option>
				<option value="4">NON</option>
				</select>
			</td></tr>

			<tr><th>Formation au numérique d'un ou plusieurs membres du personnel de vie scolaire (au cours des trois dernières années)</th><td>
				<select name="vie_scolaire">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Formation au numérique d'un ou de plusieurs membres du personnel administratif et/ou technique (au cours des trois dernières années)</th><td>
				<select name="admin">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Formation de l'équipe enseignante aux usages pédagogiques du numérique sur site et par un formateur académique (au cours des trois dernières années)</th><td>
				<select name="enseignant">
				<option selected="selected" value="1">Au moins deux fois par an</option>
				<option value="2">Au moins une fois par an</option>
				<option value="3">Au moins une fois au cours des trois dernières années</option>
				<option value="4">NON</option>
				</select>
			</td></tr>

			<tr><th>Animations ponctuelles autour des usages du numérique par un référent numérique (personne ressource) dans l'établissement  (au cours des trois dernières années)</th><td>
				<select name="animation">
				<option selected="selected" value="1">Au moins deux fois par an</option>
				<option value="2">Au moins une fois par an</option>
				<option value="3">Au moins une fois au cours des trois dernières années</option>
				<option value="4">NON</option>
				</select>
			</td></tr>

			<tr><th>Proportion de professeurs ayant suivi une formation (PAF, M@gistère) aux usages pédagogiques du numérique (au cours des trois dernières années)</th><td>
				<select name="prof_form">
				<option selected="selected" value="1">Strictement plus de la moitié</option>
				<option value="2">Entre un quart et la moitié</option>
				<option value="3">Aucun ou presque</option>
				</select>
			</td></tr>

			<tr><th>Actions d'information et de sensibilisation aux usages responsables du numérique et à l'Éducation aux Médias et à l'Information (EMI) à destination des enseignants (au cours des trois dernières années)</th><td>
				<select name="action">
				<option selected="selected" value="1">Au moins deux fois par an</option>
				<option value="2">Au moins une fois par an</option>
				<option value="3">Au moins une fois au cours des trois dernières années</option>
				<option value="4">NON</option>
				</select>
			</td></tr>

			<tr><th>Proportion d'enseignants mutualisant leurs pratiques du numérique et/ou collaborant via un ou des réseau(x) numériques)</th><td>
				<select name="prop_ens">
				<option selected="selected" value="1">Plus de la moitié</option>
				<option value="2">Strictement moins de la moitié</option>
				<option value="3">NON</option>
				</select>
			</td></tr>

			<tr><th>Identification dans l'établissement d'une personne chargée de la veille et de la curation du numérique à destination des enseignants</th><td>
				<select name="id_etab">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Formation des parents aux usages des services numériques</th><td>
				<select name="form_par">
				<option selected="selected" value="1">Au moins deux fois par an</option>
				<option value="2">Au moins une fois par an</option>
				<option value="3">Au moins une fois au cours des trois dernières années</option>
				<option value="4">NON</option>
				</select>
			</td></tr>

			<tr><th>Formation des parents aux usages responsables des services numériques</th><td>
				<select name="form_par_resp">
				<option selected="selected" value="1">Au moins deux fois par an</option>
				<option value="2">Au moins une fois par an</option>
				<option value="3">Au moins une fois au cours des trois dernières années</option>
				<option value="4">NON</option>
				</select>
			</td></tr>
	</table>

		<input class="btn" type="submit" value="Valider les informations" /><br><br>
		<input type="hidden" name="RNE" value=<?php echo $RNE ?> />
		</form>
		<?php
 }
 ?>
 	</div>
</section>
<?php
	include('pied_de_page.php');
	?>