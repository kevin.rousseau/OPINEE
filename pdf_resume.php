<?php
	ob_start() ;
	include('header.php');
	
	$requete1 = $bdd->query('SELECT * FROM equipements WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$requete2 = $bdd->query('SELECT * FROM infrastructures WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$requete3 = $bdd->query('SELECT * FROM services WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$requete4 = $bdd->query('SELECT * FROM pilotage WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$requete5 = $bdd->query('SELECT * FROM formation WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$requete6 = $bdd->query('SELECT * FROM utilisations WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$requete7 = $bdd->query('SELECT * FROM usages WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$resume = $bdd->query(' SELECT palier_equip, palier_form, palier_infra, palier_pilo, palier_serv, palier_usa, palier_uti FROM equipements, formation, infrastructures, pilotage, services, usages, utilisations WHERE equipements.RNE = "'.$_SESSION['RNE'].'" AND formation.RNE = "'.$_SESSION['RNE'].'" AND infrastructures.RNE = "'.$_SESSION['RNE'].'" AND pilotage.RNE = "'.$_SESSION['RNE'].'" AND services.RNE = "'.$_SESSION['RNE'].'" AND usages.RNE = "'.$_SESSION['RNE'].'" AND utilisations.RNE = "'.$_SESSION['RNE'].'"');
	$NomEtab = $_SESSION['NomEtab'];

setlocale(LC_TIME, 'fra_fra');
date_default_timezone_set('Europe/Paris');
?>
	<style>
img.img_gauche
{
	width: 25% ;
	height:80%;
	float: left ;
}
		
img.img_droite
{
	width:20% ;
	height:90%;
	float: right ;
	margin-top: -15 ;
}
		

h2
{
	color : #02A4E4;
	text-align:center;
	margin-top: 0px;
}

h3
{
	color: #E54986 ;
	text-align:center;			
}
		
h4
{
	color : #4A51A9;
    margin-bottom: 3px;
    margin-top: 3px;
    text-align:center;
}

h5
{
	color : #4A51A9;
	position: absolute;
	bottom : 13px;
	text-align:right; 
}

.top
{
	height: 50px ;
}

.corps
{
		width: 95%;
}
		
table
{
	border: 1px #4A51A9 solid;
	border-collapse : collapse ;
	width: 79%;
	text-align:center;
}

th
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:78%;
	margin-top: -10px;
}

th.mod
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:30%;
}

th.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:19%;
}

th.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:90%;
}

th.donnees
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:45%;
}

th.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:100%;
}

th.part_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:105%;
}

th.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:40%;
}

th.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:10%;
}

th.domaine
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

td
{
	border: 1px #4A51A9 solid ;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnees
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:45%;
}

td.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:10%;
}

td.vide
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:7%;
}

td.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:80%;
}

td.part_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:90%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:90%;
}

td.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:10%;
}

	</style>
	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Equipements - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th><h4>Critères</h4></th>
						<th class="mod"><h4>Modalité retenue</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete1->fetch())
			{
		?>

			<tr><th>Nombre moyen d'élèves par terminal</th><td><?php echo $donnees['nb_terminaux_mod']; ?></td><td class="points"><?php echo $donnees['nb_terminaux_points']; ?> sur 10</td></tr>

			<tr><th>Nombre moyen d'élèves par terminal mobile</th><td><?php echo $donnees['nb_terminaux_mobiles_mod']; ?></td><td class="points"><?php echo $donnees['nb_terminaux_mobiles_points']; ?> sur 20</td></tr>

			<tr><th>Nombre moyen d'élèves par poste de travail en accès libre aux élèves en dehors des heures de cours</th><td><?php echo $donnees['nb_postes_libres_mod']; ?></td><td class="points"><?php echo $donnees['nb_postes_libres_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de terminaux de moins de cinq ans</th><td><?php echo $donnees['nb_terminaux_moins_5_ans_mod']; ?>  </td><td class="points"><?php echo $donnees['nb_terminaux_moins_5_ans_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de salles d'enseignement équipées d'un VPI/TNI/TBI</th><td><?php echo $donnees['nb_VPI_TNI_TBI_mod']; ?></td><td class="points"><?php echo $donnees['nb_VPI_TNI_TBI_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de salles d'enseignement équipées d'un vidéo-projecteur</th><td><?php echo $donnees['nb_video_projecteur_mod']; ?></td><td class="points"><?php echo $donnees['nb_video_projecteur_points']; ?> sur 5</td></tr>
			
			<tr><th>Dotation des élèves en terminaux mobiles par la collectivité</th><td><?php echo $donnees['dotation_eleves_terminaux_mobiles_mod']; ?></td><td class="points"><?php echo $donnees['dotation_eleves_terminaux_mobiles_points']; ?> sur 10</td></tr>

			<tr><th>Dotation des enseignants en terminaux mobiles par la collectivité</th><td><?php echo $donnees['dotation_enseignants_terminaux_mobiles_mod']; ?></td><td class="points"><?php echo $donnees['dotation_enseignants_terminaux_mobiles_points']; ?> sur 15</td></tr>

			<tr><th>Équipements particuliers</th><td>Voir tableau suivant</td><td class="points"><?php echo $donnees['equipements_particuliers_points']; ?> sur 15</td></tr>

			<tr><th>Maintenance des équipements par la collectivité</th><td><?php echo $donnees['maintenance_equipement_mod']; ?></td><td class="points"><?php echo $donnees['maintenance_equipement_points']; ?> sur 20</td></tr>
			
			<tr><th>L'EPLE engage-t-il des moyens propres sur la maintenance quotidienne des équipements ?</th><td><?php echo $donnees['engagement_EPLE_mod']; ?></td><td class="points"><?php echo $donnees['engagement_EPLE_points']; ?> sur 6</td></tr>

				</tbody>
			</table>
			<br><br>
			<table align="center" border="none">
			<tr>
			<td border="none">
			<table>
				<thead>
					<tr>
						<th class="part"><h4>Equipements particuliers</h4></th>
						<th class="donnees"><h4>Données</h4></th>
					</tr>
				</thead>
				<tbody>
					<tr><th class="part">Malette MP3/ MP4</th><td class="donnees"><?php echo $donnees['equipements_particuliers_malette'] ; ?></td></tr>
					<tr><th class="part">Imprimante 3D</th><td class="donnees"><?php echo $donnees['equipements_particuliers_imprimante'] ; ?></td></tr>
					<tr><th class="part">FabLab</th><td class="donnees"><?php echo $donnees['equipements_particuliers_fablab'] ; ?></td></tr>
					<tr><th class="part">Visioconférence</th><td class="donnees"><?php echo $donnees['equipements_particuliers_visio'] ; ?></td></tr>
					<tr><th class="part">Labo Multimédia</th><td class="donnees"><?php echo $donnees['equipements_particuliers_labo'] ; ?></td></tr>
					<tr><th class="part">Boîtiers de vote</th><td class="donnees"><?php echo $donnees['equipements_particuliers_boitier'] ; ?></td></tr>
					<tr><th class="part">Visualisateurs</th><td class="donnees"><?php echo $donnees['equipements_particuliers_visu'] ; ?></td></tr>
				</tbody>
		
	</table>
	</td>
	<td border="none" class="vide">
	</td>
	<td border="none">
	<table>
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 151</td><td><?php echo $donnees['palier_equip'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	</td>
	</tr></table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
		
	</page>
	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Infrastructures - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th><h4>Critères</h4></th>
						<th class="mod"><h4>Modalité retenue</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete2->fetch())
			{
		?>

			<tr><th>Proportion de salles d'enseignements avec au moins un accès réseau</th><td><?php echo $donnees['proportion_acces_reseau_salles_mod']; ?></td><td class="points"><?php echo $donnees['proportion_acces_reseau_salles_points']; ?> sur 20</td></tr>

			<tr><th>Débit d'accès à Internet</th><td><?php echo $donnees['debit_acces_internet_mod']; ?></td><td class="points"><?php echo $donnees['debit_acces_internet_points']; ?> sur 20</td></tr>

			<tr><th>Débit du réseau interne</th><td><?php echo $donnees['debit_reseau_interne_mod']; ?></td><td class="points"><?php echo $donnees['debit_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la qualité du débit Internet par rapport aux besoins</th><td><?php echo $donnees['evaluation_qualite_debit_internet_mod']; ?></td><td class="points"><?php echo $donnees['evaluation_qualite_debit_internet_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la qualité du débit du réseau interne par rapport aux besoins</th><td><?php echo $donnees['evaluation_qualite_debit_reseau_interne_mod']; ?></td><td class="points"><?php echo $donnees['evaluation_qualite_debit_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th>Réseau pédagogique</th><td>Voir tableau suivant</td><td class="points"><?php echo $donnees['reseau_pedagogique_points']; ?> sur 20</td></tr>

			<tr><th>Proportion des espaces (hors salle de classe) couverts par WiFi</th><td><?php echo $donnees['proportion_wifi_mod']; ?></td><td class="points"><?php echo $donnees['proportion_wifi_points']; ?> sur 10</td></tr>

			<tr><th>Proportion de salles d'enseignement couvertes par un réseau WiFi</th><td><?php echo $donnees['proportion_wifi_salles_enseignements_mod']; ?></td><td class="points"><?php echo $donnees['proportion_wifi_salles_enseignements_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la couverture du réseau WiFi par rapport aux besoins</th><td><?php echo $donnees['evaluation_reseau_wifi_mod']; ?></td><td class="points"><?php echo $donnees['evaluation_reseau_wifi_points']; ?> sur 10</td></tr>
			
			<tr><th>Possibilité pour les utilisateurs de recharger les équipements mobiles</th><td><?php echo $donnees['possibilite_recharger_equipements_mobiles_mod']; ?></td><td class="points"><?php echo $donnees['possibilite_recharger_equipements_mobiles_points']; ?> sur 10</td></tr>

				</tbody>
			</table>
			<br><br>

			<table align="center" border="none">
			<tr>
			<td border="none">
			<table>
				<thead>
					<tr>
						<th class="part"><h4>Reseau pedagogique</h4></th>
						<th class="donnees"><h4>Données</h4></th>
					</tr>
				</thead>
				<tbody>
					<tr><th class="part">Pare-feu</th><td class="donnees"><?php echo $donnees['reseau_pedagogique_pare_feu'] ; ?></td></tr>
					<tr><th class="part">Antivirus</th><td class="donnees"><?php echo $donnees['reseau_pedagogique_antivirus'] ; ?></td></tr>
					<tr><th class="part">Filtrage</th><td class="donnees"><?php echo $donnees['reseau_pedagogique_filtrage'] ; ?></td></tr>
					<tr><th class="part">Contrôle a posteriori des accès</th><td class="donnees"><?php echo $donnees['reseau_pedagogique_controle'] ; ?></td></tr>
				</tbody>
		
	</table>
	</td>
	<td border="none" class="vide">
	</td>
	<td border="none">
	<table>
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 170</td><td><?php echo $donnees['palier_infra'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	</td>
	</tr></table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>

	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Services - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th><h4>Critères</h4></th>
						<th class="mod"><h4>Modalité retenue</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete3->fetch())
			{
		?>

			<tr><th>ENT - Espace Numérique de Travail - issu d'un projet académique <br>et/ou des collectivités associées</th><td><?php echo $donnees['ENT_mod']; ?></td><td class="points"><?php echo $donnees['ENT_points']; ?> sur 24</td></tr>

			<tr><th>Site web de l'établissement</th><td><?php echo $donnees['site_web_etablissement_mod']; ?></td><td class="points"><?php echo $donnees['site_web_etablissement_points']; ?> sur 10</td></tr>

			<tr><th>Notes, absences, emploi du temps</th><td><?php echo $donnees['notes_abscence_mod']; ?></td><td class="points"><?php echo $donnees['notes_abscence_points']; ?> sur 10</td></tr>
			
			<tr><th>Appel dématérialisé des élèves au début de l'heure de cours</th><td><?php echo $donnees['appel_dematerialise_mod']; ?></td><td class="points"><?php echo $donnees['appel_dematerialise_points']; ?> sur 15</td></tr>
			
			<tr><th>Messagerie interne</th><td><?php echo $donnees['messagerie_interne_mod']; ?></td><td class="points"><?php echo $donnees['messagerie_interne_points']; ?> sur 10</td></tr>
			
			<tr><th>"LSUN - Livret Scolaire Unique Numérique<br>LPC - Livret Personnel de Compétences"</th><td><?php echo $donnees['LSUN_LPC_mod']; ?></td><td class="points"><?php echo $donnees['LSUN_LPC_points']; ?> sur 15</td></tr>
			
			<tr><th>Service de réservation de ressources</th><td><?php echo $donnees['resa_ressources_mod']; ?></td><td class="points"><?php echo $donnees['resa_ressources_points']; ?> sur 5</td></tr>
			
			<tr><th>Abonnements à des services d'information et de documentation en ligne</th><td><?php echo $donnees['abonnements_services_information_en_ligne_mod']; ?></td><td class="points"><?php echo $donnees['abonnements_services_information_en_ligne_points']; ?> sur 15</td></tr>
			
			<tr><th>Folios</th><td><?php echo $donnees['folios_mod']; ?></td><td class="points"><?php echo $donnees['folios_points']; ?> sur 15</td></tr>
			
			<tr><th>Abonnements à des ressources numériques pédagogiques éditoriales</th><td><?php echo $donnees['abonnements_ressources_numeriques_pedagogiques_mod']; ?></td><td class="points"><?php echo $donnees['abonnements_ressources_numeriques_pedagogiques_points']; ?> sur 15</td></tr>
			
			<tr><th>Proportion de ressources numériques par rapport aux ressources papiers</th><td><?php echo $donnees['proportion_ressources_numeriques_papiers_mod']; ?></td><td class="points"><?php echo $donnees['proportion_ressources_numeriques_papiers_points']; ?> sur 20</td></tr>

				</tbody>
			</table>
			<br><br>

			<table align="center" border="none">
			<tr>
			<td border="none">
<?php if ($donnees['ENT_mod'] == "OUI")
			{
				?>
				<table>
				<tr><th class="part"><h4>ENT</h4></th><th class="donnees"><h4>Données</h4></th></tr>
				<tr><th class="part">Déploiement à tous les utilisateurs</th><td class="donnees"><?php echo $donnees['ENT_utilisateur']; ?></td></tr>
				<tr><th class="part">Déploiement de tous les services</th><td class="donnees"><?php echo $donnees['ENT_services']; ?></td></tr>
				</table>

				<?php
			}
			else
			{

			}
			?>
	</td>
	<td border="none" class="vide">
	</td>
	<td border="none">
	<table>
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 154</td><td><?php echo $donnees['palier_serv'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	</td>
	</tr></table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>

	<page backtop="70px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Pilotage - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th><h4>Critères</h4></th>
						<th class="mod"><h4>Modalité retenue</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete4->fetch())
			{
		?>

			<tr><th>Volet numérique au sein du projet d'établissement</th><td><?php echo $donnees['volet_numerique_projet_mod']; ?></td><td class="points"><?php echo $donnees['volet_numerique_projet_points']; ?> sur 10</td></tr>
			
			<tr><th>Charte d'usage du numérique</th><td><?php echo $donnees['charte_usage_numerique_mod']; ?></td><td class="points"><?php echo $donnees['charte_usage_numerique_points']; ?> sur 20</td></tr>
			
			<tr><th>Commissions numérique ou conseil pédagogique traitant des sujets numériques</th><td><?php echo $donnees['commission_numerique_mod']; ?></td><td class="points"><?php echo $donnees['commission_numerique_points']; ?> sur 20</td></tr>
			
			<tr><th>Personnel(s) référent(s) numérique(s) pour les usages pédagogiques du numérique</th><td><?php echo $donnees['personnels_referents_numeriques_usages_pedagogiques_mod']; ?></td><td class="points"><?php echo $donnees['personnels_referents_numeriques_usages_pedagogiques_points']; ?> sur 15</td></tr>
			
			<tr><th>Utilisation d'Indemnités de Mission Particulière (IMP) pour favoriser les usages du numérique</th><td><?php echo $donnees['utilisation_IMP_usages_numeriques_mod']; ?></td><td class="points"><?php echo $donnees['utilisation_IMP_usages_numeriques_points']; ?> sur 10</td></tr>
			
			<tr><th>Démarche "zéro papier inutile"</th><td><?php echo $donnees['demarche_zero_papier_inutile_mod']; ?></td><td class="points"><?php echo $donnees['demarche_zero_papier_inutile_points']; ?> sur 10</td></tr>
			
			<tr><th>Usage privilégié du numérique par l'équipe de direction et l'équipe administrative</th><td><?php echo $donnees['usage_privilegie_numerique_direction_administration_mod']; ?></td><td class="points"><?php echo $donnees['usage_privilegie_numerique_direction_administration_points']; ?> sur 10</td></tr>
			
			<tr><th>Équipements numériques personnels des professeurs acceptés sur le réseau de l'établissement</th><td><?php echo $donnees['equipements_numeriques_personnels_enseignants_acceptes_mod']; ?></td><td class="points"><?php echo $donnees['equipements_numeriques_personnels_enseignants_acceptes_points']; ?> sur 20</td></tr>
			
			<tr><th>Équipements numériques personnels des élèves acceptés (pour des usages pédagogiques)<br>sur le réseau de l'établissement</th><td><?php echo $donnees['equipements_numeriques_personnels_eleves_acceptes_mod']; ?></td><td class="points"><?php echo $donnees['equipements_numeriques_personnels_eleves_acceptes_points']; ?> sur 5</td></tr>
			
			<tr><th>Sécurité - Identifiants personnels d'accès au réseau</th><td><?php echo $donnees['securite_id_personnels_acces_reseau_mod']; ?></td><td class="points"><?php echo $donnees['securite_id_personnels_acces_reseau_points']; ?> sur 20</td></tr>

			<tr><th>Actions d'information et d'accompagnement des usagers au numérique</th><td>Voir tableau suivant</td><td class="points"><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_points']; ?> sur 18</td></tr>
		
			<tr><th>Assistance aux usagers</th><td>Voir tableau suivant</td><td class="points"><?php echo $donnees['assistance_usagers_points']; ?> sur 18</td></tr>
			
			<tr><th>Suivi statistique des taux d'utilisation des équipements et des services</th><td><?php echo $donnees['suivi_statistique_taux_utilisation_equipements_services_mod']; ?></td><td class="points"><?php echo $donnees['suivi_statistique_taux_utilisation_equipements_services_points']; ?> sur 10</td></tr>
			
			<tr><th>Mise en place et suivi des "Services en Ligne"</th><td><?php echo $donnees['mise_en_place_suivi_services_en_ligne_mod']; ?></td><td class="points"><?php echo $donnees['mise_en_place_suivi_services_en_ligne_points']; ?> sur 20</td></tr>
			
			<tr><th>Échanges entre le chef d'établissement et les corps d'inspection<br>pour le développement des usages du numérique</th><td><?php echo $donnees['echanges_chef_etablissement_inspection_developpement_mod']; ?></td><td class="points"><?php echo $donnees['echanges_chef_etablissement_inspection_developpement_points']; ?> sur 15</td></tr>

				</tbody>
			</table>
			<br>

			<table align="center" border="none">
			<tr>
			<td border="none">
				<table>
					<thead>
						<tr>
							<th class="part_pilo"><h4>Actions d'information et d'accompagnement des usagers au numérique</h4></th><th class="donnees"><h4>Données</h4></th>
						</tr>
					</thead>
					<tbody>
						<tr><th class="part_pilo">Avec les moyens de l'établissement (personnel référent)</th><td class="donnees"><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_etab']; ?></td></tr>
						<tr><th class="part_pilo">Par les services académiques</th><td class="donnees"><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_aca']; ?></td></tr>
						<tr><th class="part_pilo">Par la collectivité</th><td class="donnees"><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_collec']; ?></td></tr>
					</tbody>
				</table>
			</td>
	<td border="none" class="vide">
	</td>
	<td border="none">
				<table>
					<thead>
						<tr>
							<th class="part_pilo"><h4>Assistance aux usagers</h4></th><th class="donnees"><h4>Données</h4></th>
						</tr>
					</thead>
					<tbody>
						<tr><th class="part_pilo">Avec les moyens de l'établissement (personnel référent)</th><td class="donnees"><?php echo $donnees['assistance_usagers_etab']; ?></td></tr>
						<tr><th class="part_pilo">Par les services académiques</th><td class="donnees"><?php echo $donnees['assistance_usagers_aca']; ?></td></tr>
						<tr><th class="part_pilo">Par la collectivité</th><td class="donnees"><?php echo $donnees['assistance_usagers_collec']; ?></td></tr>
					</tbody>
				</table>
	</td>
	<td border="none" class="vide">
	</td>
	<td border="none">
	<table>
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 221</td><td><?php echo $donnees['palier_pilo'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	</td>
	</tr>
	</table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>

	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Formation - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th><h4>Critères</h4></th>
						<th class="mod"><h4>Modalité retenue</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete5->fetch())
			{
		?>

			<tr><th>Formation au numérique et/ou aux aspects juridiques du numérique d'un ou de plusieurs membres<br>de l'équipe de direction (au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_direction_mod']; ?></td><td class="points"><?php echo $donnees['formation_numerique_direction_points']; ?> sur 20</td></tr>

			<tr><th>Formation au numérique d'un ou plusieurs membres du personnel de vie scolaire<br>(au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_vie_scolaire_mod']; ?></td><td class="points"><?php echo $donnees['formation_numerique_vie_scolaire_points']; ?> sur 20</td></tr>

			<tr><th>Formation au numérique d'un ou de plusieurs membres du personnel administratif et/ou technique<br>(au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_administration_personnel_technique_mod']; ?></td><td class="points"><?php echo $donnees['formation_numerique_administration_personnel_technique_points']; ?> sur 20</td></tr>

			<tr><th>Formation de l'équipe enseignante aux usages pédagogiques du numérique sur site et<br>par un formateur académique (au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_enseignants_mod']; ?></td><td class="points"><?php echo $donnees['formation_numerique_enseignants_points']; ?> sur 15</td></tr>

			<tr><th>Animations ponctuelles autour des usages du numérique par un référent numérique (personne ressource)<br>dans l'établissement  (au cours des trois dernières années) :</th><td><?php echo $donnees['animations_ponctuelles_numeriques_mod']; ?></td><td class="points"><?php echo $donnees['animations_ponctuelles_numeriques_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de professeurs ayant suivi une formation (PAF, M@gistère) aux usages pédagogiques<br>du numérique(au cours des trois dernières années)</th><td><?php echo $donnees['proportion_enseignants_suivi_formation_numerique_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_suivi_formation_numerique_points']; ?> sur 15</td></tr>

			<tr><th>Actions d'information et de sensibilisation aux usages responsables du numérique et à l'Éducation aux Médias<br>et à l'Information (EMI) à destination des enseignants (au cours des trois dernières années)</th><td><?php echo $donnees['actions_informations_sensibilisations_EMI_enseignants_mod']; ?></td><td class="points"><?php echo $donnees['actions_informations_sensibilisations_EMI_enseignants_points']; ?> sur 10</td></tr>

			<tr><th>Proportion d'enseignants mutualisant leurs pratiques du numérique et/ou collaborant<br>via un ou des réseau(x) numériques)</th><td><?php echo $donnees['proportion_enseignants_mutualisant_pratique_numerique_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_mutualisant_pratique_numerique_points']; ?> sur 15</td></tr>

			<tr><th>Identification dans l'établissement d'une personne chargée de la veille et de la curation du numérique<br>à destination des enseignants</th><td><?php echo $donnees['identification_personne_veille_numerique_pour_enseignants_mod']; ?></td><td class="points"><?php echo $donnees['identification_personne_veille_numerique_pour_enseignants_points']; ?> sur 15</td></tr>

			<tr><th>Formation des parents aux usages des services numériques</th><td><?php echo $donnees['formation_parent_usages_numeriques_mod']; ?></td><td class="points"><?php echo $donnees['formation_parent_usages_numeriques_points']; ?> sur 15</td></tr>

			<tr><th>Formation des parents aux usages responsables des services numériques</th><td><?php echo $donnees['formation_parents_usages_responsables_numeriques_mod']; ?></td><td class="points"><?php echo $donnees['formation_parents_usages_responsables_numeriques_points']; ?> sur 15</td></tr>

				</tbody>
			</table>
			<br><br>

			<table align="center" border="none">
			<tr>
	<td border="none">
	<table>
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 175</td><td><?php echo $donnees['palier_form'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	</td>
	</tr></table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>
	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Utilisations - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th><h4>Critères</h4></th>
						<th class="mod"><h4>Modalité retenue</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete6->fetch())
			{
		?>

			<tr><th>Taux de réservation des salles informatiques</th><td><?php echo $donnees['taux_reservation_salles_informatiques_mod']; ?></td><td class="points"><?php echo $donnees['taux_reservation_salles_informatiques_points']; ?> sur 10</td></tr>

			<tr><th>Taux de réservation des classes mobiles</th><td><?php echo $donnees['taux_reservation_classes_mobiles_mod']; ?></td><td class="points"><?php echo $donnees['taux_reservation_classes_mobiles_points']; ?> sur 20</td></tr>

			<tr><th>Proportion d'enseignants utilisant régulièrement des VPI/TNI/TBI</th><td><?php echo $donnees['proportion_enseignants_utilisants_VPI_TNI_TBI_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_utilisants_VPI_TNI_TBI_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de professeurs remplissant le service de notes au moins mensuellement</th><td><?php echo $donnees['proportion_enseignants_rempli_service_notes_mensuellement_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_rempli_service_notes_mensuellement_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de professeurs remplissant le cahier de textes en ligne à l'issue de chaque cours</th><td><?php echo $donnees['proportion_enseignants_remplissant_cahier_texte_mod']; ?></td><td class="points"> <?php echo $donnees['proportion_enseignants_remplissant_cahier_texte_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de parents d'élèves consultant les services de vie scolaire au moins une fois par semaine</th><td><?php echo $donnees['proportion_parent_consultant_services_vie_scolaire_mod']; ?></td><td class="points"> <?php echo $donnees['proportion_parent_consultant_services_vie_scolaire_points']; ?> sur 10</td></tr>

			<tr><th>Proportion d'élèves se connectant chaque jour au réseau interne</th><td><?php echo $donnees['proportion_eleve_connexion_reseau_interne_mod']; ?></td><td class="points"><?php echo $donnees['proportion_eleve_connexion_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th>Fréquentation du site public de l'établissement</th><td><?php echo $donnees['frequentation_site_public_etablissements_mod']; ?></td><td class="points"><?php echo $donnees['frequentation_site_public_etablissements_points']; ?> sur 10</td></tr>

			<tr><th>Équipements numériques utilisés régulièrement par des personnes extérieures à l'établissement</th><td><?php echo $donnees['equipement_numeriques_utilise_regu_personne_exter_mod']; ?></td><td class="points"><?php echo $donnees['equipement_numeriques_utilise_regu_personne_exter_points']; ?> sur 5</td></tr>

				</tbody>
			</table>
			<br><br>

			<table align="center" border="none">
			<tr>
	<td border="none">
	<table>
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 135</td><td><?php echo $donnees['palier_uti'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	</td>
	</tr></table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>

	<page backtop="65px" backleft="-2mm" backright="-2mm">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Usages - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th><h4>Critères</h4></th>
						<th class="mod"><h4>Modalité retenue</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete7->fetch())
			{
		?>

			<tr><th>Pourcentage d'élèves hors classes de 3ème ayant une validation partielle du B2i Collège</th><td><?php echo $donnees['pourcentage_eleves_validation_partielle_B2i_mod']; ?></td><td class="points"><?php echo $donnees['pourcentage_eleves_validation_partielle_B2i_points']; ?> sur 5</td></tr>

			<tr><th>Proportion de professeurs impliqués dans la certification des compétences numériques des élèves(B2i)</th><td><?php echo $donnees['proportion_enseignants_impliques_certification_competence_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_impliques_certification_competence_points']; ?> sur 5</td></tr>

			<tr><th>Nombre de disciplines impliquées dans la validation des compétences du B2i</th><td><?php echo $donnees['nb_discipline_validation_competences_B2i_mod']; ?></td><td class="points"> <?php echo $donnees['nb_discipline_validation_competences_B2i_points']; ?> sur 10</td></tr>

			<tr><th>Proportion de professeurs développant des usages pédagogiques du numérique</th><td><?php echo $donnees['proportion_enseignants_developpant_usage_numerique_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_developpant_usage_numerique_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de manuels scolaires numériques dans l'EPLE</th><td><?php echo $donnees['proportion_manuels_scolaires_numeriques_EPLE_mod']; ?></td><td class="points"> <?php echo $donnees['proportion_manuels_scolaires_numeriques_EPLE_points']; ?> sur 15</td></tr>

			<tr><th>Usages de services et ressources pédagogiques numériques institutionnels (D'Col, EFS, éduthèque, Fondamentaux, etc.)</th><td><?php echo $donnees['usages_services_ressources_numeriques_mod']; ?></td><td class="points"> <?php echo $donnees['usages_services_ressources_numeriques_points']; ?> sur 15</td></tr>

			<tr><th>Usages d'une messagerie institutionnelle (académique ou dans l'ENT) pour les échanges professionnels</th><td><?php echo $donnees['usages_messagerie_institutionnelle_echanges_professionnel_mod']; ?></td><td class="points"><?php echo $donnees['usages_messagerie_institutionnelle_echanges_professionnel_points']; ?> sur 20</td></tr>

			<tr><th>Usages de la messagerie de l'ENT pour les échanges des professeurs avec les familles (si ENT)</th><td><?php echo $donnees['usages_messagerie_ENT_enseignants_famille_mod']; ?></td><td class="points"><?php echo $donnees['usages_messagerie_ENT_enseignants_famille_points']; ?> sur 15</td></tr>

			<tr><th>Usages d'espaces de stockage ou de partage de documents pédagogiques</th><td><?php echo $donnees['usages_espaces_stockages_partage_documents_pedagogiques_mod']; ?></td><td class="points"><?php echo $donnees['usages_espaces_stockages_partage_documents_pedagogiques_points']; ?> sur 10</td></tr>

			<tr><th>Usages d'outils numériques pour des pratiques collaboratives entre ensiegnants</th><td><?php echo $donnees['usages_outils_numeriques_entre_enseignants_mod']; ?></td><td class="points"> <?php echo $donnees['usages_outils_numeriques_entre_enseignants_points']; ?> sur 15</td></tr>

			<tr><th>Usages d'outils numériques pour des pratiques collaboratives des enseignants avec leurs élèves</th><td><?php echo $donnees['usages_outils_numeriques_entre_enseignants_eleves_mod']; ?></td><td class="points"><?php echo $donnees['usages_outils_numeriques_entre_enseignants_eleves_points']; ?> sur 20</td></tr>

			<tr><th>Usages connus de réseaux sociaux dans le cadre pédagogique</th><td><?php echo $donnees['usages_reseaux_sociaux_pedagogiques_mod']; ?></td><td class="points"><?php echo $donnees['usages_reseaux_sociaux_pedagogiques_points']; ?> sur 15</td></tr>

			<tr><th>Usages connus de services de publication de type blog pour l'enseignement</th><td><?php echo $donnees['usages_services_publication_pour_enseignement_mod']; ?></td><td class="points"><?php echo $donnees['usages_services_publication_pour_enseignement_points']; ?> sur 15</td></tr>

			<tr><th>Création de médias numériques (journal, radio, vidéo, exposition) connue et validée par le chef d'établissement</th><td><?php echo $donnees['creation_medias_numeriques_mod']; ?></td><td class="points"><?php echo $donnees['creation_medias_numeriques_points']; ?> sur 20</td></tr>

			<tr><th>Utilisation d'outils et de ressources numériques pour le développement de la pratique de l'oral(baladodiffusion, labo mutimédia, etc.)</th><td><?php echo $donnees['utilisation_outils_ressources_numeriques_dvlp_oral_mod']; ?></td><td class="points"><?php echo $donnees['utilisation_outils_ressources_numeriques_dvlp_oral_points']; ?> sur 20</td></tr>

			<tr><th>Usages de services numériques de suivi de la maîtrise des compétences (ex : LPC, LSUN, etc.)</th><td><?php echo $donnees['usages_services_numeriques_suivi_competences_mod']; ?></td><td class="points"><?php echo $donnees['usages_services_numeriques_suivi_competences_points']; ?> sur 20</td></tr>

			<tr><th>Usages du numérique à des fins de personnalisation des parcours et d'individualisation des enseignements</th><td><?php echo $donnees['usages_numeriques_personnalisation_parcours_individu_mod']; ?></td><td class="points"><?php echo $donnees['usages_numeriques_personnalisation_parcours_individu_points']; ?> sur 20</td></tr>

			<tr><th>Usages du numérique dans le cadre de la liaison inter-degré</th><td><?php echo $donnees['usages_numeriques_liaison_inter_degre_mod']; ?></td><td class="points"><?php echo $donnees['usages_numeriques_liaison_inter_degre_points']; ?> sur 20</td></tr>

				</tbody>
			</table>
			<br>

			<table align="center" border="none">
			<tr>
	<td border="none">
	<table>
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 280</td><td><?php echo $donnees['palier_usa'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	</td>
	</tr></table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>

	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Palier - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th class="domaine"><h4>Domaine</h4></th>
						<th class="palier_resum"><h4>Palier</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $resume->fetch())
			{
		?>

			<tr><th class="domaine">Equipements</th><td class="palier_resum"><?php echo $donnees['palier_equip']; ?> sur 10</td></tr>

			<tr><th class="domaine">Infrastructures</th><td class="palier_resum"><?php echo $donnees['palier_infra']; ?> sur 10</td></tr>

			<tr><th class="domaine">Services</th><td class="palier_resum"><?php echo $donnees['palier_serv']; ?> sur 10</td></tr>

			<tr><th class="domaine">Pilotage</th><td class="palier_resum"><?php echo $donnees['palier_pilo']; ?> sur 10</td></tr>

			<tr><th class="domaine">Formation</th><td class="palier_resum"><?php echo $donnees['palier_form']; ?> sur 10</td></tr>

			<tr><th class="domaine">Utilisations</th><td class="palier_resum"><?php echo $donnees['palier_uti']; ?> sur 10</td></tr>
			
			<tr><th class="domaine">Usages</th><td class="palier_resum"><?php echo $donnees['palier_usa']; ?> sur 10</td></tr>

				</tbody>
			</table>
			<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>
<?php
	
	$content = ob_get_clean() ;
	//die($content) ;
	require('html2pdf/html2pdf.class.php');
	try{
		$pdf = new HTML2PDF('L','A4','fr') ;
		$pdf->pdf->SetDisplayMode('fullpage') ;
		$pdf->writeHTML($content) ;
		$pdf->Output('Equipement.pdf') ;
	}catch (HTML2PDF_exception $e){
		die($e) ;
	}
	
?>
