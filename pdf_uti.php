<?php
	ob_start() ;
	include('header.php');
	
	$requete1 = $bdd->query('SELECT * FROM utilisations WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$NomEtab = $_SESSION['NomEtab'];

setlocale(LC_TIME, 'fra_fra');
date_default_timezone_set('Europe/Paris');
?>
	<style>
img.img_gauche
{
	width: 25% ;
	height:80%;
	float: left ;
}
		
img.img_droite
{
	width:20% ;
	height:90%;
	float: right ;
	margin-top: -15 ;
}
		

h2
{
	color : #02A4E4;
	text-align:center;
	margin-top: 0px;
}

h3
{
	color: #E54986 ;
	text-align:center;			
}
		
h4
{
	color : #4A51A9;
    margin-bottom: 3px;
    margin-top: 3px;
    text-align:center;
}

h5
{
	color : #4A51A9;
	position: absolute;
	bottom : 13px;
	text-align:right; 
}

.top
{
	height: 50px ;
}

.corps
{
		width: 95%;
}
		
table
{
	border: 1px #4A51A9 solid;
	border-collapse : collapse ;
	width: 79%;
	text-align:center;
}

th
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:78%;
	margin-top: -10px;
}

th.mod
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:30%;
}

th.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:19%;
}

th.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:90%;
}

th.donnees
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:45%;
}

th.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:100%;
}

th.part_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:105%;
}

th.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:40%;
}

th.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:10%;
}

th.domaine
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

td
{
	border: 1px #4A51A9 solid ;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnees
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:45%;
}

td.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:10%;
}

td.vide
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:7%;
}

td.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:80%;
}

td.part_pilo
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:90%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:90%;
}

td.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:10%;
}

	</style>
	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Utilisations - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th><h4>Critères</h4></th>
						<th class="mod"><h4>Modalité retenue</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete1->fetch())
			{
		?>

			<tr><th>Taux de réservation des salles informatiques</th><td><?php echo $donnees['taux_reservation_salles_informatiques_mod']; ?></td><td class="points"><?php echo $donnees['taux_reservation_salles_informatiques_points']; ?> sur 10</td></tr>

			<tr><th>Taux de réservation des classes mobiles</th><td><?php echo $donnees['taux_reservation_classes_mobiles_mod']; ?></td><td class="points"><?php echo $donnees['taux_reservation_classes_mobiles_points']; ?> sur 20</td></tr>

			<tr><th>Proportion d'enseignants utilisant régulièrement des VPI/TNI/TBI</th><td><?php echo $donnees['proportion_enseignants_utilisants_VPI_TNI_TBI_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_utilisants_VPI_TNI_TBI_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de professeurs remplissant le service de notes au moins mensuellement</th><td><?php echo $donnees['proportion_enseignants_rempli_service_notes_mensuellement_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_rempli_service_notes_mensuellement_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de professeurs remplissant le cahier de textes en ligne à l'issue de chaque cours</th><td><?php echo $donnees['proportion_enseignants_remplissant_cahier_texte_mod']; ?></td><td class="points"> <?php echo $donnees['proportion_enseignants_remplissant_cahier_texte_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de parents d'élèves consultant les services de vie scolaire au moins une fois par semaine</th><td><?php echo $donnees['proportion_parent_consultant_services_vie_scolaire_mod']; ?></td><td class="points"> <?php echo $donnees['proportion_parent_consultant_services_vie_scolaire_points']; ?> sur 10</td></tr>

			<tr><th>Proportion d'élèves se connectant chaque jour au réseau interne</th><td><?php echo $donnees['proportion_eleve_connexion_reseau_interne_mod']; ?></td><td class="points"><?php echo $donnees['proportion_eleve_connexion_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th>Fréquentation du site public de l'établissement</th><td><?php echo $donnees['frequentation_site_public_etablissements_mod']; ?></td><td class="points"><?php echo $donnees['frequentation_site_public_etablissements_points']; ?> sur 10</td></tr>

			<tr><th>Équipements numériques utilisés régulièrement par des personnes extérieures à l'établissement</th><td><?php echo $donnees['equipement_numeriques_utilise_regu_personne_exter_mod']; ?></td><td class="points"><?php echo $donnees['equipement_numeriques_utilise_regu_personne_exter_points']; ?> sur 5</td></tr>

				</tbody>
			</table>
			<br><br>

			<table align="center" border="none">
			<tr>
	<td border="none">
	<table>
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 135</td><td><?php echo $donnees['palier_uti'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	</td>
	</tr></table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>
<?php
	
	$content = ob_get_clean() ;
	//die($content) ;
	require('html2pdf/html2pdf.class.php');
	try{
		$pdf = new HTML2PDF('L','A4','fr') ;
		$pdf->pdf->SetDisplayMode('fullpage') ;
		$pdf->writeHTML($content) ;
		$pdf->Output('Equipement.pdf') ;
	}catch (HTML2PDF_exception $e){
		die($e) ;
	}
	
?>
