<?php
	$titre_page = "Modif_equipement" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');

	$NomEtab = $_SESSION['NomEtab'];
?>

	<section>
	<div id="top_section" >
		<h1>Modifications - <?php echo $NomEtab ; ?></h1>
	</div>
	
	<div id="content">
	<?php
		if(!empty($msg_error))
		{
			echo '<div id="msg_error_2">'.$msg_error.'</div>' ;
		}

		else
		{

		$RNE = $_SESSION['RNE'];

	$equipement = $bdd->query('SELECT * FROM equipements WHERE RNE = "'.$RNE.'"');
	 ?>

		<br />
	<?php 	
		while($donnees = $equipement->fetch())
		{
	?>
	<table>

	<form method="post" action="modif_equip_php.php?RNE=<?php echo $RNE ; ?>" enctype="multipart/form-data">

	<tr><th><h4>Critères</h4></th><th colspan="2"><h4>Données</h4></th></tr>

	<tr><th>Nombre de terminaux (ordinateur fixe, ordinateur portable, tablette)</th><td colspan="2"><input class="text" type="text" name="nb_terminaux_text" value="<?php echo $donnees['nb_terminaux_donnees']; ?>" /></td></tr>

	<tr><th>Nombre de terminaux mobiles (ordinateur portable, tablette, poste de classes mobiles)</th><td colspan="2"><input class="text" type="text" name="nb_terminaux_mobiles_text" value="<?php echo $donnees['nb_terminaux_mobiles_donnees']; ?>" /></td></tr>

	<tr><th>Nombre de postes de travail en accès libre aux élèves en dehors des cours (hors classe)</th><td colspan="2"><input class="text" type="text" name="nb_postes_libres_text" value="<?php echo $donnees['nb_postes_libres_donnees']; ?>" /></td></tr>

	<tr><th>Nombre de terminaux de moins de cinq ans</th><td colspan="2"><input class="text" type="text" name="nb_terminaux_moins_5_ans_text" value="<?php echo $donnees['nb_terminaux_moins_5_ans_donnees']; ?>" /></td></tr>

	<tr><th>Nombre de VPI/TNI/TBI</th><td colspan="2"><input class="text" type="text" name="nb_VPI_TNI_TBI_text" value="<?php echo $donnees['nb_VPI_TNI_TBI_donnees']; ?>" /></td></tr>

	<tr><th>Nombre de vidéo-projecteurs</th><td colspan="2"><input class="text" type="text" name="nb_video_projecteur_text" value="<?php echo $donnees['nb_video_projecteur_donnees']; ?>" /></td></tr>

	<tr><th>Dotation des élèves en terminaux mobiles (ordinateur portable, tablette) par la collectivité</th><td colspan="2">
	<select name="Dot_ele">
		<option selected="selected" value="Aucune">Aucune</option>
		<option value="Quelques élèves">Quelques élèves</option>
		<option value="Une classe">Une classe</option>
		<option value="Plusieurs classes">Plusieurs classes</option>
		<option value="Toutes les classes">Toutes les classes</option>
	</select></td></tr>

	<tr><th>Dotation des enseignants en terminaux mobiles (ordinateur portable, tablette) par la collectivité</th><td colspan="2">
	<select name="Dot_ens">
		<option selected="selected" value="Aucune">Aucune</option>
		<option value="Quelques enseignants">Quelques enseignants</option>
		<option value="Tous les enseignants">Tous les enseignants</option>
	</select></td></tr>

	<tr><th rowspan="7">Equipements particuliers</th>
	<th>Malette MP3/MP4</th><td>
		<select name="malette">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>
	<tr><th>Imprimante 3D</th><td>
		<select name="imprimante">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>
	<tr><th>FabLab</th><td>
		<select name="fablab">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>
	<tr><th>Visioconférence</th><td>
		<select name="visio">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>
	<tr><th>Labo Multimédia</th><td>
		<select name="labo">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>
	<tr><th>Boîtiers de vote</th><td>
		<select name="vote">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>
	<tr><th>Visualisateurs</th><td>
		<select name="visua">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>

	<tr><th>Maintenance des équipements (maintien en condition opérationnelle) par la collectivité</th><td colspan="2">
		<select name="maint">
		<option selected="selected" value="OUI">OUI</option>
		<option value="OUI, insuffisamment">OUI, insuffisamment</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>

	<tr><th>L'EPLE engage-til des moyens propres (budget, décharge, IMP, etc.) sur la maintenance quotidienne des équipements ?</th><td colspan="2">
		<select name="eple">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>
	</table>
<?php
 }
 ?>
		<input class="btn" type="submit" value="Valider les informations" /><br><br>
		<input type="hidden" name="RNE" value=<?php echo $RNE ?> />
		</form>
		<?php } ?>
 	</div>
</section>
<?php
	include('pied_de_page.php');
	?>