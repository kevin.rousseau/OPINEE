<?php
	$titre_page = "Historique" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');

	$NomEtab = $_SESSION['NomEtab'];
?>

<section>
	<div id="top_section" >
		<h1>Historique<?php if ($_SESSION['NomEtab'] != NULL){echo ' - '.$NomEtab ;} else {} ; ?></h1>
	</div>
	
	<div id="content">
<br />

<?php
if (empty($_GET['RNE']) AND (empty($_POST['RNE'])))
{
?> <h3>Veuillez tout d'abord choisir un établissement sur la page d'accueil</h3> <?php
}
else
{

		$hist = $bdd->query('SELECT * FROM historic WHERE RNE = "'.$_SESSION['RNE'].'"');
		$cmpr = $bdd->query('SELECT * FROM etablissements WHERE dpt = ( SELECT dpt FROM etablissements WHERE RNE = "'.$_SESSION['RNE'].'")');
		$queryRNE = $bdd->query('SELECT RNE FROM historic WHERE RNE = "'.$_SESSION['RNE'].'"'); 
		$count = $queryRNE->rowCount();  
			if($count == 1) 
			{	
				?>

				<h3>Pour visualiser les données de l'établissement à une date antérieure, sélectionner une date ci-dessous</h3>

	<form method="post" action="historique_visu.php">	
		<select name="date">

		<?php
				while($donnees = $hist->fetch())
				{
					echo '<option value="'.$donnees['date_modif'].'">'.$donnees['date_modif'].'</option>' ;
				}
?>

</select>&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn" type="submit" value="Afficher" /></form>


				<h3>Pour comparer cet établissement avec un autre établissement du département, sélectionner le ci-dessous</h3>

				<form method="post" action="compar_visu.php">
									<select name="RNE">
				<?php
				while($donnees1 = $cmpr->fetch())
				{
							$compare = $bdd->query('SELECT * FROM historic WHERE RNE = "'.$donnees1['RNE'].'"');
								while($donnees2 = $compare->fetch())
								{
									if ($donnees2['RNE'] == $_SESSION['RNE'])
									{

									}
									else
									{
										echo '<option value="'.$donnees2['RNE'].'">'.$donnees1['nom'].' - '.$donnees2['RNE'].'</option>' ;
									}
								}
				}
				?>
								</select>&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn" type="submit" value="Afficher" /></form>
				<?php
}
else
{
	?>
	<h2>Aucune données de cet établissement dans l'historique</h2>

	<?php
}
}
?>
		
</div>
</section>
<?php
	include('pied_de_page.php');
	?>