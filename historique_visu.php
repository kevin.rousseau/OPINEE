<?php
	$titre_page = "Historique_visu" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	?>

<section>
	<div id="top_section" >
		<h1>Comparaison des données</h1>
	</div>
	
	<div id="content">

<?php 

	if ($_SESSION['date_modif'] == NULL)
	{
		$_SESSION['date_modif'] = $_POST['date'];
	}
	else
	{
		
	}

		$hist = $bdd->query('SELECT * FROM historic WHERE RNE = "'.$_SESSION['RNE'].'" AND date_modif = "'.$_SESSION['date_modif'].'"');
		$new = $bdd->query('SELECT * FROM equipements WHERE RNE = "'.$_SESSION['RNE'].'"');

	while($donnees = $new->fetch())
				{

		while($donnees1 = $hist->fetch())
				{
					
?>
<h3>Equipements&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_infra.php">Infrastructures</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_serv">Services</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_pilo">Pilotage</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_form">Formation</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="historique_visu_uti">Utilisations</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="historique_visu_usa">Usages</a></h3> 

		<table>
			<tr><th><h4>Critères</h4></th><th><h4>Ancienne modalité</h4></th><th><h4>Nouvelle modalité</h4></th><th><h4>Ancien nombre de points</h4></th><th><h4>Nouveau nombre de points</h4></th></tr>

			<tr><th>Nombre moyen d'élèves par terminal</th><td><?php echo $donnees1['nb_terminaux_mod']; ?></td><td><?php echo $donnees['nb_terminaux_mod']; ?></td><td><?php echo $donnees1['nb_terminaux_points']; ?> sur 10</td><td><?php echo $donnees['nb_terminaux_points']; ?> sur 10</td></tr>

			<tr><th>Nombre moyen d'élèves par terminal mobile</th><td><?php echo $donnees1['nb_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees['nb_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees1['nb_terminaux_mobiles_points']; ?> sur 20</td><td><?php echo $donnees['nb_terminaux_mobiles_points']; ?> sur 20</td></tr>

			<tr><th>Nombre moyen d'élèves par poste de travail en accès libre aux élèves en dehors des heures de cours</th><td><?php echo $donnees1['nb_postes_libres_mod']; ?></td><td><?php echo $donnees['nb_postes_libres_mod']; ?></td><td><?php echo $donnees1['nb_postes_libres_points']; ?> sur 20</td><td><?php echo $donnees['nb_postes_libres_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de terminaux de moins de cinq ans</th><td><?php echo $donnees1['nb_terminaux_moins_5_ans_mod']; ?></td><td><?php echo $donnees['nb_terminaux_moins_5_ans_mod']; ?></td><td><?php echo $donnees1['nb_terminaux_moins_5_ans_points']; ?> sur 15</td><td><?php echo $donnees['nb_terminaux_moins_5_ans_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de salles d'enseignement équipées d'un VPI/TNI/TBI</th><td><?php echo $donnees1['nb_VPI_TNI_TBI_mod']; ?></td><td><?php echo $donnees['nb_VPI_TNI_TBI_mod']; ?></td><td><?php echo $donnees1['nb_VPI_TNI_TBI_points']; ?> sur 15</td><td><?php echo $donnees['nb_VPI_TNI_TBI_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de salles d'enseignement équipées d'un vidéo-projecteur</th><td><?php echo $donnees1['nb_video_projecteur_mod']; ?></td><td><?php echo $donnees['nb_video_projecteur_mod']; ?></td><td><?php echo $donnees1['nb_video_projecteur_points']; ?> sur 5</td><td><?php echo $donnees['nb_video_projecteur_points']; ?> sur 5</td></tr>
			
			<tr><th>Dotation des élèves en terminaux mobiles par la collectivité</th><td><?php echo $donnees1['dotation_eleves_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees['dotation_eleves_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees1['dotation_eleves_terminaux_mobiles_points']; ?> sur 10</td><td><?php echo $donnees['dotation_eleves_terminaux_mobiles_points']; ?> sur 10</td></tr>

			<tr><th>Dotation des enseignants en terminaux mobiles par la collectivité</th><td><?php echo $donnees1['dotation_enseignants_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees['dotation_enseignants_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees1['dotation_enseignants_terminaux_mobiles_points']; ?> sur 15</td><td><?php echo $donnees['dotation_enseignants_terminaux_mobiles_points']; ?> sur 15</td></tr>

			<tr><th>Équipements particuliers</th><td colspan="2">Voir tableau suivant</td><td><?php echo $donnees1['equipements_particuliers_points']; ?> sur 15</td><td><?php echo $donnees['equipements_particuliers_points']; ?> sur 15</td></tr>

			<tr><th>Maintenance des équipements par la collectivité</th><td><?php echo $donnees1['maintenance_equipement_mod']; ?></td><td><?php echo $donnees['maintenance_equipement_mod']; ?></td><td><?php echo $donnees1['maintenance_equipement_points']; ?> sur 20</td><td><?php echo $donnees['maintenance_equipement_points']; ?> sur 20</td></tr>
			
			<tr><th>L'EPLE engage-t-il des moyens propres sur la maintenance quotidienne des équipements ?</th><td><?php echo $donnees1['engagement_EPLE_mod']; ?></td><td><?php echo $donnees['engagement_EPLE_mod']; ?></td><td><?php echo $donnees1['engagement_EPLE_points']; ?> sur 6</td><td><?php echo $donnees['engagement_EPLE_points']; ?> sur 6</td></tr>
	</table>
	<table>
		<tr><th><h4>Equipements particuliers</h4></th><th><h4>Anciennes données</h4></th><th><h4>Nouvelles données</h4></th></tr>
		<tr><th>Malette MP3/ MP4</th><td><?php echo $donnees1['equipements_particuliers_malette'] ; ?></td><td><?php echo $donnees['equipements_particuliers_malette'] ; ?></td></tr>
		<tr><th>Imprimante 3D</th><td><?php echo $donnees1['equipements_particuliers_imprimante'] ; ?></td><td><?php echo $donnees['equipements_particuliers_imprimante'] ; ?></td></tr>
		<tr><th>FabLab</th><td><?php echo $donnees1['equipements_particuliers_fablab'] ; ?></td><td><?php echo $donnees['equipements_particuliers_fablab'] ; ?></td></tr>
		<tr><th>Visioconférence</th><td><?php echo $donnees1['equipements_particuliers_visio'] ; ?></td><td><?php echo $donnees['equipements_particuliers_visio'] ; ?></td></tr>
		<tr><th>Labo Multimédia</th><td><?php echo $donnees1['equipements_particuliers_labo'] ; ?></td><td><?php echo $donnees['equipements_particuliers_labo'] ; ?></td></tr>
		<tr><th>Boîtiers de vote</th><td><?php echo $donnees1['equipements_particuliers_boitier'] ; ?></td><td><?php echo $donnees['equipements_particuliers_boitier'] ; ?></td></tr>
		<tr><th>Visualisateurs</th><td><?php echo $donnees1['equipements_particuliers_visu'] ; ?></td><td><?php echo $donnees['equipements_particuliers_visu'] ; ?></td></tr>
	</table>
	<table>
			<th><h4>Ancien nombre de points</h4></th><th><h4>Nouveau nombre de points</h4></th><th><h4>Ancien palier</h4></th><th><h4>Nouveau palier</h4></th></tr>
			<tr><td><?php echo $donnees1['nb_points_total']; ?> sur 151</td><td><?php echo $donnees['nb_points_total']; ?> sur 151</td><td><?php echo $donnees1['palier_equip'] ; ?> sur 10</td><td><?php echo $donnees['palier_equip'] ; ?> sur 10</td></tr>
	</table>


			<?php
			}
		}
		?>			
</div>
</section>
<?php
include('pied_de_page.php');
?>