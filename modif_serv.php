<?php
	$titre_page = "Modif_serv" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');
?>

<div id="contenu">
	<section>
	<div id="top_section" >
		<h1>Modifications des données</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	
	<div id="content">
	<?php
		if(!empty($msg_error))
		{
			echo '<div id="msg_error_2">'.$msg_error.'</div>' ;
		}

		else
		{

		$RNE = $_SESSION['RNE'];
	 ?>
		<br />

	<table>

	<form method="post" action="modif_serv_php.php?RNE=<?php echo $RNE ; ?>" enctype="multipart/form-data">
	
			<tr><th><h4>Critères</h4></th><th colspan="2"><h4>Données</h4></th></tr>

			<tr><th rowspan="3">ENT - Espace Numérique de Travail - issu d'un projet académique et/ou des collectivités associées</th><td colspan="2">
				<select name="ENT">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Si oui, déploiement à tous les utilisateurs</th><td>
				<select name="ENT_user">
				<option value="1">OUI</option>
				<option selected="selected" value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Si oui, déploiement de tous les services</th><td>
				<select name="ENT_serv">
				<option value="1">OUI</option>
				<option selected="selected" value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Site web de l'établissement</th><td colspan="2">
				<select name="site">
				<option selected="selected" value="1">Aucun</option>
				<option value="2">Hébergé à l'extérieur (hors ENT) chez un hébergeur privé</option>
				<option value="3">Hébergé dans l'établissement ou sur des serveurs académiques</option>
				<option value="4">Intégré à l'ENT (pages publiques réalisées dans l'ENT)</option>
				</select>
			</td></tr>		

			<tr><th>Notes, absences, emploi du temps</th><td colspan="2">
				<select name="notes">
				<option selected="selected" value="1">Éditeur privé</option>
				<option value="2">Services fournis par le MENESR</option>
				<option value="3">Intégrés à l'ENT</option>
				</select>
			</td></tr>
			
			<tr><th>Appel dématérialisé des élèves au début de l'heure de cours</th><td colspan="2">
				<select name="appel">
				<option selected="selected" value="1">OUI</option>
				<option value="2">Oui, partiellement</option>
				<option value="3">NON</option>
				</select>
			</td></tr>
			
			<tr><th>Messagerie interne</th><td colspan="2">
				<select name="messagerie">
				<option selected="selected" value="1">Pas de messagerie interne</option>
				<option value="2">Intégrée dans le réseau pédagogique local ou via des outils dédiés</option>
				<option value="3">Intégrée à l'ENT</option>
				</select>
			</td></tr>
			
			<tr><th>"LSUN - Livret Scolaire Unique Numérique<br>LPC - Livret Personnel de Compétences"</th><td colspan="2">
				<select name="LSUN">
				<option selected="selected" value="1">Réservé à l'équipe pédagogique et de direction</option>
				<option value="2">Ouvert à la communauté éducative</option>
				</select>
			</td></tr>
			
			<tr><th>Service de réservation de ressources</th><td colspan="2">
				<select name="resa">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>
			
			<tr><th>Abonnements à des services d'information et de documentation en ligne</th><td colspan="2">
				<select name="abo">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>
			
			<tr><th>Folios</th><td colspan="2">
				<select name="folios">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>
			
			<tr><th>Abonnements à des ressources numériques pédagogiques éditoriales</th><td colspan="2">
				<select name="abo_ress">
				<option selected="selected" value="1">0</option>
				<option value="2">Entre 1 et 4</option>
				<option value="3">Entre 5 et 9</option>
				<option value="4">Plus de 10</option>
				</select>
			</td></tr>
			
			<tr><th>Proportion de ressources numériques par rapport aux ressources papiers</th><td colspan="2">
				<select name="prop">
				<option selected="selected" value="1">Strictement moins de 10% des ressources sont numériques</option>
				<option value="2">Plus de 10% et strictement moins de 25% des ressources sont numériques</option>
				<option value="3">Plus de 25% et strictement moins de 50% des ressources sont numériques</option>
				<option value="4">50% ou plus des ressources sont numériques</option>
				</select>
			</td></tr>
	</table>
		<input class="btn" type="submit" value="Valider les informations" /><br><br>
		<input type="hidden" name="RNE" value=<?php echo $RNE ?> />
		</form>
		<?php } ?>
 	</div>
</section>
</div>