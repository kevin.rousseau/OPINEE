<?php
	$titre_page = "Comparaison_visu" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
?>

<section>
	<div id="top_section" >
		<h1>Comparaison des établissements</h1>
	</div>
	
	<div id="content">

<?php 

	if (!empty($_POST['RNE']))
	{
		$RNE = $_POST['RNE'];
	}

	else
	{
		$RNE = $_GET['RNE'];
	}

		$etab = $bdd->query('SELECT * FROM equipements WHERE RNE = "'.$_SESSION['RNE'].'"');
		$etab_compar = $bdd->query('SELECT * FROM equipements WHERE RNE = "'.$RNE.'"');
		$etab_nom = $bdd->query('SELECT * FROM etablissements WHERE RNE = "'.$_SESSION['RNE'].'"');
		$etab_compar_nom = $bdd->query('SELECT * FROM etablissements WHERE RNE = "'.$RNE.'"');

	while($donnees = $etab->fetch())
				{
		while($donnees1 = $etab_compar->fetch())
				{
					
?>
<h3>Equipements&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_infra.php?RNE=<?php echo $RNE ; ?>">Infrastructures</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_serv.php?RNE=<?php echo $RNE ; ?>">Services</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_pilo.php?RNE=<?php echo $RNE ; ?>">Pilotage</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_form.php?RNE=<?php echo $RNE ; ?>">Formation</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_uti.php?RNE=<?php echo $RNE ; ?>">Utilisations</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_usa.php?RNE=<?php echo $RNE ; ?>">Usages</a></h3>
		<table>
			<tr><th><h4>Critères</h4></th><th width="18%"><h4><?php while ($donnees2 = $etab_nom -> fetch())
			{
				echo $donnees2['nom']. ' - Modalité';
			?></h4></th><th width="18%"><h4><?php while ($donnees3 = $etab_compar_nom -> fetch())
			{
				echo $donnees3['nom']. ' - Modalité';
			?></h4></th><th width="15%"><h4><?php
				echo $donnees2['nom']. ' - Points';
			?></h4></th><th width="15%"><h4><?php
				echo $donnees3['nom']. ' - Points';
			?></h4></th></tr>

			<tr><th>Nombre moyen d'élèves par terminal</th><td><?php echo $donnees['nb_terminaux_mod']; ?></td><td><?php echo $donnees1['nb_terminaux_mod']; ?></td><td><?php echo $donnees['nb_terminaux_points']; ?> sur 10</td><td><?php echo $donnees1['nb_terminaux_points']; ?> sur 10</td></tr>

			<tr><th>Nombre moyen d'élèves par terminal mobile</th><td><?php echo $donnees['nb_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees1['nb_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees['nb_terminaux_mobiles_points']; ?> sur 20</td><td><?php echo $donnees1['nb_terminaux_mobiles_points']; ?> sur 20</td></tr>

			<tr><th>Nombre moyen d'élèves par poste de travail en accès libre aux élèves en dehors des heures de cours</th><td><?php echo $donnees['nb_postes_libres_mod']; ?></td><td><?php echo $donnees1['nb_postes_libres_mod']; ?></td><td><?php echo $donnees['nb_postes_libres_points']; ?> sur 20</td><td><?php echo $donnees1['nb_postes_libres_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de terminaux de moins de cinq ans</th><td><?php echo $donnees['nb_terminaux_moins_5_ans_mod']; ?></td><td><?php echo $donnees1['nb_terminaux_moins_5_ans_mod']; ?></td><td><?php echo $donnees['nb_terminaux_moins_5_ans_points']; ?> sur 15</td><td><?php echo $donnees1['nb_terminaux_moins_5_ans_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de salles d'enseignement équipées d'un VPI/TNI/TBI</th><td><?php echo $donnees['nb_VPI_TNI_TBI_mod']; ?></td><td><?php echo $donnees1['nb_VPI_TNI_TBI_mod']; ?></td><td><?php echo $donnees['nb_VPI_TNI_TBI_points']; ?> sur 15</td><td><?php echo $donnees1['nb_VPI_TNI_TBI_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de salles d'enseignement équipées d'un vidéo-projecteur</th><td><?php echo $donnees['nb_video_projecteur_mod']; ?></td><td><?php echo $donnees1['nb_video_projecteur_mod']; ?></td><td><?php echo $donnees['nb_video_projecteur_points']; ?> sur 5</td><td><?php echo $donnees1['nb_video_projecteur_points']; ?> sur 5</td></tr>
			
			<tr><th>Dotation des élèves en terminaux mobiles par la collectivité</th><td><?php echo $donnees['dotation_eleves_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees1['dotation_eleves_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees['dotation_eleves_terminaux_mobiles_points']; ?> sur 10</td><td><?php echo $donnees1['dotation_eleves_terminaux_mobiles_points']; ?> sur 10</td></tr>

			<tr><th>Dotation des enseignants en terminaux mobiles par la collectivité</th><td><?php echo $donnees['dotation_enseignants_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees1['dotation_enseignants_terminaux_mobiles_mod']; ?></td><td><?php echo $donnees['dotation_enseignants_terminaux_mobiles_points']; ?> sur 15</td><td><?php echo $donnees1['dotation_enseignants_terminaux_mobiles_points']; ?> sur 15</td></tr>

			<tr><th>Équipements particuliers</th><td colspan="2">Voir tableau suivant</td><td><?php echo $donnees['equipements_particuliers_points']; ?> sur 15</td><td><?php echo $donnees1['equipements_particuliers_points']; ?> sur 15</td></tr>

			<tr><th>Maintenance des équipements par la collectivité</th><td><?php echo $donnees['maintenance_equipement_mod']; ?></td><td><?php echo $donnees1['maintenance_equipement_mod']; ?></td><td><?php echo $donnees['maintenance_equipement_points']; ?> sur 20</td><td><?php echo $donnees1['maintenance_equipement_points']; ?> sur 20</td></tr>
			
			<tr><th>L'EPLE engage-t-il des moyens propres sur la maintenance quotidienne des équipements ?</th><td><?php echo $donnees['engagement_EPLE_mod']; ?></td><td><?php echo $donnees1['engagement_EPLE_mod']; ?></td><td><?php echo $donnees['engagement_EPLE_points']; ?> sur 6</td><td><?php echo $donnees1['engagement_EPLE_points']; ?> sur 6</td></tr>
	</table>
	<table>
		<tr><th><h4>Equipements particuliers</h4></th><th><h4><?php
				echo $donnees2['nom']. ' - Données';
			?></h4></th><th><h4><?php
				echo $donnees3['nom']. ' - Données';
			?></h4></th></tr>
		<tr><th>Malette MP3/ MP4</th><td><?php echo $donnees['equipements_particuliers_malette'] ; ?></td><td><?php echo $donnees1['equipements_particuliers_malette'] ; ?></td></tr>
		<tr><th>Imprimante 3D</th><td><?php echo $donnees['equipements_particuliers_imprimante'] ; ?></td><td><?php echo $donnees1['equipements_particuliers_imprimante'] ; ?></td></tr>
		<tr><th>FabLab</th><td><?php echo $donnees['equipements_particuliers_fablab'] ; ?></td><td><?php echo $donnees1['equipements_particuliers_fablab'] ; ?></td></tr>
		<tr><th>Visioconférence</th><td><?php echo $donnees['equipements_particuliers_visio'] ; ?></td><td><?php echo $donnees1['equipements_particuliers_visio'] ; ?></td></tr>
		<tr><th>Labo Multimédia</th><td><?php echo $donnees['equipements_particuliers_labo'] ; ?></td><td><?php echo $donnees1['equipements_particuliers_labo'] ; ?></td></tr>
		<tr><th>Boîtiers de vote</th><td><?php echo $donnees['equipements_particuliers_boitier'] ; ?></td><td><?php echo $donnees1['equipements_particuliers_boitier'] ; ?></td></tr>
		<tr><th>Visualisateurs</th><td><?php echo $donnees['equipements_particuliers_visu'] ; ?></td><td><?php echo $donnees1['equipements_particuliers_visu'] ; ?></td></tr>
	</table>
	<table>
			<th><h4><?php
				echo $donnees2['nom']. ' - Points';
			?></h4></th><th><h4><?php
				echo $donnees3['nom']. ' - Points';
			?></h4></th><th><h4><?php
				echo $donnees2['nom']. ' - Palier';
			?></h4></th><th><h4><?php
				echo $donnees3['nom']. ' - Palier';
			?></h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 151</td><td><?php echo $donnees1['nb_points_total']; ?> sur 151</td><td><?php echo $donnees['palier_equip'] ; ?> sur 10</td><td><?php echo $donnees1['palier_equip'] ; ?> sur 10</td></tr>
	</table>

<?php
			}
		}
?> 
			
			<?php
			}
		}
		?>			
</div>
</section>
<?php
include('pied_de_page.php');
?>