<?php
	$titre_page = "Pilotage" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');

	$NomEtab = $_SESSION['NomEtab'];
?>

<section>
	<div id="top_section" >
		<h1>Pilotage<?php if ($_SESSION['NomEtab'] != NULL){echo ' - '.$NomEtab ;} else {} ; ?></h1>
	</div>
	
	<div id="content">
 <br />
		<?php if ($_SESSION['Rang'] == 2) 
		{
				$pilotage = $bdd->query('SELECT * FROM pilotage WHERE RNE = "'.$_SESSION['RNE'].'"');
			$queryRNE = $bdd->query('SELECT RNE FROM pilotage WHERE RNE = "'.$_SESSION['RNE'].'"'); 
	$count = $queryRNE->rowCount();  
		if($count == 1) 
			{	
		while($donnees = $pilotage->fetch())
		{
	?>
		<input class="btn" type="submit" value="Modifier les données" onclick="self.location.href='modif_pilo.php'">&nbsp;&nbsp;<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_pilo_ce.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>
	<table>
			<tr><th><h4>Critères</h4></th><th colspan="2"><h4>Données</h4></th><th><h4>Nombre de points</h4></th></tr>
			
			<tr><th>Volet numérique au sein du projet d'établissement</th><td colspan="2"><?php echo $donnees['volet_numerique_projet_mod']; ?></td><td><?php echo $donnees['volet_numerique_projet_points']; ?> sur 10</td></tr>
			
			<tr><th>Charte d'usage du numérique</th><td colspan="2"><?php echo $donnees['charte_usage_numerique_mod']; ?></td><td><?php echo $donnees['charte_usage_numerique_points']; ?> sur 20</td></tr>
			
			<tr><th>Commissions numérique ou conseil pédagogique traitant des sujets numériques</th><td colspan="2"><?php echo $donnees['commission_numerique_mod']; ?></td><td><?php echo $donnees['commission_numerique_points']; ?> sur 20</td></tr>
			
			<tr><th>Personnel(s) référent(s) numérique(s) pour les usages pédagogiques du numérique</th><td colspan="2"><?php echo $donnees['personnels_referents_numeriques_usages_pedagogiques_mod']; ?></td><td><?php echo $donnees['personnels_referents_numeriques_usages_pedagogiques_points']; ?> sur 15</td></tr>
			
			<tr><th>Utilisation d'Indemnités de Mission Particulière (IMP) pour favoriser les usages du numérique</th><td colspan="2"><?php echo $donnees['utilisation_IMP_usages_numeriques_mod']; ?></td><td><?php echo $donnees['utilisation_IMP_usages_numeriques_points']; ?> sur 10</td></tr>
			
			<tr><th>Démarche "zéro papier inutile"</th><td colspan="2"><?php echo $donnees['demarche_zero_papier_inutile_mod']; ?></td><td><?php echo $donnees['demarche_zero_papier_inutile_points']; ?> sur 10</td></tr>
			
			<tr><th>Usage privilégié du numérique par l'équipe de direction et l'équipe administrative</th><td colspan="2"><?php echo $donnees['usage_privilegie_numerique_direction_administration_mod']; ?></td><td><?php echo $donnees['usage_privilegie_numerique_direction_administration_points']; ?> sur 10</td></tr>
			
			<tr><th>Équipements numériques personnels des professeurs acceptés sur le réseau de l'établissement</th><td colspan="2"><?php echo $donnees['equipements_numeriques_personnels_enseignants_acceptes_mod']; ?></td><td><?php echo $donnees['equipements_numeriques_personnels_enseignants_acceptes_points']; ?> sur 20</td></tr>
			
			<tr><th>Équipements numériques personnels des élèves acceptés (pour des usages pédagogiques) sur le réseau de l'établissement</th><td colspan="2"><?php echo $donnees['equipements_numeriques_personnels_eleves_acceptes_mod']; ?></td><td><?php echo $donnees['equipements_numeriques_personnels_eleves_acceptes_points']; ?> sur 5</td></tr>
			
			<tr><th>Sécurité - Identifiants personnels d'accès au réseau</th><td colspan="2"><?php echo $donnees['securite_id_personnels_acces_reseau_mod']; ?></td><td><?php echo $donnees['securite_id_personnels_acces_reseau_points']; ?> sur 20</td></tr>

			<tr><th rowspan="3">Actions d'information et d'accompagnement des usagers au numérique</th><th>Avec les moyens de l'établissement (personnel référent)</th><td><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_etab']; ?></td><td rowspan="3"><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_points']; ?> sur 18</td></tr>
			<tr><th>Par les services académiques</th><td><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_aca']; ?></td></tr>
			<tr><th>Par la collectivité</th><td><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_collec']; ?></td></tr>
		
			<tr><th rowspan="3">Assistance aux usagers</th><th>Avec les moyens de l'établissement (personnel référent)</th><td><?php echo $donnees['assistance_usagers_etab']; ?></td><td rowspan="3"><?php echo $donnees['assistance_usagers_points']; ?> sur 18</td></tr>
			<tr><th>Par les services académiques</th><td><?php echo $donnees['assistance_usagers_aca']; ?></td></tr>
			<tr><th>Par la collectivité</th><td><?php echo $donnees['assistance_usagers_collec']; ?></td></tr>
			
			<tr><th>Suivi statistique des taux d'utilisation des équipements et des services</th><td colspan="2"><?php echo $donnees['suivi_statistique_taux_utilisation_equipements_services_mod']; ?></td><td><?php echo $donnees['suivi_statistique_taux_utilisation_equipements_services_points']; ?> sur 10</td></tr>
			
			<tr><th>Mise en place et suivi des "Services en Ligne"</th><td colspan="2"><?php echo $donnees['mise_en_place_suivi_services_en_ligne_mod']; ?></td><td><?php echo $donnees['mise_en_place_suivi_services_en_ligne_points']; ?> sur 20</td></tr>
			
			<tr><th>Échanges entre le chef d'établissement et les corps d'inspection pour le développement des usages du numérique</th><td colspan="2"><?php echo $donnees['echanges_chef_etablissement_inspection_developpement_mod']; ?></td><td><?php echo $donnees['echanges_chef_etablissement_inspection_developpement_points']; ?> sur 15</td></tr>
	</table>

	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 154</td><td><?php echo $donnees['palier_pilo'] ; ?> sur 10</td></tr>
	</table>



		<?php
		}
			}
			 else {
	 	?>

	 	<h2>Aucune donnée rentrée pour cet établissement !</h2>
	 	<input class="btn" type="submit" value="Insérer les données" onclick="self.location.href='insert_pilo.php?RNE=<?php echo $_SESSION['RNE'] ; ?> '">

	 	<?php
	 }
		}
	else
	{

if (empty($_GET['RNE']) AND (empty($_POST['RNE'])))
{
?> <h3>Veuillez tout d'abord choisir un établissement sur la page d'accueil</h3> <?php
}
	else
	{
		$RNE2 = $_GET['RNE'];
		$_SESSION['RNE'] = $_GET['RNE'];
		
	$pilo = $bdd->query('SELECT * FROM pilotage WHERE RNE = "'.$RNE2.'"');
	$queryRNE = $bdd->query('SELECT RNE FROM pilotage WHERE RNE = "'.$RNE.'"'); 
	$count = $queryRNE->rowCount();  
	if($count == 1) 
		{
		while($donnees = $pilo->fetch())
		{
	?>
		<input class="btn" type="submit" value="Modifier les données" onclick="self.location.href='modif_pilo.php'">&nbsp;&nbsp;<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_pilo.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>

	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Modalité retenue</h4></th><th><h4>Nombre de points</h4></th></tr>
			
			<tr><th>Volet numérique au sein du projet d'établissement</th><td><?php echo $donnees['volet_numerique_projet_mod']; ?></td><td><?php echo $donnees['volet_numerique_projet_points']; ?> sur 10</td></tr>
			
			<tr><th>Charte d'usage du numérique</th><td><?php echo $donnees['charte_usage_numerique_mod']; ?></td><td><?php echo $donnees['charte_usage_numerique_points']; ?> sur 20</td></tr>
			
			<tr><th>Commissions numérique ou conseil pédagogique traitant des sujets numériques</th><td><?php echo $donnees['commission_numerique_mod']; ?></td><td><?php echo $donnees['commission_numerique_points']; ?> sur 20</td></tr>
			
			<tr><th>Personnel(s) référent(s) numérique(s) pour les usages pédagogiques du numérique</th><td><?php echo $donnees['personnels_referents_numeriques_usages_pedagogiques_mod']; ?></td><td><?php echo $donnees['personnels_referents_numeriques_usages_pedagogiques_points']; ?> sur 15</td></tr>
			
			<tr><th>Utilisation d'Indemnités de Mission Particulière (IMP) pour favoriser les usages du numérique</th><td><?php echo $donnees['utilisation_IMP_usages_numeriques_mod']; ?></td><td><?php echo $donnees['utilisation_IMP_usages_numeriques_points']; ?> sur 10</td></tr>
			
			<tr><th>Démarche "zéro papier inutile"</th><td><?php echo $donnees['demarche_zero_papier_inutile_mod']; ?></td><td><?php echo $donnees['demarche_zero_papier_inutile_points']; ?> sur 10</td></tr>
			
			<tr><th>Usage privilégié du numérique par l'équipe de direction et l'équipe administrative</th><td><?php echo $donnees['usage_privilegie_numerique_direction_administration_mod']; ?></td><td><?php echo $donnees['usage_privilegie_numerique_direction_administration_points']; ?> sur 10</td></tr>
			
			<tr><th>Équipements numériques personnels des professeurs acceptés sur le réseau de l'établissement</th><td><?php echo $donnees['equipements_numeriques_personnels_enseignants_acceptes_mod']; ?></td><td><?php echo $donnees['equipements_numeriques_personnels_enseignants_acceptes_points']; ?> sur 20</td></tr>
			
			<tr><th>Équipements numériques personnels des élèves acceptés (pour des usages pédagogiques) sur le réseau de l'établissement</th><td><?php echo $donnees['equipements_numeriques_personnels_eleves_acceptes_mod']; ?></td><td><?php echo $donnees['equipements_numeriques_personnels_eleves_acceptes_points']; ?> sur 5</td></tr>
			
			<tr><th>Sécurité - Identifiants personnels d'accès au réseau</th><td><?php echo $donnees['securite_id_personnels_acces_reseau_mod']; ?></td><td><?php echo $donnees['securite_id_personnels_acces_reseau_points']; ?> sur 20</td></tr>

			<tr><th>Actions d'information et d'accompagnement des usagers au numérique</th><td>Voir tableau suivant</td><td><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_points']; ?> sur 18</td></tr>
		
			<tr><th>Assistance aux usagers</th><td>Voir tableau suivant</td><td><?php echo $donnees['assistance_usagers_points']; ?> sur 18</td></tr>
			
			<tr><th>Suivi statistique des taux d'utilisation des équipements et des services</th><td><?php echo $donnees['suivi_statistique_taux_utilisation_equipements_services_mod']; ?></td><td><?php echo $donnees['suivi_statistique_taux_utilisation_equipements_services_points']; ?> sur 10</td></tr>
			
			<tr><th>Mise en place et suivi des "Services en Ligne"</th><td><?php echo $donnees['mise_en_place_suivi_services_en_ligne_mod']; ?></td><td><?php echo $donnees['mise_en_place_suivi_services_en_ligne_points']; ?> sur 20</td></tr>
			
			<tr><th>Échanges entre le chef d'établissement et les corps d'inspection pour le développement des usages du numérique</th><td><?php echo $donnees['echanges_chef_etablissement_inspection_developpement_mod']; ?></td><td><?php echo $donnees['echanges_chef_etablissement_inspection_developpement_points']; ?> sur 15</td></tr>
	</table>
	
	<table class="tabledouble">
	<tr>
	<td class="tabledouble">
	<table>
			<tr><th><h4>Actions d'information et d'accompagnement des usagers au numérique</h4></th><th><h4>Données</h4></th></tr>
			<tr><th>Avec les moyens de l'établissement (personnel référent)</th><td><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_etab']; ?></td></tr>
			<tr><th>Par les services académiques</th><td><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_aca']; ?></td></tr>
			<tr><th>Par la collectivité</th><td><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_collec']; ?></td></tr>
	</table>
</td>
<td class="tabledouble">
	<table>
			<tr><th><h4>Assistance aux usagers</h4></th><th><h4>Données</h4></th></tr>
			<tr><th>Avec les moyens de l'établissement (personnel référent)</th><td><?php echo $donnees['assistance_usagers_etab']; ?></td></tr>
			<tr><th>Par les services académiques</th><td><?php echo $donnees['assistance_usagers_aca']; ?></td></tr>
			<tr><th>Par la collectivité</th><td><?php echo $donnees['assistance_usagers_collec']; ?></td></tr>
	</table>
</td>
</tr>
</table>
	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 221</td><td><?php echo $donnees['palier_pilo'] ; ?> sur 10</td></tr>
	</table>


		<?php
		}
	}
else
{
	?><h2>Aucune notation effectuée sur cet établissement</h2><br>
	<?php
}
}
	}
?>
</div>
</section>

<?php
include('pied_de_page.php');
?>