<?php
	ob_start() ;
	include('header.php');
	
	$requete1 = $bdd->query('SELECT * FROM usages WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$NomEtab = $_SESSION['NomEtab'];

setlocale(LC_TIME, 'fra_fra');
date_default_timezone_set('Europe/Paris');
?>
	<style>
img.img_gauche
{
	width: 25% ;
	height:80%;
	float: left ;
}
		
img.img_droite
{
	width:20% ;
	height:90%;
	float: right ;
	margin-top: -15 ;
}
		

h2
{
	color : #02A4E4;
	text-align:center;
	margin-top: 0px;
}

h3
{
	color: #E54986 ;
	text-align:center;			
}
		
h4
{
	color : #4A51A9;
    margin-bottom: 3px;
    margin-top: 3px;
    text-align:center;
}

h5
{
	color : #4A51A9;
	position: absolute;
	bottom : 13px;
	text-align:right; 
}

.top
{
	height: 50px ;
}

.corps
{
		width: 95%;
}
		
table
{
	border: 1px #4A51A9 solid;
	border-collapse : collapse ;
	width: 79%;
	text-align:center;
}

th
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:78%;
	margin-top: -10px;
}

th.mod
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:30%;
}

th.critere
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:80%;
}

th.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:17%;
}

th.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:80%;
}

th.donnee
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:30%;
}

th.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:50%;
}

th.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:10%;
}

th.domaine
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

th.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

td
{
	border: 1px #4A51A9 solid ;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnee
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:17%;
}

td.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:80%;
}

td.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:50%;
}

td.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:5%;
}
	</style>
	<page backtop="60px" backleft="-2mm" backright="-2mm">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Usages - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th class="critere"><h4>Critères</h4></th>
						<th class="donnee"><h4>Modalité retenue</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete1->fetch())
			{
		?>

			<tr><th class="critere">Pourcentage d'élèves hors classes de 3ème ayant une validation partielle du B2i Collège</th><td class="donnee"><?php echo $donnees['pourcentage_eleves_validation_partielle_B2i_mod']; ?></td><td class="points"><?php echo $donnees['pourcentage_eleves_validation_partielle_B2i_points']; ?> sur 5</td></tr>

			<tr><th class="critere">Proportion de professeurs impliqués dans la certification des compétences numériques des élèves (B2i)</th><td class="donnee"><?php echo $donnees['proportion_enseignants_impliques_certification_competence_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_impliques_certification_competence_points']; ?> sur 5</td></tr>

			<tr><th class="critere">Nombre de disciplines impliquées dans la validation des compétences du B2i</th><td class="donnee"><?php echo $donnees['nb_discipline_validation_competences_B2i_mod']; ?></td><td class="points"> <?php echo $donnees['nb_discipline_validation_competences_B2i_points']; ?> sur 10</td></tr>

			<tr><th class="critere">Proportion de professeurs développant des usages pédagogiques du numérique</th><td class="donnee"><?php echo $donnees['proportion_enseignants_developpant_usage_numerique_mod']; ?></td><td class="points"><?php echo $donnees['proportion_enseignants_developpant_usage_numerique_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Proportion de manuels scolaires numériques dans l'EPLE</th><td class="donnee"><?php echo $donnees['proportion_manuels_scolaires_numeriques_EPLE_mod']; ?></td><td class="points"> <?php echo $donnees['proportion_manuels_scolaires_numeriques_EPLE_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Usages de services et ressources pédagogiques numériques institutionnels (D'Col, EFS, éduthèque, Fondamentaux, etc.)</th><td class="donnee"><?php echo $donnees['usages_services_ressources_numeriques_mod']; ?></td><td class="points"> <?php echo $donnees['usages_services_ressources_numeriques_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Usages d'une messagerie institutionnelle (académique ou dans l'ENT) pour les échanges professionnels</th><td class="donnee"><?php echo $donnees['usages_messagerie_institutionnelle_echanges_professionnel_mod']; ?></td><td class="points"><?php echo $donnees['usages_messagerie_institutionnelle_echanges_professionnel_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Usages de la messagerie de l'ENT pour les échanges des professeurs avec les familles (si ENT)</th><td class="donnee"><?php echo $donnees['usages_messagerie_ENT_enseignants_famille_mod']; ?></td><td class="points"><?php echo $donnees['usages_messagerie_ENT_enseignants_famille_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Usages d'espaces de stockage ou de partage de documents pédagogiques</th><td class="donnee"><?php echo $donnees['usages_espaces_stockages_partage_documents_pedagogiques_mod']; ?></td><td class="points"><?php echo $donnees['usages_espaces_stockages_partage_documents_pedagogiques_points']; ?> sur 10</td></tr>

			<tr><th class="critere">Usages d'outils numériques pour des pratiques collaboratives entre ensiegnants</th><td class="donnee"><?php echo $donnees['usages_outils_numeriques_entre_enseignants_mod']; ?></td><td class="points"> <?php echo $donnees['usages_outils_numeriques_entre_enseignants_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Usages d'outils numériques pour des pratiques collaboratives des enseignants avec leurs élèves</th><td class="donnee"><?php echo $donnees['usages_outils_numeriques_entre_enseignants_eleves_mod']; ?></td><td class="points"><?php echo $donnees['usages_outils_numeriques_entre_enseignants_eleves_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Usages connus de réseaux sociaux dans le cadre pédagogique</th><td class="donnee"><?php echo $donnees['usages_reseaux_sociaux_pedagogiques_mod']; ?></td><td class="points"><?php echo $donnees['usages_reseaux_sociaux_pedagogiques_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Usages connus de services de publication de type blog pour l'enseignement</th><td class="donnee"><?php echo $donnees['usages_services_publication_pour_enseignement_mod']; ?></td><td class="points"><?php echo $donnees['usages_services_publication_pour_enseignement_points']; ?> sur 15</td></tr>

			<tr><th class="critere">Création de médias numériques (journal, radio, vidéo, exposition) connue et validée par le chef d'établissement</th><td class="donnee"><?php echo $donnees['creation_medias_numeriques_mod']; ?></td><td class="points"><?php echo $donnees['creation_medias_numeriques_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Utilisation d'outils et de ressources numériques pour le développement de la pratique de l'oral (baladodiffusion, labo mutimédia, etc.)</th><td class="donnee"><?php echo $donnees['utilisation_outils_ressources_numeriques_dvlp_oral_mod']; ?></td><td class="points"><?php echo $donnees['utilisation_outils_ressources_numeriques_dvlp_oral_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Usages de services numériques de suivi de la maîtrise des compétences (ex : LPC, LSUN, etc.)</th><td class="donnee"><?php echo $donnees['usages_services_numeriques_suivi_competences_mod']; ?></td><td class="points"><?php echo $donnees['usages_services_numeriques_suivi_competences_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Usages du numérique à des fins de personnalisation des parcours et d'individualisation des enseignements</th><td class="donnee"><?php echo $donnees['usages_numeriques_personnalisation_parcours_individu_mod']; ?></td><td class="points" class="points"><?php echo $donnees['usages_numeriques_personnalisation_parcours_individu_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Usages du numérique dans le cadre de la liaison inter-degré</th><td class="donnee"><?php echo $donnees['usages_numeriques_liaison_inter_degre_mod']; ?></td><td class="points"><?php echo $donnees['usages_numeriques_liaison_inter_degre_points']; ?> sur 20</td></tr>

				</tbody>
			</table>
			<br>

			<table align="center" border="none">
			<tr>
	<td border="none">
	<table>
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th>
				<th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 280</td><td class="palier"><?php echo $donnees['palier_usa'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	</td>
	</tr></table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
	</page>
<?php
	
	$content = ob_get_clean() ;
	//die($content) ;
	require('html2pdf/html2pdf.class.php');
	try{
		$pdf = new HTML2PDF('L','A4','fr') ;
		$pdf->pdf->SetDisplayMode('fullpage') ;
		$pdf->writeHTML($content) ;
		$pdf->Output('Equipement.pdf') ;
	}catch (HTML2PDF_exception $e){
		die($e) ;
	}
	
?>
