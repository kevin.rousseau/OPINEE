<?php
	$titre_page = "Nouveau CE" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');
?>

<div id="contenu">
<section>
	<div id="top_section" >
		<h1>Nouveau CE</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	
	<div id="content">
	<h2>Veuillez renseigner les informations pour le nouveau chef d'établissement</h2>
	<form method="post" action="para_new_ce_php.php">
		<table>
			<tr><th>Nom</th><td><input class="text" type="text" name="nom" /></td></tr>
			<tr><th>Prénom</th><td><input class="text" type="text" name="prenom" /></td></tr>
			<tr><th>Pseudo</th><td><input class="text" type="text" name="pseudo" /></td></tr>
			<tr><th>Mot de passe (provisoire)</th><td><input class="text" type="text" name="mdp" /></td></tr>
			<tr><th>RNE</th><td><input class="text" type="text" name="rne" /></td></tr>
		</table>
	<input class="btn" type="submit" value="Valider" />
</form>

<h3><font color="red">Attention, vous devez inserer l'etablissement correspondant au RNE du nouveau chef d'etablissement avant d'inserer celui-ci !!</font></h3>
</div>
</section>
</div>