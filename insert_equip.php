<?php
	$titre_page = "Insert_equipement" ;
	include('header.php');
	include('menu.php');
?>

	<section>
	<div id="top_section" >
		<h1>Insertion des données</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	
	<div id="content">
	<?php
		if(!empty($msg_error))
		{
			echo '<div id="msg_error_2">'.$msg_error.'</div>' ;
		}

		else
		{

		$RNE = $_SESSION['RNE'];
	 ?>
		<br />
	
	<table>

	<form method="post" action="insert_equip_php.php?RNE=<?php echo $RNE ; ?>" enctype="multipart/form-data">

	<tr><th><h4>Critères</h4></th><th colspan="2"><h4>Données</h4></th></tr>

	<tr><th>Nombre de terminaux (ordinateur fixe, ordinateur portable, tablette)</th><td colspan="2"><input class="text" type="text" name="nb_terminaux_text" /></td></tr>

	<tr><th>Nombre de terminaux mobiles (ordinateur portable, tablette, poste de classes mobiles)</th><td colspan="2"><input class="text" type="text" name="nb_terminaux_mobiles_text" /></td></tr>

	<tr><th>Nombre de postes de travail en accès libre aux élèves en dehors des cours (hors classe)</th><td colspan="2"><input class="text" type="text" name="nb_postes_libres_text" /></td></tr>

	<tr><th>Nombre de terminaux de moins de cinq ans</th><td colspan="2"><input class="text" type="text" name="nb_terminaux_moins_5_ans_text" /></td></tr>

	<tr><th>Nombre de VPI/TNI/TBI</th><td colspan="2"><input class="text" type="text" name="nb_VPI_TNI_TBI_text" /></td></tr>

	<tr><th>Nombre de vidéo-projecteurs</th><td colspan="2"><input class="text" type="text" name="nb_video_projecteur_text" /></td></tr>

	<tr><th>Dotation des élèves en terminaux mobiles (ordinateur portable, tablette) par la collectivité</th><td colspan="2">
	<select name="Dot_ele">
		<option selected="selected" value="Aucune">Aucune</option>
		<option value="Quelques élèves">Quelques élèves</option>
		<option value="Une classe">Une classe</option>
		<option value="Plusieurs classes">Plusieurs classes</option>
		<option value="Toutes les classes">Toutes les classes</option>
	</select></td></tr>

	<tr><th>Dotation des enseignants en terminaux mobiles (ordinateur portable, tablette) par la collectivité</th><td colspan="2">
	<select name="Dot_ens">
		<option selected="selected" value="Aucune">Aucune</option>
		<option value="Quelques enseignants">Quelques enseignants</option>
		<option value="Tous les enseignants">Tous les enseignants</option>
	</select></td></tr>

	<tr><th rowspan="7">Equipements particuliers</th>
	<th>Malette MP3/MP4</th><td>
		<select name="malette">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>
	<tr><th>Imprimante 3D</th><td>
		<select name="imprimante">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>
	<tr><th>FabLab</th><td>
		<select name="fablab">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>
	<tr><th>Visioconférence</th><td>
		<select name="visio">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>
	<tr><th>Labo Multimédia</th><td>
		<select name="labo">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>
	<tr><th>Boîtiers de vote</th><td>
		<select name="vote">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>
	<tr><th>Visualisateurs</th><td>
		<select name="visua">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>

	<tr><th>Maintenance des équipements (maintien en condition opérationnelle) par la collectivité</th><td colspan="2">
		<select name="maint">
		<option selected="selected" value="OUI">OUI</option>
		<option value="OUI, insuffisamment">OUI, insuffisamment</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>

	<tr><th>L'EPLE engage-til des moyens propres (budget, décharge, IMP, etc.) sur la maintenance quotidienne des équipements ?</th><td colspan="2">
		<select name="eple">
		<option selected="selected" value="OUI">OUI</option>
		<option value="NON">NON</option>
	</select>
	</td></tr>
	</table>
		<input class="btn" type="submit" value="Valider les informations" />
		<input type="hidden" name="RNE" value=<?php echo $RNE ?> />
		</form>
		<?php } ?>
 	</div>
</section>