<?php
	$titre_page = "Mon compte" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
?>

<section>
	<div id="top_section" >
		<h1>Mon compte</h1>
	</div>
	
	<div id="content">

	<?php if ($_SESSION['Rang'] == 2) 
		{
	$ce = $bdd->query('SELECT * FROM ce WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$etablissement = $bdd->query('SELECT * FROM etablissements WHERE RNE = "'.$_SESSION['RNE'].'"');

		while($donnees = $etablissement->fetch())
			{
			while ($donnees2 = $ce ->fetch())
				{
	?>
<br><br>

	<table>
			<tr><th><h4>Données</h4></th><td><h4>Informations</h4></td></tr>
			<tr><th>Prénom</th><td><?php echo $donnees2['prenomCE'] ; ?></td></tr>
			<tr><th>Nom</th><td><?php echo $donnees2['nomCE'] ; ?></td></tr>
			<tr><th>Pseudo</th><td><?php echo $donnees2['pseudoCE'] ; ?></td></tr>
			<tr><th>Mot de passe</th><td><?php echo $donnees2['mdpCE'] ; ?></td></tr>
	</table>

	<form method="post" action="modif_info_ce.php">
	<input class="btn" type="submit" value="Modifier mes informations" />
	</form>

	<br>

	<table>
			<tr><th><h4>Données</h4></th><th><h4>Informations</h4></th></tr>
			<tr><th>Nom de l'établissement</th><td><?php echo $donnees['nom']; ?></td></tr>
			<tr><th>Ville de l'établissement</th><td><?php echo $donnees['ville']; ?></td></tr>
			<tr><th>Nom et prénom du principal de l'établissement</th><td><?php echo $donnees2['prenomCE']." ".$donnees2['nomCE'] ; ?></td></tr></tr>
			<tr><th>UAI de l'établissement</th><td><?php echo $_SESSION['RNE'] ; ?></td></tr>
			<tr><th>Nombre d'élèves</th><td><?php echo $donnees['nb_eleves'] ; ?></td></tr>
			<tr><th>Nombre d'enseignants</th><td><?php echo $donnees['nb_enseignants'] ; ?></td></tr>
			<tr><th>Nombre de salles d'enseignements</th><td><?php echo $donnees['nb_salles_enseignements'] ; ?></td></tr>
			<tr><th>Site de l'établissement</th><td><?php echo $donnees['URL_etablissement'] ; ?></td></tr>
	</table>

		<form method="post" action="modif_info.php">
		<input class="btn" type="submit" value="Modifier les informations de l'établissement" />
		</form>

		<br>

	<?php
				}
			}
		}
		else
		{

			if ($_SESSION['Rang'] == 1)
			{
				$admin = $bdd->query('SELECT * FROM admin WHERE pseudoAdmin = "'.$_SESSION['PseudoAdmin'].'"');
				while($donnees = $admin->fetch())
				{
					?>
					<br><br>

	<table>
			<tr><th><h4>Données</h4></th><td><h4>Informations</h4></td></tr>
			<tr><th>Prénom</th><td><?php echo $donnees['prenomAdmin'] ; ?></td></tr>
			<tr><th>Nom</th><td><?php echo $donnees['nomAdmin'] ; ?></td></tr>
			<tr><th>Pseudo</th><td><?php echo $donnees['pseudoAdmin'] ; ?></td></tr>
			<tr><th>Mot de passe</th><td><?php echo $donnees['mdpAdmin'] ; ?></td></tr>
	</table>

	<form method="post" action="modif_info_admin.php">
	<input class="btn" type="submit" value="Modifier mes informations" />
	</form>
	<?php
			}
		}

		elseif ($_SESSION['Rang'] == 3)
		{
			$dan = $bdd->query('SELECT * FROM dan WHERE pseudoDAN = "'.$_SESSION['PseudoDAN'].'"');
			while($donnees = $dan->fetch())
				{
					?>
					<br><br>

	<table>
			<tr><th><h4>Données</h4></th><td><h4>Informations</h4></td></tr>
			<tr><th>Prénom</th><td><?php echo $donnees['prenomDan'] ; ?></td></tr>
			<tr><th>Nom</th><td><?php echo $donnees['nomDan'] ; ?></td></tr>
			<tr><th>Pseudo</th><td><?php echo $donnees['pseudoDAN'] ; ?></td></tr>
			<tr><th>Mot de passe</th><td><?php echo $donnees['mdpDAN'] ; ?></td></tr>
	</table>

	<form method="post" action="modif_info_dan.php">
	<input class="btn" type="submit" value="Modifier mes informations" />
	</form>
	<?php
			}
		}
		else
		{
			$ci = $bdd->query('SELECT * FROM ci WHERE pseudoCI = "'.$_SESSION['PseudoCI'].'"');
			while($donnees = $ci->fetch())
				{
					?>
					<br><br>

	<table>
			<tr><th><h4>Données</h4></th><td><h4>Informations</h4></td></tr>
			<tr><th>Prénom</th><td><?php echo $donnees['prenomCI'] ; ?></td></tr>
			<tr><th>Nom</th><td><?php echo $donnees['nomCI'] ; ?></td></tr>
			<tr><th>Pseudo</th><td><?php echo $donnees['pseudoCI'] ; ?></td></tr>
			<tr><th>Mot de passe</th><td><?php echo $donnees['mdpCI'] ; ?></td></tr>
	</table>

	<form method="post" action="modif_info_ci.php">
	<input class="btn" type="submit" value="Modifier mes informations" />
	</form>
	<?php
			}
		}
	}
	?>
</div>
</section>
<?php
	include('pied_de_page.php');
	?>