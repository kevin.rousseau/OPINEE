<?php
	$titre_page = "Utilisations" ;
	include('header.php');
	include('menu.php');
	include('footer.php');
?>

<section>
	<div id="top_section" >
		<h1>Utilisations</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	
	<div id="content">
<br />
		<?php 
					$uti = $bdd->query('SELECT * FROM utilisations WHERE RNE = "'.$_SESSION['RNE'].'"');
		while($donnees = $uti->fetch())
		{
	?>

	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Nombre de points</h4></th></tr>

			<tr><th>Taux de réservation des salles informatiques</th><td><?php echo $donnees['taux_reservation_salles_informatiques_points']; ?> sur 10</td></tr>

			<tr><th>Taux de réservation des classes mobiles</th><td><?php echo $donnees['taux_reservation_classes_mobiles_points']; ?> sur 20</td></tr>

			<tr><th>Proportion d'enseignants utilisant régulièrement des VPI/TNI/TBI</th><td><?php echo $donnees['proportion_enseignants_utilisants_VPI_TNI_TBI_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de professeurs remplissant le service de notes au moins mensuellement</th><td><?php echo $donnees['proportion_enseignants_rempli_service_notes_mensuellement_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de professeurs remplissant le cahier de textes en ligne à l'issue de chaque cours</th><td><?php echo $donnees['proportion_enseignants_remplissant_cahier_texte_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de parents d'élèves consultant les services de vie scolaire au moins une fois par semaine</th><td><?php echo $donnees['proportion_parent_consultant_services_vie_scolaire_points']; ?> sur 10</td></tr>

			<tr><th>Proportion d'élèves se connectant chaque jour au réseau interne de l'établissement</th><td><?php echo $donnees['proportion_eleve_connexion_reseau_interne_points']; ?> sur 20</td><tr>

			<tr><th>Fréquentation du site public de l'établissement</th><td><?php echo $donnees['frequentation_site_public_etablissements_points']; ?> sur 10</td></tr>

			<tr><th>Équipements numériques utilisés régulièrement par des personnes extérieures à l'établissement</th><td><?php echo $donnees['equipement_numeriques_utilise_regu_personne_exter_points']; ?> sur 5</td></tr>
	</table>

	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 135</td><td><?php echo $donnees['palier_uti'] ; ?> sur 10</td></tr>
	</table>

		<?php
		}
?>
 	</div>
</section>