<?php
	$titre_page = "Modif_pilo" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');
?>

<div id="contenu">
	<section>
	<div id="top_section" >
		<h1>Modifications des données</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	
	<div id="content">
	<?php
		if(!empty($msg_error))
		{
			echo '<div id="msg_error_2">'.$msg_error.'</div>' ;
		}

		else
		{

		$RNE = $_SESSION['RNE'];
	 ?>

		<br />

	<table>

		<form method="post" action="modif_pilo_php.php?RNE=<?php echo $RNE ; ?>" enctype="multipart/form-data">
		
			<tr><th><h4>Critères</h4></th><th colspan="2"><h4>Données</h4></th></tr>

			<tr><th>Volet numérique au sein du projet d'établissement</th><td colspan="2">
				<select name="volet">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Charte d'usage du numérique</th><td colspan="2">
				<select name="charte">
				<option selected="selected" value="1">Oui, signée par tous les usagers</option>
				<option value="2">Oui, signée par une partie des usagers</option>
				<option value="3">NON</option>
				</select>
			</td></tr>

			<tr><th>Commissions numérique ou conseil pédagogique traitant des sujets numériques</th><td colspan="2">
				<select name="commission">
				<option selected="selected" value="1">Réunion pluriannuelle</option>
				<option value="2">Réunion annuelle</option>
				<option value="3">NON</option>
				</select>
			</td></tr>

			<tr><th>Personnel(s) référent(s) numérique(s) pour les usages pédagogiques du numérique</th><td colspan="2">
				<select name="referent">
				<option selected="selected" value="1">Oui, uniquement</option>
				<option value="2">Oui, entre autres missions "numériques"</option>
				<option value="3">NON</option>
				</select>
			</td></tr>

			<tr><th>Utilisation d'Indemnités de Mission Particulière (IMP) pour favoriser les usages du numérique</th><td colspan="2">
				<select name="IMP">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Démarche "zéro papier inutile"</th><td colspan="2">
				<select name="demarche">
				<option selected="selected" value="1">Totalement mise en place</option>
				<option value="2">Partiellement mise en place</option>
				<option value="3">En projet</option>
				<option value="4">NON</option>
				</select>
			</td></tr>

			<tr><th>Usage privilégié du numérique par l'équipe de direction et l'équipe administrative</th><td colspan="2">
				<select name="usage">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Équipements numériques personnels des professeurs acceptés sur le réseau de l'établissement</th><td colspan="2">
				<select name="prof">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Équipements numériques personnels des élèves acceptés (pour des usages pédagogiques) sur le réseau de l'établissement</th><td colspan="2">
				<select name="eleve">
				<option selected="selected" value="1">Ordinateurs portables et tablettes</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Sécurité - Identifiants personnels d'accès au réseau</th><td colspan="2">
				<select name="secu">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th rowspan="3">Actions d'information et d'accompagnement des usagers au numérique</th><th>Avec les moyens de l'établissement (personnel référent)</th><td>
				<select name="action_etab">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>
			<tr><th>Par les services académiques</th><td>
				<select name="action_aca">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>
			<tr><th>Par la collectivité</th><td>
				<select name="action_collec">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th rowspan="3">Assistance aux usagers</th><th>Avec les moyens de l'établissement (personnel référent)</th><td>
				<select name="assist_etab">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>
			<tr><th>Par les services académiques</th><td>
				<select name="assist_aca">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>
			<tr><th>Par la collectivité</th><td>
				<select name="assist_collec">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Suivi statistique des taux d'utilisation des équipements et des services</th><td colspan="2">
				<select name="suivi">
				<option selected="selected" value="1">Oui, suivi fin</option>
				<option value="2">Oui, suivi global</option>
				<option value="3">NON</option>
				</select>
			</td></tr>
			
			<tr><th>Mise en place et suivi des "Services en Ligne"</th><td colspan="2">
				<select name="services_ligne">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>

			<tr><th>Échanges entre le chef d'établissement et les corps d'inspection pour le développement des usages du numérique</th><td colspan="2">
				<select name="action_">
				<option selected="selected" value="1">Généralisés</option>
				<option value="2">Limités</option>
				<option value="3">Rares</option>
				</select>
			</td></tr>
	</table>
		<input class="btn" type="submit" value="Valider les informations" /><br><br>
		<input type="hidden" name="RNE" value=<?php echo $RNE ?> />
		</form>
		<?php } ?>
 	</div>
</section>
</div>