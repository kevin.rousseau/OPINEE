<?php
	$titre_page = "Modif_admin" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');

	$error = 0;

	if(isset($_POST['prenom']) and isset($_POST['nom']) and isset($_POST['pseudo']) and isset($_POST['mdp']))							
	{
		if(!empty($_POST['prenom']) and !empty($_POST['nom']) and !empty($_POST['pseudo']) and !empty($_POST['mdp']))
	{
		if ($_POST['pseudo'] == $_SESSION['PseudoAdmin'])
		{
					$prenom = $_POST['prenom'] ;
					$nom = $_POST['nom'] ;
					$pseudo = $_POST['pseudo'] ;
					$mdp = $_POST['mdp'] ;

					$requete2 = $bdd->prepare('UPDATE admin SET prenomAdmin = :prenom, nomAdmin = :nom , pseudoAdmin = :pseudo, mdpAdmin = :mdp WHERE pseudoAdmin = "'.$_SESSION['PseudoAdmin'].'"');
					$requete2->execute(array(
						'nom' => $nom,
						'prenom' => $prenom,
						'pseudo' => $pseudo,
						'mdp' => $mdp
						));

		header('refresh:0;url=confirm_modif_info_admin.php') ;
		}
		else
		{

			$reponse = $bdd->query('SELECT pseudoAdmin FROM admin WHERE pseudoAdmin = "' . $_POST['pseudo'] . '" ');
			$login = $reponse->fetch();
			
			if (strtolower($_POST['pseudo']) == strtolower($login['pseudoAdmin']))
			{
				$error = 1;
				$msg_error = "Pseudo deja utiliser, veuillez en choisir un autre" ;
			}

				else
				{
					
					$prenom = $_POST['prenom'] ;
					$nom = $_POST['nom'] ;
					$pseudo = $_POST['pseudo'] ;
					$mdp = $_POST['mdp'] ;

					$requete2 = $bdd->prepare('UPDATE admin SET prenomAdmin = :prenom, nomAdmin = :nom , pseudoAdmin = :pseudo, mdpAdmin = :mdp WHERE pseudoAdmin = "'.$_SESSION['PseudoAdmin'].'"');
					$requete2->execute(array(
						'nom' => $nom,
						'prenom' => $prenom,
						'pseudo' => $pseudo,
						'mdp' => $mdp
						));

					$_SESSION['PseudoAdmin'] = $_POST['pseudo'];

		header('refresh:0;url=confirm_modif_info_admin.php') ;
				}
			}

	}
	else
	{
		$error = 2;
		$msg_error = "Vous n'avez pas rempli tout les champs, veuillez réessayer" ;
	}
}
?>
<div id="contenu">
	<section>
	<div id="top_section" >
		<h1>Modifications des informations</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	
	<div id="content">
	<?php		
		$admin = $bdd->query('SELECT * FROM admin WHERE pseudoAdmin = "'.$_SESSION['PseudoAdmin'].'"'); ?>


	<form method="post" action="modif_info_admin.php" enctype="multipart/form-data">
		<br>

		<table>
		<?php
			while($donnees2 = $admin->fetch())
			{
	?>

			<tr><th><h4>Données</h4></th><td><h4>Informations</h4></td></tr>
			<tr><th>Prénom</th><td><input class="text" type="text" name="prenom" value="<?php echo $donnees2['prenomAdmin'] ; ?>" /></td></tr>
			<tr><th>Nom</th><td><input class="text" type="text" name="nom" value="<?php echo $donnees2['nomAdmin'] ; ?>" /></td></tr>
			<tr><th>Pseudo</th><td><input class="text" type="text" name="pseudo" value="<?php echo $donnees2['pseudoAdmin'] ; ?>" /></td></tr>
			<tr><th>Mot de passe</th><td><input class="text" type="text" name="mdp" value="<?php echo $donnees2['mdpAdmin'] ; ?>" /></td></tr>
</table>
<?php 
}
 ?>
		<input class="btn" type="submit" value="Valider les informations" /><br><br>
		</form>
		<?php
			if($error == 1)
			{
		?>

				<div id="msg_error">
					<?php echo '<p>'.$msg_error.'</p>' ; ?>
				</div>
		
		<?php
			}

			elseif($error == 2)
			{
		?>

				<div id="msg_error">
					<?php echo '<p>'.$msg_error.'</p>' ; ?>
				</div>
		
		<?php
			}
			?>
		</div>
		</section>
		</div>