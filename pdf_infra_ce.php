<?php
	ob_start() ;
	include('header.php');
	
	$requete1 = $bdd->query('SELECT * FROM infrastructures WHERE RNE = "'.$_SESSION['RNE'].'"') ;
	$NomEtab = $_SESSION['NomEtab'];

setlocale(LC_TIME, 'fra_fra');
date_default_timezone_set('Europe/Paris');
?>
	<style>
img.img_gauche
{
	width: 25% ;
	height:80%;
	float: left ;
}
		
img.img_droite
{
	width:20% ;
	height:90%;
	float: right ;
	margin-top: -15 ;
}
		

h2
{
	color : #02A4E4;
	text-align:center;
	margin-top: 0px;
}

h3
{
	color: #E54986 ;
	text-align:center;			
}
		
h4
{
	color : #4A51A9;
    margin-bottom: 3px;
    margin-top: 3px;
    text-align:center;
}

h5
{
	color : #4A51A9;
	position: absolute;
	bottom : 13px;
	text-align:right; 
}

.top
{
	height: 50px ;
}

.corps
{
		width: 95%;
}
		
table
{
	border: 1px #4A51A9 solid;
	border-collapse : collapse ;
	width: 79%;
	text-align:center;
}

th
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:78%;
	margin-top: -10px;
}

th.mod
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:30%;
}

th.critere
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:65%;
}

th.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:19%;
}

th.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

th.donnee
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:40%;
}

th.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:15%;
}

th.palier_resum
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:10%;
}

th.domaine
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

th.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #00A2E1;
	vertical-align: middle ;
	width:20%;
}

td
{
	border: 1px #4A51A9 solid ;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.donnee
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:30%;
}

td.points
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:19%;
}

td.points_total
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:20%;
}

td.palier
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:15%;
}

td.part
{
	border: 1px #4A51A9 solid ;
	padding: 5px;
	color : #E84983;
	vertical-align: middle ;
	width:5%;
}
	</style>
	<page backtop="100px">
			<page_header> 
				<div class="top">
					<img class="img_gauche" src="img/logo_head.png" alt="Académie de Nantes" />
					<img class="img_droite" src="img/logo.png" alt="Ecole numerique" />
					<h2>Infrastructures - <?php echo $NomEtab ; ?></h2>
				</div>
			</page_header>
			<table  align="center">
				<thead>
					<tr>
						<th class="critere"><h4>Critères</h4></th>
						<th colspan="2" class="donnee"><h4>Données</h4></th>
						<th class="points"><h4>Nombre de points</h4></th>
					</tr>
				</thead>
				<tbody>
		<?php
			while($donnees = $requete1->fetch())
			{
		?>

			<tr><th class="critere">Nombre de salles avec au moins un accès réseau</th><td colspan="2" class="donnee"><?php echo $donnees['proportion_acces_reseau_salles_donnees']; ?></td><td class="points"><?php echo $donnees['proportion_acces_reseau_salles_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Débit d'accès à Internet</th><td colspan="2" class="donnee"><?php echo $donnees['debit_acces_internet_mod']; ?></td><td class="points"><?php echo $donnees['debit_acces_internet_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Débit du réseau interne</th><td colspan="2" class="donnee"><?php echo $donnees['debit_reseau_interne_mod']; ?></td><td class="points"><?php echo $donnees['debit_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Évaluation de la qualité du débit Internet par rapport aux besoins</th><td colspan="2" class="donnee"><?php echo $donnees['evaluation_qualite_debit_internet_mod']; ?></td><td class="points"><?php echo $donnees['evaluation_qualite_debit_internet_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Évaluation de la qualité du débit du réseau interne par rapport aux besoins</th><td colspan="2" class="donnee"><?php echo $donnees['evaluation_qualite_debit_reseau_interne_mod']; ?></td><td class="points"><?php echo $donnees['evaluation_qualite_debit_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th rowspan="4" class="critere">Réseau pédagogique</th><th class="part">Pare-feu</th><td class="part"><?php echo $donnees['reseau_pedagogique_pare_feu']; ?></td><td rowspan="4" class="points"><?php echo $donnees['reseau_pedagogique_points']; ?> sur 20</td></tr>
			<tr><th class="part">Antivirus</th><td class="part"><?php echo $donnees['reseau_pedagogique_antivirus']; ?></td></tr>
			<tr><th class="part">Filtrage</th><td class="part"><?php echo $donnees['reseau_pedagogique_filtrage']; ?></td></tr>
			<tr><th class="part">Contrôle a posteriori des accès</th><td class="part"><?php echo $donnees['reseau_pedagogique_controle']; ?></td></tr>

			<tr><th class="critere">Proportion des espaces (hors salle de classe) couverts par WiFi</th><td colspan="2" class="donnee"><?php echo $donnees['proportion_wifi_mod']; ?></td><td class="points"><?php echo $donnees['proportion_wifi_points']; ?> sur 10</td></tr>

			<tr><th class="critere">Nombre de salles d'enseignement couvertes par un réseau WiFi</th><td colspan="2" class="donnee"><?php echo $donnees['proportion_wifi_salles_enseignements_donnees']; ?></td><td class="points"><?php echo $donnees['proportion_wifi_salles_enseignements_points']; ?> sur 20</td></tr>

			<tr><th class="critere">Évaluation de la couverture du réseau WiFi par rapport aux besoins</th><td colspan="2" class="donnee"><?php echo $donnees['evaluation_reseau_wifi_mod']; ?></td><td class="points"><?php echo $donnees['evaluation_reseau_wifi_points']; ?> sur 10</td></tr>

			<tr><th class="critere">Possibilité pour les utilisateurs de recharger les équipements mobiles</th><td colspan="2" class="donnee"><?php echo $donnees['possibilite_recharger_equipements_mobiles_mod']; ?></td><td class="points"><?php echo $donnees['possibilite_recharger_equipements_mobiles_points']; ?> sur 10</td></tr>

				</tbody>
			</table>
			<br><br>
	<table align="center">
		<thead>
			<tr>
				<th class="points_total"><h4>Nombre de points</h4></th><th class="palier"><h4>Palier</h4></th>
			</tr>
		</thead>
			
		<tbody>
			<tr><td class="points_total"><?php echo $donnees['nb_points_total']; ?> sur 170</td><td class="palier"><?php echo $donnees['palier_infra'] ; ?> sur 10</td></tr>
		</tbody>
	</table>
	<h5>Edité le <?php echo strftime('%A %d %B %Y'); ?> à <?php echo strftime('%H:%M') ; ?></h5>
			<?php		
			}
			
		?>
		
	</page>
<?php
	
	$content = ob_get_clean() ;
	//die($content) ;
	require('html2pdf/html2pdf.class.php');
	try{
		$pdf = new HTML2PDF('L','A4','fr') ;
		$pdf->pdf->SetDisplayMode('fullpage') ;
		$pdf->writeHTML($content) ;
		$pdf->Output('Equipement.pdf') ;
	}catch (HTML2PDF_exception $e){
		die($e) ;
	}
	
?>
