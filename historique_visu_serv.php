<?php
	$titre_page = "Historique_visu_serv" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
?>

<section>
	<div id="top_section" >
		<h1>Comparaison des données</h1>
	</div>
	
	<div id="content">

<?php 

	$date = $_SESSION['date_modif'];

		$hist = $bdd->query('SELECT * FROM historic WHERE RNE = "'.$_SESSION['RNE'].'" AND date_modif = "'.$date.'"');
		$new = $bdd->query('SELECT * FROM services WHERE RNE = "'.$_SESSION['RNE'].'"');

	while($donnees = $new->fetch())
				{


		while($donnees1 = $hist->fetch())
				{
					
?>

<h3><a HREF="Historique_visu">Equipements</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_infra.php">Infrastructures</a>&nbsp;&nbsp;&nbsp;&nbsp;Services&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_pilo">Pilotage</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_form">Formation</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="historique_visu_uti">Utilisations</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="historique_visu_usa">Usages</a></h3>

	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Ancienne modalité</h4></th><th><h4>Nouvelle modalité</h4></th><th><h4>Ancien nombre de points</h4></th><th><h4>Nouveau nombre de points</h4></th></tr>

			<tr><th>ENT - Espace Numérique de Travail - issu d'un projet académique et/ou des collectivités associées</th><td><?php echo $donnees1['ENT_mod']; ?></td><td><?php echo $donnees['ENT_mod']; ?></td><td><?php echo $donnees1['ENT_points']; ?> sur 24</td><td><?php echo $donnees['ENT_points']; ?> sur 24</td></tr>

			<tr><th>Site web de l'établissement</th><td><?php echo $donnees1['site_web_etablissement_mod']; ?></td><td><?php echo $donnees['site_web_etablissement_mod']; ?></td><td><?php echo $donnees1['site_web_etablissement_points']; ?> sur 10</td><td><?php echo $donnees['site_web_etablissement_points']; ?> sur 10</td></tr>

			<tr><th>Notes, absences, emploi du temps</th><td><?php echo $donnees1['notes_abscence_mod']; ?></td><td><?php echo $donnees['notes_abscence_mod']; ?></td><td><?php echo $donnees1['notes_abscence_points']; ?> sur 10</td><td><?php echo $donnees['notes_abscence_points']; ?> sur 10</td></tr>
			
			<tr><th>Appel dématérialisé des élèves au début de l'heure de cours</th><td><?php echo $donnees1['appel_dematerialise_mod']; ?></td><td><?php echo $donnees['appel_dematerialise_mod']; ?></td><td><?php echo $donnees1['appel_dematerialise_points']; ?> sur 15</td><td><?php echo $donnees['appel_dematerialise_points']; ?> sur 15</td></tr>
			
			<tr><th>Messagerie interne</th><td><?php echo $donnees1['messagerie_interne_mod']; ?></td><td><?php echo $donnees['messagerie_interne_mod']; ?></td><td><?php echo $donnees1['messagerie_interne_points']; ?> sur 10</td><td><?php echo $donnees['messagerie_interne_points']; ?> sur 10</td></tr>
			
			<tr><th>"LSUN - Livret Scolaire Unique Numérique<br>LPC - Livret Personnel de Compétences"</th><td><?php echo $donnees1['LSUN_LPC_mod']; ?></td><td><?php echo $donnees['LSUN_LPC_mod']; ?></td><td><?php echo $donnees1['LSUN_LPC_points']; ?> sur 15</td><td><?php echo $donnees['LSUN_LPC_points']; ?> sur 15</td></tr>
			
			<tr><th>Service de réservation de ressources</th><td><?php echo $donnees1['resa_ressources_mod']; ?></td><td><?php echo $donnees['resa_ressources_mod']; ?></td><td><?php echo $donnees1['resa_ressources_points']; ?> sur 5</td><td><?php echo $donnees['resa_ressources_points']; ?> sur 5</td></tr>
			
			<tr><th>Abonnements à des services d'information et de documentation en ligne</th><td><?php echo $donnees1['abonnements_services_information_en_ligne_mod']; ?></td><td><?php echo $donnees['abonnements_services_information_en_ligne_mod']; ?></td><td><?php echo $donnees1['abonnements_services_information_en_ligne_points']; ?> sur 15</td><td><?php echo $donnees['abonnements_services_information_en_ligne_points']; ?> sur 15</td></tr>
			
			<tr><th>Folios</th><td><?php echo $donnees1['folios_mod']; ?></td><td><?php echo $donnees['folios_mod']; ?></td><td><?php echo $donnees1['folios_points']; ?> sur 15</td><td><?php echo $donnees['folios_points']; ?> sur 15</td></tr>
			
			<tr><th>Abonnements à des ressources numériques pédagogiques éditoriales</th><td><?php echo $donnees1['abonnements_ressources_numeriques_pedagogiques_mod']; ?></td><td><?php echo $donnees['abonnements_ressources_numeriques_pedagogiques_mod']; ?></td><td><?php echo $donnees1['abonnements_ressources_numeriques_pedagogiques_points']; ?> sur 15</td><td><?php echo $donnees['abonnements_ressources_numeriques_pedagogiques_points']; ?> sur 15</td></tr>
			
			<tr><th>Proportion de ressources numériques par rapport aux ressources papiers</th><td><?php echo $donnees1['proportion_ressources_numeriques_papiers_mod']; ?></td><td><?php echo $donnees['proportion_ressources_numeriques_papiers_mod']; ?></td><td><?php echo $donnees1['proportion_ressources_numeriques_papiers_points']; ?> sur 20</td><td><?php echo $donnees['proportion_ressources_numeriques_papiers_points']; ?> sur 20</td></tr>
	</table>

	<table>
				<tr><th><h4>ENT</h4></th><th><h4>Ancienne données</h4></th><th><h4>Nouvelle données</h4></th></tr>
				<tr><th>Déploiement à tous les utilisateurs</th><td><?php echo $donnees1['ENT_utilisateur']; ?></td><td><?php echo $donnees['ENT_utilisateur']; ?></td></tr>
				<tr><th>Déploiement de tous les services</th><td><?php echo $donnees1['ENT_services']; ?></td><td><?php echo $donnees['ENT_services']; ?></td></tr>
	</table>

	<table>
			<tr><th><h4>Ancien nombre de points</h4></th><th><h4>Nouveau nombre de points</h4></th><th><h4>Ancien palier</h4></th><th><h4>Nouveau palier</h4></th></tr>
			<tr><td><?php echo $donnees1['nb_points_total']; ?> sur 154</td><td><?php echo $donnees['nb_points_total']; ?> sur 154</td><td><?php echo $donnees1['palier_serv'] ; ?> sur 10</td><td><?php echo $donnees['palier_serv'] ; ?> sur 10</td></tr>
	</table>

			<?php
			}
		}
		?>			
</div>
</section>
<?php
include('pied_de_page.php');
?>