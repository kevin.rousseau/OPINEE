<?php
	$titre_page = "Infrastructures" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');

	$NomEtab = $_SESSION['NomEtab'];
?>
<section>
	<div id="top_section" >
		<h1>Infrastructures<?php if ($_SESSION['NomEtab'] != NULL){echo ' - '.$NomEtab ;} else {} ; ?></h1>
	</div>
	
	<div id="content">

		<?php if ($_SESSION['Rang'] == 2) 
		{
				$infra = $bdd->query('SELECT * FROM infrastructures WHERE RNE = "'.$_SESSION['RNE'].'"');
				$queryRNE = $bdd->query('SELECT RNE FROM infrastructures WHERE RNE = "'.$_SESSION['RNE'].'"'); 
	$count = $queryRNE->rowCount();  
		if($count == 1) 
			{	
			while($donnees = $infra->fetch())
				{
	?><br>
			<input class="btn" type="submit" value="Modifier les données" onclick="self.location.href='modif_infra.php'">&nbsp;&nbsp;<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_infra_ce.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>
	<table>
			<tr><th><h4>Critères</h4></th><th colspan="2"><h4>Données</h4></th><th><h4>Nombre de points</h4></th></tr>

			<tr><th>Nombre de salles avec au moins un accès réseau</th><td colspan="2"><?php echo $donnees['proportion_acces_reseau_salles_donnees']; ?></td><td><?php echo $donnees['proportion_acces_reseau_salles_points']; ?> sur 20</td></tr>

			<tr><th>Débit d'accès à Internet</th><td colspan="2"><?php echo $donnees['debit_acces_internet_mod']; ?></td><td><?php echo $donnees['debit_acces_internet_points']; ?> sur 20</td></tr>

			<tr><th>Débit du réseau interne</th><td colspan="2"><?php echo $donnees['debit_reseau_interne_mod']; ?></td><td><?php echo $donnees['debit_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la qualité du débit Internet par rapport aux besoins</th><td colspan="2"><?php echo $donnees['evaluation_qualite_debit_internet_mod']; ?></td><td><?php echo $donnees['evaluation_qualite_debit_internet_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la qualité du débit du réseau interne par rapport aux besoins</th><td colspan="2"><?php echo $donnees['evaluation_qualite_debit_reseau_interne_mod']; ?></td><td><?php echo $donnees['evaluation_qualite_debit_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th rowspan="4">Réseau pédagogique</th><th>Pare-feu</th><td><?php echo $donnees['reseau_pedagogique_pare_feu']; ?></td><td rowspan="4"><?php echo $donnees['reseau_pedagogique_points']; ?> sur 20</td></tr>
			<tr><th>Antivirus</th><td><?php echo $donnees['reseau_pedagogique_antivirus']; ?></td></tr>
			<tr><th>Filtrage</th><td><?php echo $donnees['reseau_pedagogique_filtrage']; ?></td></tr>
			<tr><th>Contrôle a posteriori des accès</th><td><?php echo $donnees['reseau_pedagogique_controle']; ?></td></tr>

			<tr><th>Proportion des espaces (hors salle de classe) couverts par WiFi</th><td colspan="2"><?php echo $donnees['proportion_wifi_mod']; ?></td><td><?php echo $donnees['proportion_wifi_points']; ?> sur 10</td></tr>

			<tr><th>Nombre de salles d'enseignement couvertes par un réseau WiFi</th><td colspan="2"><?php echo $donnees['proportion_wifi_salles_enseignements_donnees']; ?></td><td><?php echo $donnees['proportion_wifi_salles_enseignements_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la couverture du réseau WiFi par rapport aux besoins</th><td colspan="2"><?php echo $donnees['evaluation_reseau_wifi_mod']; ?></td><td><?php echo $donnees['evaluation_reseau_wifi_points']; ?> sur 10</td></tr>

			<tr><th>Possibilité pour les utilisateurs de recharger les équipements mobiles</th><td colspan="2"><?php echo $donnees['possibilite_recharger_equipements_mobiles_mod']; ?></td><td><?php echo $donnees['possibilite_recharger_equipements_mobiles_points']; ?> sur 10</td></tr>
	</table>

	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 170</td><td><?php echo $donnees['palier_infra'] ; ?> sur 10</td></tr>
	</table>

		<?php
	}
	 }
			 else {
	 	?><h2>Aucune donnée rentrée pour cet établissement !</h2>
	 	<input class="btn" type="submit" value="Insérer les données" onclick="self.location.href='insert_infra.php?RNE=<?php echo $_SESSION['RNE'] ; ?> '">
	 	<?php
	 }
		}

		// Si l'utilisateur connecté n'est pas un chef d'établissement

	else
	{
		if (empty($_GET['RNE']) AND (empty($_POST['RNE'])))
{
?> <h3>Veuillez tout d'abord choisir un établissement sur la page d'accueil</h3> <?php
}
	else
	{
		$RNE2 = $_GET['RNE'];
		$_SESSION['RNE'] = $_GET['RNE'];
		
	$infra = $bdd->query('SELECT * FROM infrastructures WHERE RNE = "'.$RNE.'"');
	$queryRNE = $bdd->query('SELECT RNE FROM infrastructures WHERE RNE = "'.$RNE.'"'); 
	$count = $queryRNE->rowCount();  
	if($count == 1) 
		{
			while($donnees = $infra->fetch())
			{
	?><br>
		<input class="btn" type="submit" value="Modifier les données" onclick="self.location.href='modif_infra.php'">&nbsp;&nbsp;<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_infra.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>
	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Modalité retenue</h4></th><th><h4>Nombre de points</h4></th></tr>

			<tr><th>Proportion de salles d'enseignements avec au moins un accès réseau</th><td><?php echo $donnees['proportion_acces_reseau_salles_mod']; ?></td><td><?php echo $donnees['proportion_acces_reseau_salles_points']; ?> sur 20</td></tr>

			<tr><th>Débit d'accès à Internet</th><td><?php echo $donnees['debit_acces_internet_mod']; ?></td><td><?php echo $donnees['debit_acces_internet_points']; ?> sur 20</td></tr>

			<tr><th>Débit du réseau interne</th><td><?php echo $donnees['debit_reseau_interne_mod']; ?></td><td><?php echo $donnees['debit_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la qualité du débit Internet par rapport aux besoins</th><td><?php echo $donnees['evaluation_qualite_debit_internet_mod']; ?></td><td><?php echo $donnees['evaluation_qualite_debit_internet_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la qualité du débit du réseau interne par rapport aux besoins</th><td><?php echo $donnees['evaluation_qualite_debit_reseau_interne_mod']; ?></td><td><?php echo $donnees['evaluation_qualite_debit_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th>Réseau pédagogique</th><td>Voir tableau suivant</td><td><?php echo $donnees['reseau_pedagogique_points']; ?> sur 20</td></tr>

			<tr><th>Proportion des espaces (hors salle de classe) couverts par WiFi</th><td><?php echo $donnees['proportion_wifi_mod']; ?></td><td><?php echo $donnees['proportion_wifi_points']; ?> sur 10</td></tr>

			<tr><th>Proportion de salles d'enseignement couvertes par un réseau WiFi</th><td><?php echo $donnees['proportion_wifi_salles_enseignements_mod']; ?></td><td><?php echo $donnees['proportion_wifi_salles_enseignements_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la couverture du réseau WiFi par rapport aux besoins</th><td><?php echo $donnees['evaluation_reseau_wifi_mod']; ?></td><td><?php echo $donnees['evaluation_reseau_wifi_points']; ?> sur 10</td></tr>
			
			<tr><th>Possibilité pour les utilisateurs de recharger les équipements mobiles</th><td><?php echo $donnees['possibilite_recharger_equipements_mobiles_mod']; ?></td><td><?php echo $donnees['possibilite_recharger_equipements_mobiles_points']; ?> sur 10</td></tr>
	</table>
	
	<table class="tabledouble">
	<tr>
	<td class="tabledouble">
	<table>
		<tr><th><h4>Réseau pédagogique</h4></th><th><h4>Données</h4></th></tr>
		<tr><th>Pare-feu</th><td><?php echo $donnees['reseau_pedagogique_pare_feu'] ; ?></td></tr>
		<tr><th>Antivirus</th><td><?php echo $donnees['reseau_pedagogique_antivirus'] ; ?></td></tr>
		<tr><th>Filtrage</th><td><?php echo $donnees['reseau_pedagogique_filtrage'] ; ?></td></tr>
		<tr><th>Contrôle a posteriori des accès</th><td><?php echo $donnees['reseau_pedagogique_controle'] ; ?></td></tr>
	</table>
	</td>
	<td class="tabledouble">
	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 170</td><td><?php echo $donnees['palier_infra'] ; ?> sur 10</td></tr>
	</table>
	</td>
	</tr>
	</table>
		<?php
			}
		}
else
{
	?><h2>Aucune notation effectuée sur cet établissement</h2><br>
	<?php
}
}
	}
?>
 	</div>
</section>
<?php
	include('pied_de_page.php');
	?>
