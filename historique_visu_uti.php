<?php
	$titre_page = "Historique_visu_uti" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
?>

<section>
	<div id="top_section" >
		<h1>Comparaison des données</h1>
	</div>
	
	<div id="content">

<?php 

	$date = $_SESSION['date_modif'];

		$hist = $bdd->query('SELECT * FROM historic WHERE RNE = "'.$_SESSION['RNE'].'" AND date_modif = "'.$date.'"');
		$new = $bdd->query('SELECT * FROM utilisations WHERE RNE = "'.$_SESSION['RNE'].'"');

	while($donnees = $new->fetch())
				{


		while($donnees1 = $hist->fetch())
				{
					
?>

<h3><a HREF="Historique_visu">Equipements</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_infra.php">Infrastructures</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_serv">Services</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_pilo">Pilotage</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_form">Formation</a>&nbsp;&nbsp;&nbsp;&nbsp;Utilisations&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="historique_visu_usa">Usages</a></h3>

	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Ancienne modalité</h4></th><th><h4>Nouvelle modalité</h4></th><th><h4>Ancien nombre de points</h4></th><th><h4>Nouveau nombre de points</h4></th></tr>

			<tr><th>Taux de réservation des salles informatiques</th><td><?php echo $donnees1['taux_reservation_salles_informatiques_mod']; ?></td><td><?php echo $donnees['taux_reservation_salles_informatiques_mod']; ?></td><td><?php echo $donnees1['taux_reservation_salles_informatiques_points']; ?> sur 10</td><td><?php echo $donnees['taux_reservation_salles_informatiques_points']; ?> sur 10</td></tr>

			<tr><th>Taux de réservation des classes mobiles</th><td><?php echo $donnees1['taux_reservation_classes_mobiles_mod']; ?></td><td><?php echo $donnees['taux_reservation_classes_mobiles_mod']; ?></td><td><?php echo $donnees1['taux_reservation_classes_mobiles_points']; ?> sur 20</td><td><?php echo $donnees['taux_reservation_classes_mobiles_points']; ?> sur 20</td></tr>

			<tr><th>Proportion d'enseignants utilisant régulièrement des VPI/TNI/TBI</th><td><?php echo $donnees1['proportion_enseignants_utilisants_VPI_TNI_TBI_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_utilisants_VPI_TNI_TBI_mod']; ?></td><td><?php echo $donnees1['proportion_enseignants_utilisants_VPI_TNI_TBI_points']; ?> sur 20</td><td><?php echo $donnees['proportion_enseignants_utilisants_VPI_TNI_TBI_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de professeurs remplissant le service de notes au moins mensuellement</th><td><?php echo $donnees1['proportion_enseignants_rempli_service_notes_mensuellement_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_rempli_service_notes_mensuellement_mod']; ?></td><td><?php echo $donnees1['proportion_enseignants_rempli_service_notes_mensuellement_points']; ?> sur 20</td><td><?php echo $donnees['proportion_enseignants_rempli_service_notes_mensuellement_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de professeurs remplissant le cahier de textes en ligne à l'issue de chaque cours</th><td><?php echo $donnees1['proportion_enseignants_remplissant_cahier_texte_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_remplissant_cahier_texte_mod']; ?></td><td> <?php echo $donnees1['proportion_enseignants_remplissant_cahier_texte_points']; ?> sur 20</td><td> <?php echo $donnees['proportion_enseignants_remplissant_cahier_texte_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de parents d'élèves consultant les services de vie scolaire au moins une fois par semaine</th><td><?php echo $donnees1['proportion_parent_consultant_services_vie_scolaire_mod']; ?></td><td><?php echo $donnees['proportion_parent_consultant_services_vie_scolaire_mod']; ?></td><td> <?php echo $donnees1['proportion_parent_consultant_services_vie_scolaire_points']; ?> sur 10</td><td> <?php echo $donnees['proportion_parent_consultant_services_vie_scolaire_points']; ?> sur 10</td></tr>

			<tr><th>Proportion d'élèves se connectant chaque jour au réseau interne</th><td><?php echo $donnees1['proportion_eleve_connexion_reseau_interne_mod']; ?></td><td><?php echo $donnees['proportion_eleve_connexion_reseau_interne_mod']; ?></td><td><?php echo $donnees1['proportion_eleve_connexion_reseau_interne_points']; ?> sur 20</td><td><?php echo $donnees['proportion_eleve_connexion_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th>Fréquentation du site public de l'établissement</th><td><?php echo $donnees1['frequentation_site_public_etablissements_mod']; ?></td><td><?php echo $donnees['frequentation_site_public_etablissements_mod']; ?></td><td><?php echo $donnees1['frequentation_site_public_etablissements_points']; ?> sur 10</td><td><?php echo $donnees['frequentation_site_public_etablissements_points']; ?> sur 10</td></tr>

			<tr><th>Équipements numériques utilisés régulièrement par des personnes extérieures à l'établissement</th><td><?php echo $donnees1['equipement_numeriques_utilise_regu_personne_exter_mod']; ?></td><td><?php echo $donnees['equipement_numeriques_utilise_regu_personne_exter_mod']; ?></td><td><?php echo $donnees1['equipement_numeriques_utilise_regu_personne_exter_points']; ?> sur 5</td><td><?php echo $donnees['equipement_numeriques_utilise_regu_personne_exter_points']; ?> sur 5</td></tr>

	</table>

	<table>
			<tr><th><h4>Ancien nombre de points</h4></th><th><h4>Nouveau nombre de points</h4></th><th><h4>Ancien palier</h4></th><th><h4>Nouveau palier</h4></th></tr>
			<tr><td><?php echo $donnees1['nb_points_total']; ?> sur 135</td><td><?php echo $donnees['nb_points_total']; ?> sur 135</td><td><?php echo $donnees1['palier_uti'] ; ?> sur 10</td><td><?php echo $donnees['palier_uti'] ; ?> sur 10</td></tr>
	</table>

			<?php
			}
		}
		?>			
</div>
</section>
<?php
include('pied_de_page.php');
?>