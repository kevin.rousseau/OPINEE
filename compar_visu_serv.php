<?php
	$titre_page = "Comparaison_visu_serv" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
?>

<section>
	<div id="top_section" >
		<h1>Comparaison des établissements</h1>
	</div>
	
	<div id="content">

<?php 

		$RNE = $_GET['RNE'];

		$etab = $bdd->query('SELECT * FROM services WHERE RNE = "'.$_SESSION['RNE'].'"');
		$etab_compar = $bdd->query('SELECT * FROM services WHERE RNE = "'.$RNE.'"');
		$etab_nom = $bdd->query('SELECT * FROM etablissements WHERE RNE = "'.$_SESSION['RNE'].'"');
		$etab_compar_nom = $bdd->query('SELECT * FROM etablissements WHERE RNE = "'.$RNE.'"');

	while($donnees = $etab->fetch())
				{


		while($donnees1 = $etab_compar->fetch())
				{
					
?>
	<h3><a HREF="compar_visu.php?RNE=<?php echo $RNE ; ?>">Equipements</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_infra.php?RNE=<?php echo $RNE ; ?>">Infrastructures</a>&nbsp;&nbsp;&nbsp;&nbsp;Services</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_pilo.php?RNE=<?php echo $RNE ; ?>">Pilotage</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_form.php?RNE=<?php echo $RNE ; ?>">Formation</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_uti.php?RNE=<?php echo $RNE ; ?>">Utilisations</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_usa.php?RNE=<?php echo $RNE ; ?>">Usages</a></h3>
	<table>
			<tr><th><h4>Critères</h4></th><th width="18%"><h4><?php while ($donnees2 = $etab_nom -> fetch())
			{
				echo $donnees2['nom']. ' - Modalité';
			?></h4></th><th width="18%"><h4><?php while ($donnees3 = $etab_compar_nom -> fetch())
			{
				echo $donnees3['nom']. ' - Modalité';
			?></h4></th><th width="15%"><h4><?php
				echo $donnees2['nom']. ' - Points';
			?></h4></th><th width="15%"><h4><?php
				echo $donnees3['nom']. ' - Points';
			?></h4></th></tr>

			<tr><th>ENT - Espace Numérique de Travail - issu d'un projet académique et/ou des collectivités associées</th><td><?php echo $donnees['ENT_mod']; ?></td><td><?php echo $donnees1['ENT_mod']; ?></td><td><?php echo $donnees['ENT_points']; ?> sur 24</td><td><?php echo $donnees1['ENT_points']; ?> sur 24</td></tr>

			<tr><th>Site web de l'établissement</th><td><?php echo $donnees['site_web_etablissement_mod']; ?></td><td><?php echo $donnees1['site_web_etablissement_mod']; ?></td><td><?php echo $donnees['site_web_etablissement_points']; ?> sur 10</td><td><?php echo $donnees1['site_web_etablissement_points']; ?> sur 10</td></tr>

			<tr><th>Notes, absences, emploi du temps</th><td><?php echo $donnees['notes_abscence_mod']; ?></td><td><?php echo $donnees1['notes_abscence_mod']; ?></td><td><?php echo $donnees['notes_abscence_points']; ?> sur 10</td><td><?php echo $donnees1['notes_abscence_points']; ?> sur 10</td></tr>
			
			<tr><th>Appel dématérialisé des élèves au début de l'heure de cours</th><td><?php echo $donnees['appel_dematerialise_mod']; ?></td><td><?php echo $donnees1['appel_dematerialise_mod']; ?></td><td><?php echo $donnees['appel_dematerialise_points']; ?> sur 15</td><td><?php echo $donnees1['appel_dematerialise_points']; ?> sur 15</td></tr>
			
			<tr><th>Messagerie interne</th><td><?php echo $donnees['messagerie_interne_mod']; ?></td><td><?php echo $donnees1['messagerie_interne_mod']; ?></td><td><?php echo $donnees['messagerie_interne_points']; ?> sur 10</td><td><?php echo $donnees1['messagerie_interne_points']; ?> sur 10</td></tr>
			
			<tr><th>"LSUN - Livret Scolaire Unique Numérique<br>LPC - Livret Personnel de Compétences"</th><td><?php echo $donnees['LSUN_LPC_mod']; ?></td><td><?php echo $donnees1['LSUN_LPC_mod']; ?></td><td><?php echo $donnees['LSUN_LPC_points']; ?> sur 15</td><td><?php echo $donnees1['LSUN_LPC_points']; ?> sur 15</td></tr>
			
			<tr><th>Service de réservation de ressources</th><td><?php echo $donnees['resa_ressources_mod']; ?></td><td><?php echo $donnees1['resa_ressources_mod']; ?></td><td><?php echo $donnees['resa_ressources_points']; ?> sur 5</td><td><?php echo $donnees1['resa_ressources_points']; ?> sur 5</td></tr>
			
			<tr><th>Abonnements à des services d'information et de documentation en ligne</th><td><?php echo $donnees['abonnements_services_information_en_ligne_mod']; ?></td><td><?php echo $donnees1['abonnements_services_information_en_ligne_mod']; ?></td><td><?php echo $donnees['abonnements_services_information_en_ligne_points']; ?> sur 15</td><td><?php echo $donnees1['abonnements_services_information_en_ligne_points']; ?> sur 15</td></tr>
			
			<tr><th>Folios</th><td><?php echo $donnees['folios_mod']; ?></td><td><?php echo $donnees1['folios_mod']; ?></td><td><?php echo $donnees['folios_points']; ?> sur 15</td><td><?php echo $donnees1['folios_points']; ?> sur 15</td></tr>
			
			<tr><th>Abonnements à des ressources numériques pédagogiques éditoriales</th><td><?php echo $donnees['abonnements_ressources_numeriques_pedagogiques_mod']; ?></td><td><?php echo $donnees1['abonnements_ressources_numeriques_pedagogiques_mod']; ?></td><td><?php echo $donnees['abonnements_ressources_numeriques_pedagogiques_points']; ?> sur 15</td><td><?php echo $donnees1['abonnements_ressources_numeriques_pedagogiques_points']; ?> sur 15</td></tr>
			
			<tr><th>Proportion de ressources numériques par rapport aux ressources papiers</th><td><?php echo $donnees['proportion_ressources_numeriques_papiers_mod']; ?></td><td><?php echo $donnees1['proportion_ressources_numeriques_papiers_mod']; ?></td><td><?php echo $donnees['proportion_ressources_numeriques_papiers_points']; ?> sur 20</td><td><?php echo $donnees1['proportion_ressources_numeriques_papiers_points']; ?> sur 20</td></tr>
	</table>

	<table>
				<tr><th><h4>ENT</h4></th><th><h4><?php
				echo $donnees2['nom']. ' - Données';
			?></h4></th><th><h4><?php
				echo $donnees3['nom']. ' - Données';
			?></h4></th></tr>
				<tr><th>Déploiement à tous les utilisateurs</th><td><?php echo $donnees['ENT_utilisateur']; ?></td><td><?php echo $donnees1['ENT_utilisateur']; ?></td></tr>
				<tr><th>Déploiement de tous les services</th><td><?php echo $donnees['ENT_services']; ?></td><td><?php echo $donnees1['ENT_services']; ?></td></tr>
	</table>

	<table>
			<tr><th><h4><?php
				echo $donnees2['nom']. ' - Points';
			?></h4></th><th><h4><?php
				echo $donnees3['nom']. ' - Points';
			?></h4></th><th><h4><?php
				echo $donnees2['nom']. ' - Palier';
			?></h4></th><th><h4><?php
				echo $donnees3['nom']. ' - Palier';
			?></h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 154</td><td><?php echo $donnees1['nb_points_total']; ?> sur 154</td><td><?php echo $donnees['palier_serv'] ; ?> sur 10</td><td><?php echo $donnees1['palier_serv'] ; ?> sur 10</td></tr>
	</table>

<?php
			}
		}
?> 

			<?php
			}
		}
		?>			
</div>
</section>
<?php
include('pied_de_page.php');
?>