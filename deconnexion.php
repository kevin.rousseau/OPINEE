<?php
	$titre_page = "Déconnexion" ;
	$force_erreur = 1 ;										// Destruction des sessions en cours

	include('header.php');
	include('en_tete.php');

	session_destroy();
	
?>

<section>

	<div id="top_section">
		<h1>Déconnexion</h1>
	</div>
	
	<div id="content">
		
		<div id="msg_correct">
		<p><h2>Vous êtes maintenant déconnecté</h2></p>
		</div>
		
	</div>

</section>
<?php
	header('refresh:3;url=index.php');
?>
