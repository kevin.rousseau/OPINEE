<?php
	$titre_page = "Historique_visu_form" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
?>

<section>
	<div id="top_section" >
		<h1>Comparaison des données</h1>
	</div>
	
	<div id="content">

<?php 

	$date = $_SESSION['date_modif'];

		$hist = $bdd->query('SELECT * FROM historic WHERE RNE = "'.$_SESSION['RNE'].'" AND date_modif = "'.$date.'"');
		$new = $bdd->query('SELECT * FROM formation WHERE RNE = "'.$_SESSION['RNE'].'"');

	while($donnees = $new->fetch())
				{
		while($donnees1 = $hist->fetch())
				{
					
?>

<h3><a HREF="Historique_visu">Equipements</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_infra.php">Infrastructures</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_serv">Services</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_pilo">Pilotage</a>&nbsp;&nbsp;&nbsp;&nbsp;Formation&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="historique_visu_uti">Utilisations</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="historique_visu_usa">Usages</a></h3>

	<table>
			<tr><th width="45%"><h4>Critères</h4></th><th width="18%"><h4>Ancienne modalité</h4></th><th width="18%"><h4>Nouvelle modalité</h4></th><th><h4>Ancien nombre de points</h4></th><th><h4>Nouveau nombre de points</h4></th></tr>

			<tr><th>Formation au numérique et/ou aux aspects juridiques du numérique d'un ou de plusieurs membres de l'équipe de direction (au cours des trois dernières années)</th><td><?php echo $donnees1['formation_numerique_direction_mod']; ?></td><td><?php echo $donnees['formation_numerique_direction_mod']; ?></td><td><?php echo $donnees1['formation_numerique_direction_points']; ?> sur 20</td><td><?php echo $donnees['formation_numerique_direction_points']; ?> sur 20</td></tr>

			<tr><th>Formation au numérique d'un ou plusieurs membres du personnel de vie scolaire (au cours des trois dernières années)</th><td><?php echo $donnees1['formation_numerique_vie_scolaire_mod']; ?></td><td><?php echo $donnees['formation_numerique_vie_scolaire_mod']; ?></td><td><?php echo $donnees1['formation_numerique_vie_scolaire_points']; ?> sur 20</td><td><?php echo $donnees['formation_numerique_vie_scolaire_points']; ?> sur 20</td></tr>

			<tr><th>Formation au numérique d'un ou de plusieurs membres du personnel administratif et/ou technique (au cours des trois dernières années)</th><td><?php echo $donnees1['formation_numerique_administration_personnel_technique_mod']; ?></td><td><?php echo $donnees['formation_numerique_administration_personnel_technique_mod']; ?></td><td><?php echo $donnees1['formation_numerique_administration_personnel_technique_points']; ?> sur 20</td><td><?php echo $donnees['formation_numerique_administration_personnel_technique_points']; ?> sur 20</td></tr>

			<tr><th>Formation de l'équipe enseignante aux usages pédagogiques du numérique sur site et par un formateur académique (au cours des trois dernières années)</th><td><?php echo $donnees1['formation_numerique_enseignants_mod']; ?></td><td><?php echo $donnees['formation_numerique_enseignants_mod']; ?></td><td><?php echo $donnees1['formation_numerique_enseignants_points']; ?> sur 15</td><td><?php echo $donnees['formation_numerique_enseignants_points']; ?> sur 15</td></tr>

			<tr><th>Animations ponctuelles autour des usages du numérique par un référent numérique (personne ressource) dans l'établissement  (au cours des trois dernières années) :</th><td><?php echo $donnees1['animations_ponctuelles_numeriques_mod']; ?></td><td><?php echo $donnees['animations_ponctuelles_numeriques_mod']; ?></td><td><?php echo $donnees1['animations_ponctuelles_numeriques_points']; ?> sur 15</td><td><?php echo $donnees['animations_ponctuelles_numeriques_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de professeurs ayant suivi une formation (PAF, M@gistère) aux usages pédagogiques du numérique (au cours des trois dernières années)</th><td><?php echo $donnees1['proportion_enseignants_suivi_formation_numerique_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_suivi_formation_numerique_mod']; ?></td><td><?php echo $donnees1['proportion_enseignants_suivi_formation_numerique_points']; ?> sur 15</td><td><?php echo $donnees['proportion_enseignants_suivi_formation_numerique_points']; ?> sur 15</td></tr>

			<tr><th>Actions d'information et de sensibilisation aux usages responsables du numérique et à l'Éducation aux Médias et à l'Information (EMI) à destination des enseignants (au cours des trois dernières années)</th><td><?php echo $donnees1['actions_informations_sensibilisations_EMI_enseignants_mod']; ?></td><td><?php echo $donnees['actions_informations_sensibilisations_EMI_enseignants_mod']; ?></td><td><?php echo $donnees1['actions_informations_sensibilisations_EMI_enseignants_points']; ?> sur 10</td><td><?php echo $donnees['actions_informations_sensibilisations_EMI_enseignants_points']; ?> sur 10</td></tr>

			<tr><th>Proportion d'enseignants mutualisant leurs pratiques du numérique et/ou collaborant via un ou des réseau(x) numériques)</th><td><?php echo $donnees1['proportion_enseignants_mutualisant_pratique_numerique_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_mutualisant_pratique_numerique_mod']; ?></td><td><?php echo $donnees1['proportion_enseignants_mutualisant_pratique_numerique_points']; ?> sur 15</td><td><?php echo $donnees['proportion_enseignants_mutualisant_pratique_numerique_points']; ?> sur 15</td></tr>

			<tr><th>Identification dans l'établissement d'une personne chargée de la veille et de la curation du numérique à destination des enseignants</th><td><?php echo $donnees1['identification_personne_veille_numerique_pour_enseignants_mod']; ?></td><td><?php echo $donnees['identification_personne_veille_numerique_pour_enseignants_mod']; ?></td><td> <?php echo $donnees1['identification_personne_veille_numerique_pour_enseignants_points']; ?> sur 15</td><td> <?php echo $donnees['identification_personne_veille_numerique_pour_enseignants_points']; ?> sur 15</td></tr>

			<tr><th>Formation des parents aux usages des services numériques</th><td><?php echo $donnees1['formation_parent_usages_numeriques_mod']; ?></td><td><?php echo $donnees['formation_parent_usages_numeriques_mod']; ?></td><td><?php echo $donnees1['formation_parent_usages_numeriques_points']; ?> sur 15</td><td><?php echo $donnees['formation_parent_usages_numeriques_points']; ?> sur 15</td></tr>

			<tr><th>Formation des parents aux usages responsables des services numériques</th><td><?php echo $donnees1['formation_parents_usages_responsables_numeriques_mod']; ?></td><td><?php echo $donnees['formation_parents_usages_responsables_numeriques_mod']; ?></td><td><?php echo $donnees1['formation_parents_usages_responsables_numeriques_points']; ?> sur 15</td><td><?php echo $donnees['formation_parents_usages_responsables_numeriques_points']; ?> sur 15</td></tr>

	</table>

	<table>
			<tr><th><h4>Ancien nombre de points</h4></th><th><h4>Nouveau nombre de points</h4></th><th><h4>Ancien palier</h4></th><th><h4>Nouveau palier</h4></th></tr>
			<tr><td><?php echo $donnees1['nb_points_total']; ?> sur 175</td><td><?php echo $donnees['nb_points_total']; ?> sur 175</td><td><?php echo $donnees1['palier_form'] ; ?> sur 10</td><td><?php echo $donnees['palier_form'] ; ?> sur 10</td></tr>
	</table>


			<?php
			}
		}
		?>			
</div>
</section>
<?php
include('pied_de_page.php');
?>