<?php
	$titre_page = "Infrastructures" ;
	include('header.php');
	include('menu.php');
	include('footer.php');
?>

<section>
	<div id="top_section" >
		<h1>Infrastructures</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	
	<div id="content">
 
<br />
		<?php 
				$infra = $bdd->query('SELECT * FROM infrastructures WHERE RNE = "'.$_SESSION['RNE'].'"');

			while($donnees = $infra->fetch())
				{
	?>
	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Nombre de points</h4></th></tr>

			<tr><th>Proportion de salles d'enseignements avec au moins un accès réseau</th><td><?php echo $donnees['proportion_acces_reseau_salles_points']; ?> sur 20</td></tr>

			<tr><th>Débit d'accès à Internet</th><td><?php echo $donnees['debit_acces_internet_points']; ?> sur 20</td></tr>

			<tr><th>Débit du réseau interne</th><td><?php echo $donnees['debit_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la qualité du débit Internet par rapport aux besoins</th><td><?php echo $donnees['evaluation_qualite_debit_internet_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la qualité du débit du réseau interne par rapport aux besoins</th><td><?php echo $donnees['evaluation_qualite_debit_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th>Réseau pédagogique</th><td><?php echo $donnees['reseau_pedagogique_points']; ?> sur 20</td></tr>

			<tr><th>Proportion des espaces (hors salle de classe) couverts par WiFi</th><td><?php echo $donnees['proportion_wifi_points']; ?> sur 10</td></tr>

			<tr><th>Proportion de salles d'enseignement couvertes par un réseau WiFi</th><td><?php echo $donnees['proportion_wifi_salles_enseignements_points']; ?> sur 20</td></tr>

			<tr><th>Évaluation de la couverture du réseau WiFi par rapport aux besoins</th><td><?php echo $donnees['evaluation_reseau_wifi_points']; ?> sur 10</td></tr>

			<tr><th>Possibilité pour les utilisateurs de recharger les équipements mobiles</th><td><?php echo $donnees['possibilite_recharger_equipements_mobiles_points']; ?> sur 10</td></tr>
	</table>

	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 151</td><td><?php echo $donnees['palier_infra'] ; ?> sur 10</td></tr>
	</table>
		<?php
				}
?>
 	</div>
</section>