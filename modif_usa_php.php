<?php
	$titre_page = "Modif_usa" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');
	
	$RNE = $_POST['RNE'];

	if(!empty($_POST['pourcentage_eleves_validation_partielle_B2i']) 
		and !empty($_POST['proportion_enseignants_impliques_certification_competences']) 
		and !empty($_POST['nb_discipline_validation_competences_B2i']) 
		and !empty($_POST['proportion_enseignants_developpant_usage_numerique']) 
		and !empty($_POST['proportion_manuels_scolaires_numeriques_EPLE']) 
		and !empty($_POST['usages_services_ressources_numeriques']) 
		and !empty($_POST['usages_messagerie_institutionnelle_echanges_professionnels']) 
		and !empty($_POST['usages_messagerie_ENT_enseignants_famille']) 
		and !empty($_POST['usages_espaces_stockages_partage_documents_pedagogiques']) 
		and !empty($_POST['usages_outils_numeriques_entre_enseignants']) 
		and !empty($_POST['usages_outils_numeriques_entre_enseignants_eleves']) 
		and !empty($_POST['usages_reseaux_sociaux_pedagogiques']) 
		and !empty($_POST['usages_services_publication_pour_enseignement']) 
		and !empty($_POST['creation_medias_numeriques'])
		and !empty($_POST['utilisation_outils_ressources_numeriques_developpement_oral'])
		and !empty($_POST['usages_services_numeriques_suivi_competences']) 
		and !empty($_POST['usages_numeriques_personnalisation_parcours_individualisation']) 
		and !empty($_POST['usages_numeriques_liaison_inter_degre']) 
		and !empty($_POST['nb_points_total']) 
		and !empty($_POST['palier_usa']))
	{
	
		$pourcent = $_POST['pourcentage_eleves_validation_partielle_B2i'] ;
		$prop = $_POST['proportion_enseignants_impliques_certification_competences'] ;
		$disc = $_POST['nb_discipline_validation_competences_B2i'] ;
		$prop_ens = $_POST['proportion_enseignants_developpant_usage_numerique'] ;
		$prop_manu = $_POST['proportion_manuels_scolaires_numeriques_EPLE'] ;
		$usa_serv = $_POST['usages_services_ressources_numeriques'] ;
		$usa_mess = $_POST['usages_messagerie_institutionnelle_echanges_professionnels'] ;
		$usa_messagerie = $_POST['usages_messagerie_ENT_enseignants_famille'] ;
		$usa_espa = $_POST['usages_espaces_stockages_partage_documents_pedagogiques'] ;
		$usa_out = $_POST['usages_outils_numeriques_entre_enseignants'] ;
		$usa_outils = $_POST['usages_outils_numeriques_entre_enseignants_eleves'] ;
		$usa_rs = $_POST['usages_reseaux_sociaux_pedagogiques'] ;
		$usa_serv_publi = $_POST['usages_services_publication_pour_enseignement'] ;
		$media = $_POST['creation_medias_numeriques'] ;
		$uti = $_POST['utilisation_outils_ressources_numeriques_developpement_oral'] ;
		$usa_serv_num = $_POST['usages_services_numeriques_suivi_competences'] ;
		$usa_num = $_POST['usages_numeriques_personnalisation_parcours_individualisation'] ;
		$usa_numerique = $_POST['usages_numeriques_liaison_inter_degre'] ;
		$points = $_POST['nb_points_total'] ;
		$palier = $_POST['palier_usa'] ;


		$requete1 = $bdd->prepare('UPDATE usages SET pourcentage_eleves_validation_partielle_B2i = :pour, 
			proportion_enseignants_impliques_certification_competences = :pro, 
			nb_discipline_validation_competences_B2i = :disci, 
			proportion_enseignants_developpant_usage_numerique = :prop_ens_num,
			proportion_manuels_scolaires_numeriques_EPLE = :prop_manuel, 
			usages_services_ressources_numeriques = :usa_service, 
			usages_messagerie_institutionnelle_echanges_professionnels = :usa_messagerie, 
			usages_messagerie_ENT_enseignants_famille = :usa_mess, 
			usages_espaces_stockages_partage_documents_pedagogiques = :usa_espace, 
			usages_outils_numeriques_entre_enseignants = :usa_out,
			usages_outils_numeriques_entre_enseignants_eleves = :usa_outils,
			usages_reseaux_sociaux_pedagogiques = :usa_rs,
			usages_services_publication_pour_enseignement = :usa_serv_publi,
			creation_medias_numeriques = :media,
			utilisation_outils_ressources_numeriques_developpement_oral = :uti,
			usages_services_numeriques_suivi_competences = :usa_serv_num, 
			usages_numeriques_personnalisation_parcours_individualisation = :usa_num, 
			usages_numeriques_liaison_inter_degre = :usa_numerique,  
			nb_points_total = :pts, 
			palier_usa = :pale WHERE RNE = "'.$_POST['RNE'].'"');

		$requete1->execute(array(
			'pour' => $pourcent,
			'pro' => $prop,
			'disci' => $disc,
			'prop_ens_num' => $prop_ens,
			'prop_manuel' => $prop_manu,
			'usa_service' => $usa_serv,
			'usa_messagerie' => $usa_mess,
			'usa_mess' => $usa_messagerie,
			'usa_espace' => $usa_espa,
			'usa_out' => $usa_out,
			'usa_outils' => $usa_outils,
			'usa_rs' => $usa_rs,
			'usa_serv_publi' => $usa_serv_publi,
			'media' => $media,
			'uti' => $uti,
			'usa_serv_num' => $usa_serv_num,
			'usa_num' => $usa_num,
			'usa_numerique' => $usa_numerique,
			'pts' => $points,
			'pale' => $palier
				));	

			header("refresh:0;url=confirm_modif_usa.php?RNE=".$RNE."") ;
	}
	else
	{
		header('refresh:0;url=modif_usa.php') ;
	}
?>
</div>
</section>