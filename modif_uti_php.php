<?php
	$titre_page = "Modif_uti" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');
	
	$RNE = $_POST['RNE'];

	if(!empty($_POST['taux_reservation_salles_informatiques']) and !empty($_POST['taux_reservation_classes_mobiles']) and !empty($_POST['proportion_enseignants_utilisants_VPI_TNI_TBI']) and !empty($_POST['proportion_enseignants_remplissant_services_notes_mensuellement']) and !empty($_POST['proportion_enseignants_remplissant_cahier_texte']) and !empty($_POST['proportion_parent_consultant_services_vie_scolaire']) and !empty($_POST['proportion_eleve_connexion_reseau_interne']) and !empty($_POST['frequentation_site_public_etablissements']) and !empty($_POST['equipement_numeriques_utilises_regulierement_personne_exterieur']) and !empty($_POST['nb_points_total']) and !empty($_POST['palier_uti']))
	{
	
		$tx = $_POST['taux_reservation_salles_informatiques'] ;
		$taux = $_POST['taux_reservation_classes_mobiles'] ;
		$prop = $_POST['proportion_enseignants_utilisants_VPI_TNI_TBI'] ;
		$prop_ens_notes = $_POST['proportion_enseignants_remplissant_services_notes_mensuellement'] ;
		$prop_ens_cahier = $_POST['proportion_enseignants_remplissant_cahier_texte'] ;
		$prop_parent = $_POST['proportion_parent_consultant_services_vie_scolaire'] ;
		$prop_eleve = $_POST['proportion_eleve_connexion_reseau_interne'] ;
		$freq = $_POST['frequentation_site_public_etablissements'] ;
		$equip = $_POST['equipement_numeriques_utilises_regulierement_personne_exterieur'] ;
		$points = $_POST['nb_points_total'] ;
		$palier = $_POST['palier_uti'] ;


		$requete1 = $bdd->prepare('UPDATE utilisations SET taux_reservation_salles_informatiques = :taux, taux_reservation_classes_mobiles = :taux_cla, proportion_enseignants_utilisants_VPI_TNI_TBI = :prop_ens_VPI, proportion_enseignants_remplissant_services_notes_mensuellement = :prop_ens_serv, proportion_enseignants_remplissant_cahier_texte = :prop_ens_cahier, proportion_parent_consultant_services_vie_scolaire = :prop_parent_serv, proportion_eleve_connexion_reseau_interne = :prop_eleve_reseau, frequentation_site_public_etablissements = :freq_site, equipement_numeriques_utilises_regulierement_personne_exterieur = :equipement, nb_points_total = :pts, palier_uti = :pale WHERE RNE = "'.$_POST['RNE'].'"');

		$requete1->execute(array(
			'taux' => $tx,
			'taux_cla' => $taux,
			'prop_ens_VPI' => $prop,
			'prop_ens_serv' => $prop_ens_notes,
			'prop_ens_cahier' => $prop_ens_cahier,
			'prop_parent_serv' => $prop_parent,
			'prop_eleve_reseau' => $prop_eleve,
			'freq_site' => $freq,
			'equipement' => $equip,
			'pts' => $points,
			'pale' => $palier
				));	

			header("refresh:0;url=confirm_modif_uti.php?RNE=".$RNE."") ;
	}
	else
	{
		header('refresh:0;url=modif_uti.php') ;
	}
?>
</div>
</section>