<?php
	$titre_page = "Inscription" ;
	$force_erreur = 1 ;
	include('header.php');
	include('menu.php');

	$error = 0;					// On initialise la variable $error à 0
	$msg_error = "";			// On inscrit une valeur nulle à $msg_error 

if(isset($_POST['autorite']))									//On vérifie si les différents champs existent
{
	if(empty($_POST['autorite']))							//On vérifie si les différents champs sont remplis
		{
			$error = 1;																							//Si ce n'est pas le cas, $error passe à 1
			$msg_error = "Veuilez saisir un statut" ;
		}

		else																									//Si tout les champs sont remplis, on leur affecte les différentes valeurs passées en $_POST
		{																				
			$autorite = $_POST['autorite'] ;
		}
	}
?>

<section>
<div id="top_section" >
		<h1>Inscription</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	<div id="content">
<?php if((!isset($_POST['type'])) OR $error == 1)			//Si les différentes valeurs n'éxistent pas ou que $error est égale à 0
		{
		?>
		<form method="post" action="inscription.php">																<!--On affiche le formulaire afin de choisir le type, le statut et le département choisi -->
 <br>
		<h3>Veuillez choisir un statut</h3>
		<br />

		<h3><input type= "radio" name="type" value="1">Admin&nbsp;&nbsp;&nbsp;<INPUT type= "radio" name="type" value="2">Chef d'établissement&nbsp;&nbsp;&nbsp;<input type= "radio" name="statut" value="1">DAN&nbsp;&nbsp;&nbsp;<INPUT type= "radio" name="statut" value="2">CI</h3>

		<input class="btn" type="submit" value="Continuer" /></form>
<?php 
			if($error == 1)																						//Si $error == 1, on affiche le message d'erreur en dessous du formulaire
			{
		?>

				<div id="msg_error">
					<?php echo '<p>'.$msg_error.'</p>' ; ?>
				</div>
		
		<?php
	}
			}
		else($error == 0 and isset($_POST['type']) and isset($_POST['statut']) and isset($_POST['dpt']))		//Si $error == 0 et que les 3 champs existent, on affiche le menu déroulant afin de choisir un établissement
		{

		?>
<h2>Bienvenue <?php echo $_SESSION['Prenom']. " " .$_SESSION['Nom'] ; ?></h2>

 <h3>Choississez un établissement dans la liste ci-dessous</h3>
		<form method="post" action="charge_etab.php">	
		<select name="RNE">
		<option selected="selected" disabled="disabled">Etablissement</option>
				<?php
				$requete1 = $bdd->query('SELECT DISTINCT RNE, nom FROM etablissements WHERE type = '.$type.' and statut = '.$statut.' and dpt = '.$dpt.'') ;
			while($donnees1 = $requete1->fetch())
			{
				echo '<option value="'.$donnees1['RNE'].'">'.$donnees1['nom'].' - '.$donnees1['RNE'].'</option>' ;
			}
		?>
</select>&nbsp;&nbsp;<input class="btn" type="submit" value="Afficher" /></form>

<form method="post" action="menu.php"><input type="hidden" name="NomEtab" value="<?php echo $donnees1['nom'] ; ?>" /><input type="hidden" name="RNE" value="<?php echo $donnees1['RNE'] ; ?>" /></form>
<?php
}
?>
</div>
</section>