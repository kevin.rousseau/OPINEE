<?php
	$titre_page = "Historique_visu_pilo" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
?>

<section>
	<div id="top_section" >
		<h1>Comparaison des données</h1>
	</div>
	
	<div id="content">

<?php 

	$date = $_SESSION['date_modif'];

		$hist = $bdd->query('SELECT * FROM historic WHERE RNE = "'.$_SESSION['RNE'].'" AND date_modif = "'.$date.'"');
		$new = $bdd->query('SELECT * FROM pilotage WHERE RNE = "'.$_SESSION['RNE'].'"');

	while($donnees = $new->fetch())
				{


		while($donnees1 = $hist->fetch())
				{
					
?>

<h3><a HREF="Historique_visu">Equipements</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_infra.php">Infrastructures</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_serv">Services</a>&nbsp;&nbsp;&nbsp;&nbsp;Pilotage&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="Historique_visu_form">Formation</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="historique_visu_uti">Utilisations</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="historique_visu_usa">Usages</a></h3>

	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Ancienne modalité</h4></th><th><h4>Nouvelle modalité</h4></th><th><h4>Ancien nombre de points</h4></th><th><h4>Nouveau nombre de points</h4></th></tr>

			<tr><th>Volet numérique au sein du projet d'établissement</th><td><?php echo $donnees1['volet_numerique_projet_mod']; ?></td><td><?php echo $donnees['volet_numerique_projet_mod']; ?></td><td><?php echo $donnees1['volet_numerique_projet_points']; ?> sur 10</td><td><?php echo $donnees['volet_numerique_projet_points']; ?> sur 10</td></tr>
			
			<tr><th>Charte d'usage du numérique</th><td><?php echo $donnees1['charte_usage_numerique_mod']; ?></td><td><?php echo $donnees['charte_usage_numerique_mod']; ?></td><td><?php echo $donnees1['charte_usage_numerique_points']; ?> sur 20</td><td><?php echo $donnees['charte_usage_numerique_points']; ?> sur 20</td></tr>
			
			<tr><th>Commissions numérique ou conseil pédagogique traitant des sujets numériques</th><td><?php echo $donnees1['commission_numerique_mod']; ?></td><td><?php echo $donnees['commission_numerique_mod']; ?></td><td><?php echo $donnees1['commission_numerique_points']; ?> sur 20</td><td><?php echo $donnees['commission_numerique_points']; ?> sur 20</td></tr>
			
			<tr><th>Personnel(s) référent(s) numérique(s) pour les usages pédagogiques du numérique</th><td><?php echo $donnees1['personnels_referents_numeriques_usages_pedagogiques_mod']; ?></td><td><?php echo $donnees['personnels_referents_numeriques_usages_pedagogiques_mod']; ?></td><td><?php echo $donnees1['personnels_referents_numeriques_usages_pedagogiques_points']; ?> sur 15</td><td><?php echo $donnees['personnels_referents_numeriques_usages_pedagogiques_points']; ?> sur 15</td></tr>
			
			<tr><th>Utilisation d'Indemnités de Mission Particulière (IMP) pour favoriser les usages du numérique</th><td><?php echo $donnees1['utilisation_IMP_usages_numeriques_mod']; ?></td><td><?php echo $donnees['utilisation_IMP_usages_numeriques_mod']; ?></td><td><?php echo $donnees1['utilisation_IMP_usages_numeriques_points']; ?> sur 10</td><td><?php echo $donnees['utilisation_IMP_usages_numeriques_points']; ?> sur 10</td></tr>
			
			<tr><th>Démarche "zéro papier inutile"</th><td><?php echo $donnees1['demarche_zero_papier_inutile_mod']; ?></td><td><?php echo $donnees['demarche_zero_papier_inutile_mod']; ?></td><td><?php echo $donnees1['demarche_zero_papier_inutile_points']; ?> sur 10</td><td><?php echo $donnees['demarche_zero_papier_inutile_points']; ?> sur 10</td></tr>
			
			<tr><th>Usage privilégié du numérique par l'équipe de direction et l'équipe administrative</th><td><?php echo $donnees1['usage_privilegie_numerique_direction_administration_mod']; ?></td><td><?php echo $donnees['usage_privilegie_numerique_direction_administration_mod']; ?></td><td><?php echo $donnees1['usage_privilegie_numerique_direction_administration_points']; ?> sur 10</td><td><?php echo $donnees['usage_privilegie_numerique_direction_administration_points']; ?> sur 10</td></tr>
			
			<tr><th>Équipements numériques personnels des professeurs acceptés sur le réseau de l'établissement</th><td><?php echo $donnees1['equipements_numeriques_personnels_enseignants_acceptes_mod']; ?></td><td><?php echo $donnees['equipements_numeriques_personnels_enseignants_acceptes_mod']; ?></td><td><?php echo $donnees1['equipements_numeriques_personnels_enseignants_acceptes_points']; ?> sur 20</td><td><?php echo $donnees['equipements_numeriques_personnels_enseignants_acceptes_points']; ?> sur 20</td></tr>
			
			<tr><th>Équipements numériques personnels des élèves acceptés (pour des usages pédagogiques) sur le réseau de l'établissement</th><td><?php echo $donnees1['equipements_numeriques_personnels_eleves_acceptes_mod']; ?></td><td><?php echo $donnees['equipements_numeriques_personnels_eleves_acceptes_mod']; ?></td><td><?php echo $donnees1['equipements_numeriques_personnels_eleves_acceptes_points']; ?> sur 5</td><td><?php echo $donnees['equipements_numeriques_personnels_eleves_acceptes_points']; ?> sur 5</td></tr>
			
			<tr><th>Sécurité - Identifiants personnels d'accès au réseau</th><td><?php echo $donnees1['securite_id_personnels_acces_reseau_mod']; ?></td><td><?php echo $donnees['securite_id_personnels_acces_reseau_mod']; ?></td><td><?php echo $donnees1['securite_id_personnels_acces_reseau_points']; ?> sur 20</td><td><?php echo $donnees['securite_id_personnels_acces_reseau_points']; ?> sur 20</td></tr>

			<tr><th>Actions d'information et d'accompagnement des usagers au numérique</th><td colspan="2">Voir tableau suivant</td><td><?php echo $donnees1['actions_informations_accompagnement_usages_numeriques_points']; ?> sur 18</td><td><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_points']; ?> sur 18</td></tr>
		
			<tr><th>Assistance aux usagers</th><td colspan="2">Voir tableau suivant</td><td><?php echo $donnees1['assistance_usagers_points']; ?> sur 18</td><td><?php echo $donnees['assistance_usagers_points']; ?> sur 18</td></tr>
			
			<tr><th>Suivi statistique des taux d'utilisation des équipements et des services</th><td><?php echo $donnees1['suivi_statistique_taux_utilisation_equipements_services_mod']; ?></td><td><?php echo $donnees['suivi_statistique_taux_utilisation_equipements_services_mod']; ?></td><td><?php echo $donnees1['suivi_statistique_taux_utilisation_equipements_services_points']; ?> sur 10</td><td><?php echo $donnees['suivi_statistique_taux_utilisation_equipements_services_points']; ?> sur 10</td></tr>
			
			<tr><th>Mise en place et suivi des "Services en Ligne"</th><td><?php echo $donnees1['mise_en_place_suivi_services_en_ligne_mod']; ?></td><td><?php echo $donnees['mise_en_place_suivi_services_en_ligne_mod']; ?></td><td><?php echo $donnees1['mise_en_place_suivi_services_en_ligne_points']; ?> sur 20</td><td><?php echo $donnees['mise_en_place_suivi_services_en_ligne_points']; ?> sur 20</td></tr>
			
			<tr><th>Échanges entre le chef d'établissement et les corps d'inspection pour le développement des usages du numérique</th><td><?php echo $donnees1['echanges_chef_etablissement_inspection_developpement_mod']; ?></td><td><?php echo $donnees['echanges_chef_etablissement_inspection_developpement_mod']; ?></td><td><?php echo $donnees1['echanges_chef_etablissement_inspection_developpement_points']; ?> sur 15</td><td><?php echo $donnees['echanges_chef_etablissement_inspection_developpement_points']; ?> sur 15</td></tr>
	</table>
			
	<table>
			<tr><th><h4>Actions d'information et d'accompagnement des usagers au numérique</h4></th><th><h4>Anciennes données</h4></th><th><h4>Nouvelles données</h4></th></tr>
			<tr><th>Avec les moyens de l'établissement (personnel référent)</th><td><?php echo $donnees1['actions_informations_accompagnement_usages_numeriques_etab']; ?></td><td><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_etab']; ?></td></tr>
			<tr><th>Par les services académiques</th><td><?php echo $donnees1['actions_informations_accompagnement_usages_numeriques_aca']; ?></td><td><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_aca']; ?></td></tr>
			<tr><th>Par la collectivité</th><td><?php echo $donnees1['actions_informations_accompagnement_usages_numeriques_collec']; ?></td><td><?php echo $donnees['actions_informations_accompagnement_usages_numeriques_collec']; ?></td></tr>
	</table>

	<table>
			<tr><th><h4>Assistance aux usagers</h4></th><th><h4>Anciennes données</h4></th><th><h4>Nouvelles données</h4></th></tr>
			<tr><th>Avec les moyens de l'établissement (personnel référent)</th><td><?php echo $donnees1['assistance_usagers_etab']; ?></td><td><?php echo $donnees['assistance_usagers_etab']; ?></td></tr>
			<tr><th>Par les services académiques</th><td><?php echo $donnees1['assistance_usagers_aca']; ?></td><td><?php echo $donnees['assistance_usagers_aca']; ?></td></tr>
			<tr><th>Par la collectivité</th><td><?php echo $donnees1['assistance_usagers_collec']; ?></td><td><?php echo $donnees['assistance_usagers_collec']; ?></td></tr>
	</table>

	<table>
			<tr><th><h4>Ancien nombre de points</h4></th><th><h4>Nouveau nombre de points</h4></th><th><h4>Ancien palier</h4></th><th><h4>Nouveau palier</h4></th></tr>
			<tr><td><?php echo $donnees1['nb_points_total']; ?> sur 221</td><td><?php echo $donnees['nb_points_total']; ?> sur 221</td><td><?php echo $donnees1['palier_pilo'] ; ?> sur 10</td><td><?php echo $donnees['palier_pilo'] ; ?> sur 10</td></tr>
	</table>

			<?php
			}
		}
		?>			
</div>
</section>
<?php
include('pied_de_page.php');
?>