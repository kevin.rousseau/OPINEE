<?php
	$titre_page = "Compar_etab_usa" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
?>

<section>
	<div id="top_section" >
		<h1>Comparaison des établissements</h1>
	</div>
	
	<div id="content">

<?php 

		$RNE = $_GET['RNE'];

		$etab = $bdd->query('SELECT * FROM usages WHERE RNE = "'.$_SESSION['RNE'].'"');
		$etab_compar = $bdd->query('SELECT * FROM usages WHERE RNE = "'.$RNE.'"');
		$etab_nom = $bdd->query('SELECT * FROM etablissements WHERE RNE = "'.$_SESSION['RNE'].'"');
		$etab_compar_nom = $bdd->query('SELECT * FROM etablissements WHERE RNE = "'.$RNE.'"');

	while($donnees = $etab->fetch())
				{

		while($donnees1 = $etab_compar->fetch())
				{
					
?>
<h3><a HREF="compar_visu.php?RNE=<?php echo $RNE ; ?>">Equipements</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_infra.php?RNE=<?php echo $RNE ; ?>">Infrastructures</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_serv.php?RNE=<?php echo $RNE ; ?>">Services</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_pilo.php?RNE=<?php echo $RNE ; ?>">Pilotage</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_form.php?RNE=<?php echo $RNE ; ?>">Formation</a>&nbsp;&nbsp;&nbsp;&nbsp;<a HREF="compar_visu_uti.php?RNE=<?php echo $RNE ; ?>">Utilisations</a>&nbsp;&nbsp;&nbsp;&nbsp;Usages</h3>
	<table>
			<tr><th><h4>Critères</h4></th><th width="18%"><h4><?php while ($donnees2 = $etab_nom -> fetch())
			{
				echo $donnees2['nom']. ' - Modalité';
			?></h4></th><th width="18%"><h4><?php while ($donnees3 = $etab_compar_nom -> fetch())
			{
				echo $donnees3['nom']. ' - Modalité';
			?></h4></th><th width="15%"><h4><?php
				echo $donnees2['nom']. ' - Points';
			?></h4></th><th width="15%"><h4><?php
				echo $donnees3['nom']. ' - Points';
			?></h4></th></tr>

			<tr><th>Pourcentage d'élèves hors classes de 3ème ayant une validation partielle du B2i Collège</th><td><?php echo $donnees['pourcentage_eleves_validation_partielle_B2i_mod']; ?></td><td><?php echo $donnees1['pourcentage_eleves_validation_partielle_B2i_mod']; ?></td><td><?php echo $donnees['pourcentage_eleves_validation_partielle_B2i_points']; ?> sur 5</td><td><?php echo $donnees1['pourcentage_eleves_validation_partielle_B2i_points']; ?> sur 5</td></tr>

			<tr><th>Proportion de professeurs impliqués dans la certification des compétences numériques des élèves (B2i)</th><td><?php echo $donnees['proportion_enseignants_impliques_certification_competence_mod']; ?></td><td><?php echo $donnees1['proportion_enseignants_impliques_certification_competence_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_impliques_certification_competence_points']; ?> sur 5</td><td><?php echo $donnees1['proportion_enseignants_impliques_certification_competence_points']; ?> sur 5</td></tr>

			<tr><th>Nombre de disciplines impliquées dans la validation des compétences du B2i</th><td><?php echo $donnees['nb_discipline_validation_competences_B2i_mod']; ?></td><td><?php echo $donnees1['nb_discipline_validation_competences_B2i_mod']; ?></td><td> <?php echo $donnees['nb_discipline_validation_competences_B2i_points']; ?> sur 10</td><td> <?php echo $donnees1['nb_discipline_validation_competences_B2i_points']; ?> sur 10</td></tr>

			<tr><th>Proportion de professeurs développant des usages pédagogiques du numérique</th><td><?php echo $donnees['proportion_enseignants_developpant_usage_numerique_mod']; ?></td><td><?php echo $donnees1['proportion_enseignants_developpant_usage_numerique_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_developpant_usage_numerique_points']; ?> sur 20</td><td><?php echo $donnees1['proportion_enseignants_developpant_usage_numerique_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de manuels scolaires numériques dans l'EPLE</th><td><?php echo $donnees['proportion_manuels_scolaires_numeriques_EPLE_mod']; ?></td><td><?php echo $donnees1['proportion_manuels_scolaires_numeriques_EPLE_mod']; ?></td><td> <?php echo $donnees['proportion_manuels_scolaires_numeriques_EPLE_points']; ?> sur 15</td><td> <?php echo $donnees1['proportion_manuels_scolaires_numeriques_EPLE_points']; ?> sur 15</td></tr>

			<tr><th>Usages de services et ressources pédagogiques numériques institutionnels (D'Col, EFS, éduthèque, Fondamentaux, etc.)</th><td><?php echo $donnees['usages_services_ressources_numeriques_mod']; ?></td><td><?php echo $donnees1['usages_services_ressources_numeriques_mod']; ?></td><td> <?php echo $donnees['usages_services_ressources_numeriques_points']; ?> sur 15</td><td> <?php echo $donnees1['usages_services_ressources_numeriques_points']; ?> sur 15</td></tr>

			<tr><th>Usages d'une messagerie institutionnelle (académique ou dans l'ENT) pour les échanges professionnels</th><td><?php echo $donnees['usages_messagerie_institutionnelle_echanges_professionnel_mod']; ?></td><td><?php echo $donnees1['usages_messagerie_institutionnelle_echanges_professionnel_mod']; ?></td><td><?php echo $donnees['usages_messagerie_institutionnelle_echanges_professionnel_points']; ?> sur 20</td><td><?php echo $donnees1['usages_messagerie_institutionnelle_echanges_professionnel_points']; ?> sur 20</td></tr>

			<tr><th>Usages de la messagerie de l'ENT pour les échanges des professeurs avec les familles (si ENT)</th><td><?php echo $donnees['usages_messagerie_ENT_enseignants_famille_mod']; ?></td><td><?php echo $donnees1['usages_messagerie_ENT_enseignants_famille_mod']; ?></td><td><?php echo $donnees['usages_messagerie_ENT_enseignants_famille_points']; ?> sur 15</td><td><?php echo $donnees1['usages_messagerie_ENT_enseignants_famille_points']; ?> sur 15</td></tr>

			<tr><th>Usages d'espaces de stockage ou de partage de documents pédagogiques</th><td><?php echo $donnees['usages_espaces_stockages_partage_documents_pedagogiques_mod']; ?></td><td><?php echo $donnees1['usages_espaces_stockages_partage_documents_pedagogiques_mod']; ?></td><td><?php echo $donnees['usages_espaces_stockages_partage_documents_pedagogiques_points']; ?> sur 10</td><td><?php echo $donnees1['usages_espaces_stockages_partage_documents_pedagogiques_points']; ?> sur 10</td></tr>

			<tr><th>Usages d'outils numériques pour des pratiques collaboratives entre ensiegnants</th><td><?php echo $donnees['usages_outils_numeriques_entre_enseignants_mod']; ?></td><td><?php echo $donnees1['usages_outils_numeriques_entre_enseignants_mod']; ?></td><td> <?php echo $donnees['usages_outils_numeriques_entre_enseignants_points']; ?> sur 15</td><td> <?php echo $donnees1['usages_outils_numeriques_entre_enseignants_points']; ?> sur 15</td></tr>

			<tr><th>Usages d'outils numériques pour des pratiques collaboratives des enseignants avec leurs élèves</th><td><?php echo $donnees['usages_outils_numeriques_entre_enseignants_eleves_mod']; ?></td><td><?php echo $donnees1['usages_outils_numeriques_entre_enseignants_eleves_mod']; ?></td><td><?php echo $donnees['usages_outils_numeriques_entre_enseignants_eleves_points']; ?> sur 20</td><td><?php echo $donnees1['usages_outils_numeriques_entre_enseignants_eleves_points']; ?> sur 20</td></tr>

			<tr><th>Usages connus de réseaux sociaux dans le cadre pédagogique</th><td><?php echo $donnees['usages_reseaux_sociaux_pedagogiques_mod']; ?></td><td><?php echo $donnees1['usages_reseaux_sociaux_pedagogiques_mod']; ?></td><td><?php echo $donnees['usages_reseaux_sociaux_pedagogiques_points']; ?> sur 15</td><td><?php echo $donnees1['usages_reseaux_sociaux_pedagogiques_points']; ?> sur 15</td></tr>

			<tr><th>Usages connus de services de publication de type blog pour l'enseignement</th><td><?php echo $donnees['usages_services_publication_pour_enseignement_mod']; ?></td><td><?php echo $donnees1['usages_services_publication_pour_enseignement_mod']; ?></td><td><?php echo $donnees['usages_services_publication_pour_enseignement_points']; ?> sur 15</td><td><?php echo $donnees1['usages_services_publication_pour_enseignement_points']; ?> sur 15</td></tr>

			<tr><th>Création de médias numériques (journal, radio, vidéo, exposition) - connue et validée par le chef d'établissement</th><td><?php echo $donnees['creation_medias_numeriques_mod']; ?></td><td><?php echo $donnees1['creation_medias_numeriques_mod']; ?></td><td><?php echo $donnees['creation_medias_numeriques_points']; ?> sur 20</td><td><?php echo $donnees1['creation_medias_numeriques_points']; ?> sur 20</td></tr>

			<tr><th>Utilisation d'outils et de ressources numériques pour le développement de la pratique de l'oral (baladodiffusion, labo mutimédia, etc.)</th><td><?php echo $donnees['utilisation_outils_ressources_numeriques_dvlp_oral_mod']; ?></td><td><?php echo $donnees1['utilisation_outils_ressources_numeriques_dvlp_oral_mod']; ?></td><td><?php echo $donnees['utilisation_outils_ressources_numeriques_dvlp_oral_points']; ?> sur 20</td><td><?php echo $donnees1['utilisation_outils_ressources_numeriques_dvlp_oral_points']; ?> sur 20</td></tr>

			<tr><th>Usages de services numériques de suivi de la maîtrise des compétences (ex : LPC, LSUN, etc.)</th><td><?php echo $donnees['usages_services_numeriques_suivi_competences_mod']; ?></td><td><?php echo $donnees1['usages_services_numeriques_suivi_competences_mod']; ?></td><td><?php echo $donnees['usages_services_numeriques_suivi_competences_points']; ?> sur 20</td><td><?php echo $donnees1['usages_services_numeriques_suivi_competences_points']; ?> sur 20</td></tr>

			<tr><th>Usages du numérique à des fins de personnalisation des parcours et d'individualisation des enseignements</th><td><?php echo $donnees['usages_numeriques_personnalisation_parcours_individu_mod']; ?></td><td><?php echo $donnees1['usages_numeriques_personnalisation_parcours_individu_mod']; ?></td><td><?php echo $donnees['usages_numeriques_personnalisation_parcours_individu_points']; ?> sur 20</td><td><?php echo $donnees1['usages_numeriques_personnalisation_parcours_individu_points']; ?> sur 20</td></tr>

			<tr><th>Usages du numérique dans le cadre de la liaison inter-degré</th><td><?php echo $donnees['usages_numeriques_liaison_inter_degre_mod']; ?></td><td><?php echo $donnees1['usages_numeriques_liaison_inter_degre_mod']; ?></td><td><?php echo $donnees['usages_numeriques_liaison_inter_degre_points']; ?> sur 20</td><td><?php echo $donnees1['usages_numeriques_liaison_inter_degre_points']; ?> sur 20</td></tr>

	</table>

	<table>
			<tr><th><h4><?php
				echo $donnees2['nom']. ' - Points';
			?></h4></th><th><h4><?php
				echo $donnees3['nom']. ' - Points';
			?></h4></th><th><h4><?php
				echo $donnees2['nom']. ' - Palier';
			?></h4></th><th><h4><?php
				echo $donnees3['nom']. ' - Palier';
			?></h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 280</td><td><?php echo $donnees1['nb_points_total']; ?> sur 280</td><td><?php echo $donnees['palier_usa'] ; ?> sur 10</td><td><?php echo $donnees1['palier_usa'] ; ?> sur 10</td></tr>
	</table>

<?php
			}
		}
?>
			
			<?php
			}
		}
		?>			
</div>
</section>
<?php
include('pied_de_page.php');
?>