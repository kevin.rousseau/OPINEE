<?php
	$titre_page = "Utilisations" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');

	$NomEtab = $_SESSION['NomEtab'];
?>

<section>
	<div id="top_section" >
		<h1>Utilisations<?php if ($_SESSION['NomEtab'] != NULL){echo ' - '.$NomEtab ;} else {} ; ?></h1>
	</div>
	
	<div id="content">
<br />
		<?php if ($_SESSION['Rang'] == 2) 
		{
				$uti = $bdd->query('SELECT * FROM utilisations WHERE RNE = "'.$_SESSION['RNE'].'"');
			$queryRNE = $bdd->query('SELECT RNE FROM utilisations WHERE RNE = "'.$_SESSION['RNE'].'"'); 
	$count = $queryRNE->rowCount();  
		if($count == 1) 
			{
		while($donnees = $uti->fetch())
		{
	?>
			<input class="btn" type="submit" value="Modifier les données" onclick="self.location.href='modif_uti.php'">&nbsp;&nbsp;<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_uti_ce.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>
	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Données</h4></th><th><h4>Nombre de points</h4></th></tr>

			<tr><th>Taux de réservation des salles informatiques</th><td><?php echo $donnees['taux_reservation_salles_informatiques_mod']; ?></td><td><?php echo $donnees['taux_reservation_salles_informatiques_points']; ?> sur 10</td></tr>

			<tr><th>Taux de réservation des classes mobiles</th><td><?php echo $donnees['taux_reservation_classes_mobiles_mod']; ?></td><td><?php echo $donnees['taux_reservation_classes_mobiles_points']; ?> sur 20</td></tr>

			<tr><th>Proportion d'enseignants utilisant régulièrement des VPI/TNI/TBI</th><td><?php echo $donnees['proportion_enseignants_utilisants_VPI_TNI_TBI_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_utilisants_VPI_TNI_TBI_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de professeurs remplissant le service de notes au moins mensuellement</th><td><?php echo $donnees['proportion_enseignants_rempli_service_notes_mensuellement_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_rempli_service_notes_mensuellement_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de professeurs remplissant le cahier de textes en ligne à l'issue de chaque cours</th><td><?php echo $donnees['proportion_enseignants_remplissant_cahier_texte_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_remplissant_cahier_texte_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de parents d'élèves consultant les services de vie scolaire au moins une fois par semaine</th><td><?php echo $donnees['proportion_parent_consultant_services_vie_scolaire_mod']; ?></td><td><?php echo $donnees['proportion_parent_consultant_services_vie_scolaire_points']; ?> sur 10</td></tr>

			<tr><th>Proportion d'élèves se connectant chaque jour au réseau interne</th><td><?php echo $donnees['proportion_eleve_connexion_reseau_interne_mod']; ?></td><td><?php echo $donnees['proportion_eleve_connexion_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th>Fréquentation du site public de l'établissement</th><td><?php echo $donnees['frequentation_site_public_etablissements_mod']; ?></td><td><?php echo $donnees['frequentation_site_public_etablissements_points']; ?> sur 10</td></tr>

			<tr><th>Équipements numériques utilisés régulièrement par des personnes extérieures à l'établissement</th><td><?php echo $donnees['equipement_numeriques_utilise_regu_personne_exter_mod']; ?></td><td><?php echo $donnees['equipement_numeriques_utilise_regu_personne_exter_points']; ?> sur 5</td></tr>
	</table>

	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 135</td><td><?php echo $donnees['palier_uti'] ; ?> sur 10</td></tr>
	</table>
	

		<?php
		}
			}
			else {
	 	?>

	 	<h2>Aucune donnée rentrée pour cet établissement !</h2>
	 	<input class="btn" type="submit" value="Insérer les données" onclick="self.location.href='insert_uti.php?RNE=<?php echo $_SESSION['RNE'] ; ?> '">

	 	<?php
	 }
		}
	else
	{

if (empty($_GET['RNE']) AND (empty($_POST['RNE'])))
{
?> <h3>Veuillez tout d'abord choisir un établissement sur la page d'accueil</h3> <?php
}
	else
	{
		$RNE2 = $_GET['RNE'];
		$_SESSION['RNE'] = $_GET['RNE'];
		
	$uti = $bdd->query('SELECT * FROM utilisations WHERE RNE = "'.$RNE2.'"');
	$queryRNE = $bdd->query('SELECT RNE FROM utilisations WHERE RNE = "'.$RNE.'"'); 
	$count = $queryRNE->rowCount();  
	if($count == 1) 
		{
		while($donnees = $uti->fetch())
		{
	?>
		<input class="btn" type="submit" value="Modifier les données" onclick="self.location.href='modif_uti.php'">&nbsp;&nbsp;<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_uti.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>
	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Modalité retenue</h4></th><th><h4>Nombre de points</h4></th></tr>

			<tr><th>Taux de réservation des salles informatiques</th><td><?php echo $donnees['taux_reservation_salles_informatiques_mod']; ?></td><td><?php echo $donnees['taux_reservation_salles_informatiques_points']; ?> sur 10</td></tr>

			<tr><th>Taux de réservation des classes mobiles</th><td><?php echo $donnees['taux_reservation_classes_mobiles_mod']; ?></td><td><?php echo $donnees['taux_reservation_classes_mobiles_points']; ?> sur 20</td></tr>

			<tr><th>Proportion d'enseignants utilisant régulièrement des VPI/TNI/TBI</th><td><?php echo $donnees['proportion_enseignants_utilisants_VPI_TNI_TBI_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_utilisants_VPI_TNI_TBI_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de professeurs remplissant le service de notes au moins mensuellement</th><td><?php echo $donnees['proportion_enseignants_rempli_service_notes_mensuellement_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_rempli_service_notes_mensuellement_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de professeurs remplissant le cahier de textes en ligne à l'issue de chaque cours</th><td><?php echo $donnees['proportion_enseignants_remplissant_cahier_texte_mod']; ?></td><td> <?php echo $donnees['proportion_enseignants_remplissant_cahier_texte_points']; ?> sur 20</td></tr>

			<tr><th>Proportion de parents d'élèves consultant les services de vie scolaire au moins une fois par semaine</th><td><?php echo $donnees['proportion_parent_consultant_services_vie_scolaire_mod']; ?></td><td> <?php echo $donnees['proportion_parent_consultant_services_vie_scolaire_points']; ?> sur 10</td></tr>

			<tr><th>Proportion d'élèves se connectant chaque jour au réseau interne</th><td><?php echo $donnees['proportion_eleve_connexion_reseau_interne_mod']; ?></td><td><?php echo $donnees['proportion_eleve_connexion_reseau_interne_points']; ?> sur 20</td></tr>

			<tr><th>Fréquentation du site public de l'établissement</th><td><?php echo $donnees['frequentation_site_public_etablissements_mod']; ?></td><td><?php echo $donnees['frequentation_site_public_etablissements_points']; ?> sur 10</td></tr>

			<tr><th>Équipements numériques utilisés régulièrement par des personnes extérieures à l'établissement</th><td><?php echo $donnees['equipement_numeriques_utilise_regu_personne_exter_mod']; ?></td><td><?php echo $donnees['equipement_numeriques_utilise_regu_personne_exter_points']; ?> sur 5</td></tr>
	</table>

	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 135</td><td><?php echo $donnees['palier_uti'] ; ?> sur 10</td></tr>
	</table>

		<?php
		}
		?>
<?php }
else
{
	?><h2>Aucune notation effectuée sur cet établissement</h2><br>
	<?php
}
}
}
?>
 	</div>
</section>
<?php
	include('pied_de_page.php');
	?>