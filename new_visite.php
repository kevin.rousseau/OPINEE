<?php
	$titre_page = "Nouvelle visite" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');

	if(!empty($_POST['objectif']) and !empty($_POST['constat']) and !empty($_POST['proposition']) and !empty($_POST['mot_cle']))
	{
	
		$date = $_POST['date_visite'] ;
		$objectif = $_POST['objectif'] ;
		$constat = $_POST['constat'] ;
		$proposition = $_POST['proposition'] ;
		$mot = $_POST['mot_cle'] ;

		$requete1 = $bdd->prepare("INSERT INTO visite( RNE, date_visite, objectif, constat, proposition, mot_cle) VALUES ( :RNE, :date_visite, :objectif, :constat, :proposition, :mot_cle)");
		$requete1->execute(array(
			'RNE' => $_SESSION['RNE'],
			'date_visite' => $date,
			'objectif' => $objectif,
			'constat' => $constat,
			'proposition' => $proposition,
			'mot_cle' => $mot
				));	

			
			header("refresh:0;url=visite.php") ;
			?><script>
			alert("Nouvelle visite ajouter");
			</script><?php
	}
	else
	{
		header("refresh:0;url=visite.php") ;
		?><script>
			alert("Veuillez remplir tout les champs");
			</script><?php
	}
?>
