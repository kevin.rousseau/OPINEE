<?php
	$titre_page = "Modif_infra" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	include('pied_de_page.php');
	
	$RNE = $_POST['RNE'];

	if(!empty($_POST['proportion_acces_reseau_salles']) and !empty($_POST['debit_acces_internet']) and !empty($_POST['debit_reseau_interne']) and !empty($_POST['evaluation_qualite_debit_internet']) and !empty($_POST['evaluation_qualite_debit_reseau_interne']) and !empty($_POST['reseau_pedagogique']) and !empty($_POST['proportion_wifi']) and !empty($_POST['proportion_wifi_salles_enseignements']) and !empty($_POST['evaluation_reseau_wifi']) and !empty($_POST['possibilite_recharger_equipements_mobiles']) and !empty($_POST['nb_points_total']) and !empty($_POST['palier_infra']))
	{
	
		$prosalles = $_POST['proportion_acces_reseau_salles'] ;
		$debitinternet = $_POST['debit_acces_internet'] ;
		$debitinterne = $_POST['debit_reseau_interne'] ;
		$eval_internet = $_POST['evaluation_qualite_debit_internet'] ;
		$eval_interne = $_POST['evaluation_qualite_debit_reseau_interne'] ;
		$reseau = $_POST['reseau_pedagogique'] ;
		$prowifi = $_POST['proportion_wifi'] ;
		$prowifisalles = $_POST['proportion_wifi_salles_enseignements'] ;
		$eval_wifi = $_POST['evaluation_reseau_wifi'] ;
		$poss = $_POST['possibilite_recharger_equipements_mobiles'] ;
		$points = $_POST['nb_points_total'] ;
		$palier = $_POST['palier_infra'] ;


		$requete1 = $bdd->prepare('UPDATE infrastructures SET proportion_acces_reseau_salles = :salles, debit_acces_internet = :internet, debit_reseau_interne = :interne, evaluation_qualite_debit_internet = :interneteval, evaluation_qualite_debit_reseau_interne = :interneeval, reseau_pedagogique = :pedareseau, proportion_wifi = :wifipro, proportion_wifi_salles_enseignements = :wifisalles, evaluation_reseau_wifi = :wifieval, possibilite_recharger_equipements_mobiles = :recharge, nb_points_total = :pts, palier_infra = :pale WHERE RNE = "'.$_POST['RNE'].'"');

		$requete1->execute(array(
			'salles' => $prosalles,
			'internet' => $debitinternet,
			'interne' => $debitinterne,
			'interneteval' => $eval_internet,
			'interneeval' => $eval_interne,
			'pedareseau' => $reseau,
			'wifipro' => $prowifi,
			'wifisalles' => $prowifisalles,
			'wifieval' => $eval_wifi,
			'recharge' => $poss,
			'pts' => $points,
			'pale' => $palier
				));	

			header("refresh:0;url=confirm_modif_infra.php?RNE=".$RNE."") ;
	}
	else
	{
		header('refresh:0;url=modif_infra.php') ;
	}
?>
</div>
</section>