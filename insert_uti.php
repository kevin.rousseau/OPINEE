<?php
	$titre_page = "Insert_uti" ;
	include('header.php');
	include('menu.php');
?>

	<section>
	<div id="top_section" >
		<h1>Insertion des données</h1>
		<img src="img/center-header.png" alt="Image du haut" />
	</div>
	
	<div id="content">
	<?php
		if(!empty($msg_error))
		{
			echo '<div id="msg_error_2">'.$msg_error.'</div>' ;
		}

		else
		{

		$RNE = $_SESSION['RNE'];
	 ?>
		<br />

	<table>

		<form method="post" action="insert_uti_php.php?RNE=<?php echo $RNE ; ?>" enctype="multipart/form-data">
		
			<tr><th><h4>Critères</h4></th><th><h4>Données</h4></th></tr>

			<tr><th>Taux de réservation des salles informatiques</th><td>
				<select name="resa">
				<option selected="selected" value="1">Plus de 50%</option>
				<option value="2">Strictement moins de 50%</option>
				<option value="3">Inconnu</option>
				</select>
			</td></tr>

			<tr><th>Taux de réservation des classes mobiles</th><td>
				<select name="resa_mobile">
				<option selected="selected" value="1">Plus de 50%</option>
				<option value="2">Strictement moins de 50%</option>
				<option value="3">Inconnu</option>
				</select>
			</td></tr>

			<tr><th>Proportion d'enseignants utilisant régulièrement des VPI/TNI/TBI</th><td>
				<select name="VPI">
				<option selected="selected" value="1">Plus de 50%</option>
				<option value="2">Strictement moins de 50%</option>
				<option value="3">Inconnue</option>
				</select>
			</td></tr>

			<tr><th>Proportion de professeurs remplissant le service de notes au moins mensuellement</th><td>
				<select name="notes">
				<option selected="selected" value="1">Plus de 50%</option>
				<option value="2">Strictement moins de 50%</option>
				<option value="3">Inconnue</option>
				</select>
			</td></tr>

			<tr><th>Proportion de professeurs remplissant le cahier de textes en ligne à l'issue de chaque cours</th><td>
				<select name="cahier">
				<option selected="selected" value="1">Plus de 50%</option>
				<option value="2">Strictement moins de 50%</option>
				<option value="3">Inconnue</option>
				</select>
			</td></tr>

			<tr><th>Proportion de parents d'élèves consultant les services de vie scolaire au moins une fois par semaine</th><td>
				<select name="services">
				<option selected="selected" value="1">Plus de 50%</option>
				<option value="2">Strictement moins de 50%</option>
				<option value="3">Inconnue</option>
				</select>
			</td></tr>

			<tr><th>Proportion d'élèves se connectant chaque jour au réseau interne de l'établissement</th><td>
				<select name="connexion">
				<option selected="selected" value="1">Plus de 50%</option>
				<option value="2">Strictement moins de 50%</option>
				<option value="3">Inconnue</option>
				</select>
			</td></tr>

			<tr><th>Fréquentation du site public de l'établissement</th><td>
				<select name="freq">
				<option selected="selected" value="1">Régulièrement visité</option>
				<option value="2">Rarement visité</option>
				<option value="3">Pas de site public de l'EPLE</option>
				</select>
			</td></tr>

			<tr><th>Équipements numériques utilisés régulièrement par des personnes extérieures à l'établissement</th><td>
				<select name="equip">
				<option selected="selected" value="1">OUI</option>
				<option value="2">NON</option>
				</select>
			</td></tr>
	</table>

		<input class="btn" type="submit" value="Valider les informations" />
		<input type="hidden" name="RNE" value=<?php echo $RNE ?> />
		</form>
		<?php
 }
 ?>
 	</div>
</section>