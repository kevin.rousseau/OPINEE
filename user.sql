-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 31 Mai 2017 à 10:11
-- Version du serveur :  5.5.46-0+deb8u1
-- Version de PHP :  5.6.17-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `k.rousseau`
--

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `nom` varchar(30) CHARACTER SET utf8 NOT NULL,
  `prenom` varchar(30) CHARACTER SET utf8 NOT NULL,
  `pseudo` varchar(30) CHARACTER SET utf8 NOT NULL,
  `mdp` varchar(30) CHARACTER SET utf8 NOT NULL,
  `IDrang` int(11) NOT NULL,
  `1ere_connec` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`ID`, `nom`, `prenom`, `pseudo`, `mdp`, `IDrang`, `1ere_connec`) VALUES
(1, 'ROUSSEAU', 'kev', 'testadmin', 'new', 1, 1),
(2, 'ROUSSEAU', 'Kevin', 'newadmin', 'testadmin', 1, 0),
(3, 'ROUSSEAU', 'Kevin', 'new', 'testadmin', 1, 0),
(4, 'zefczecf', 'zefc', 'zeqfc', 'eqscf', 1, 1),
(5, 'zv', 'zefze', 'aze', 'ezad', 1, 0),
(6, 'nomCETest', 'prenomCETest', 'test', 'test', 2, 1),
(7, 'TEST', 'Laurent', 'testce', 'test', 2, 1),
(8, 'jean', 'jacques', 'jj', 'jj', 2, 1),
(9, 'ROUSSEAU', 'Kévin', 'testci', 'testci', 4, 1),
(10, 'newci', 'newci', 'newci', 'newci', 4, 0),
(11, 'ROUSSEAU', 'Kévin', 'testdan', 'testdan', 3, 1),
(12, 'newdan', 'newdan', 'newdan', 'newdan', 3, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
