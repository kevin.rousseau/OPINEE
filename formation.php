<?php
	$titre_page = "Formation" ;
	include('header.php');
	include('en_tete.php');
	include('menu.php');
	
	$NomEtab = $_SESSION['NomEtab'];
?>
<section>
	<div id="top_section" >
		<h1>Formation<?php if ($_SESSION['NomEtab'] != NULL){echo ' - '.$NomEtab ;} else {} ; ?></h1>
	</div>
	
	<div id="content">
<br />
		<?php if ($_SESSION['Rang'] == 2) 
		{
				$form = $bdd->query('SELECT * FROM formation WHERE RNE = "'.$_SESSION['RNE'].'"');
			$queryRNE = $bdd->query('SELECT RNE FROM formation WHERE RNE = "'.$_SESSION['RNE'].'"'); 
	$count = $queryRNE->rowCount();  
		if($count == 1) 
			{
		while($donnees = $form->fetch())
		{
	?>
		<input class="btn" type="submit" value="Modifier les données" onclick="self.location.href='modif_form.php'">&nbsp;&nbsp;<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_form_ce.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>
	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Données</h4></th><th><h4>Nombre de points</h4></th></tr>

			<tr><th>Formation au numérique et/ou aux aspects juridiques du numérique d'un ou de plusieurs membres de l'équipe de direction (au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_direction_mod']; ?></td><td><?php echo $donnees['formation_numerique_direction_points']; ?> sur 20</td></tr>

			<tr><th>Formation au numérique d'un ou plusieurs membres du personnel de vie scolaire (au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_vie_scolaire_mod']; ?></td><td><?php echo $donnees['formation_numerique_vie_scolaire_points']; ?> sur 20</td></tr>

			<tr><th>Formation au numérique d'un ou de plusieurs membres du personnel administratif et/ou technique (au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_administration_personnel_technique_mod']; ?></td><td><?php echo $donnees['formation_numerique_administration_personnel_technique_points']; ?> sur 20</td></tr>

			<tr><th>Formation de l'équipe enseignante aux usages pédagogiques du numérique sur site et par un formateur académique (au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_enseignants_mod']; ?></td><td><?php echo $donnees['formation_numerique_enseignants_points']; ?> sur 15</td></tr>

			<tr><th>Animations ponctuelles autour des usages du numérique par un référent numérique (personne ressource) dans l'établissement  (au cours des trois dernières années) :</th><td><?php echo $donnees['animations_ponctuelles_numeriques_mod']; ?></td><td><?php echo $donnees['animations_ponctuelles_numeriques_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de professeurs ayant suivi une formation (PAF, M@gistère) aux usages pédagogiques du numérique (au cours des trois dernières années)</th><td><?php echo $donnees['proportion_enseignants_suivi_formation_numerique_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_suivi_formation_numerique_points']; ?> sur 15</td></tr>

			<tr><th>Actions d'information et de sensibilisation aux usages responsables du numérique et à l'Éducation aux Médias et à l'Information (EMI) à destination des enseignants (au cours des trois dernières années)</th><td><?php echo $donnees['actions_informations_sensibilisations_EMI_enseignants_mod']; ?></td><td><?php echo $donnees['actions_informations_sensibilisations_EMI_enseignants_points']; ?> sur 10</td></tr>

			<tr><th>Proportion d'enseignants mutualisant leurs pratiques du numérique et/ou collaborant via un ou des réseau(x) numériques)</th><td><?php echo $donnees['proportion_enseignants_mutualisant_pratique_numerique_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_mutualisant_pratique_numerique_points']; ?> sur 15</td></tr>

			<tr><th>Identification dans l'établissement d'une personne chargée de la veille et de la curation du numérique à destination des enseignants</th><td><?php echo $donnees['identification_personne_veille_numerique_pour_enseignants_mod']; ?></td><td><?php echo $donnees['identification_personne_veille_numerique_pour_enseignants_points']; ?> sur 15</td></tr>

			<tr><th>Formation des parents aux usages des services numériques</th><td><?php echo $donnees['formation_parent_usages_numeriques_mod']; ?></td><td><?php echo $donnees['formation_parent_usages_numeriques_points']; ?> sur 15</td></tr>

			<tr><th>Formation des parents aux usages responsables des services numériques</th><td><?php echo $donnees['formation_parents_usages_responsables_numeriques_mod']; ?></td><td><?php echo $donnees['formation_parents_usages_responsables_numeriques_points']; ?> sur 15</td></tr>
	</table>
	
	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 175</td><td><?php echo $donnees['palier_form'] ; ?> sur 10</td></tr>
	</table>


		<?php
		}
			}
			else {
	 	?>

	 	<h2>Aucune donnée rentrée pour cet établissement !</h2>
	 	<input class="btn" type="submit" value="Insérer les données" onclick="self.location.href='insert_form.php?RNE=<?php echo $_SESSION['RNE'] ; ?> '">

	 	<?php
	 }
		}
	else
	{

if (empty($_GET['RNE']) AND (empty($_POST['RNE'])))
{
?> <h3>Veuillez tout d'abord choisir un établissement sur la page d'accueil</h3> <?php
}
	else
	{
		$RNE2 = $_GET['RNE'];
		$_SESSION['RNE'] = $_GET['RNE'];
		
	$form = $bdd->query('SELECT * FROM formation WHERE RNE = "'.$RNE2.'"');
	$queryRNE = $bdd->query('SELECT RNE FROM formation WHERE RNE = "'.$RNE.'"'); 
	$count = $queryRNE->rowCount();  
	if($count == 1) 
		{
		while($donnees = $form->fetch())
		{
	?>
		<input class="btn" type="submit" value="Modifier les données" onclick="self.location.href='modif_form.php'">&nbsp;&nbsp;<input class="pdf" type="submit" value="Exportation PDF" onclick="self.location.href='pdf_form.php?RNE=<?php echo $_SESSION['RNE'] ; ?>'"><br><br>
	<table>
			<tr><th><h4>Critères</h4></th><th><h4>Modalité retenue</h4></th><th><h4>Nombre de points</h4></th></tr>
			
			<tr><th>Formation au numérique et/ou aux aspects juridiques du numérique d'un ou de plusieurs membres de l'équipe de direction (au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_direction_mod']; ?></td><td><?php echo $donnees['formation_numerique_direction_points']; ?> sur 20</td></tr>

			<tr><th>Formation au numérique d'un ou plusieurs membres du personnel de vie scolaire (au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_vie_scolaire_mod']; ?></td><td><?php echo $donnees['formation_numerique_vie_scolaire_points']; ?> sur 20</td></tr>

			<tr><th>Formation au numérique d'un ou de plusieurs membres du personnel administratif et/ou technique (au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_administration_personnel_technique_mod']; ?></td><td><?php echo $donnees['formation_numerique_administration_personnel_technique_points']; ?> sur 20</td></tr>

			<tr><th>Formation de l'équipe enseignante aux usages pédagogiques du numérique sur site et par un formateur académique (au cours des trois dernières années)</th><td><?php echo $donnees['formation_numerique_enseignants_mod']; ?></td><td><?php echo $donnees['formation_numerique_enseignants_points']; ?> sur 15</td></tr>

			<tr><th>Animations ponctuelles autour des usages du numérique par un référent numérique (personne ressource) dans l'établissement  (au cours des trois dernières années) :</th><td><?php echo $donnees['animations_ponctuelles_numeriques_mod']; ?></td><td><?php echo $donnees['animations_ponctuelles_numeriques_points']; ?> sur 15</td></tr>

			<tr><th>Proportion de professeurs ayant suivi une formation (PAF, M@gistère) aux usages pédagogiques du numérique (au cours des trois dernières années)</th><td><?php echo $donnees['proportion_enseignants_suivi_formation_numerique_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_suivi_formation_numerique_points']; ?> sur 15</td></tr>

			<tr><th>Actions d'information et de sensibilisation aux usages responsables du numérique et à l'Éducation aux Médias et à l'Information (EMI) à destination des enseignants (au cours des trois dernières années)</th><td><?php echo $donnees['actions_informations_sensibilisations_EMI_enseignants_mod']; ?></td><td><?php echo $donnees['actions_informations_sensibilisations_EMI_enseignants_points']; ?> sur 10</td></tr>

			<tr><th>Proportion d'enseignants mutualisant leurs pratiques du numérique et/ou collaborant via un ou des réseau(x) numériques)</th><td><?php echo $donnees['proportion_enseignants_mutualisant_pratique_numerique_mod']; ?></td><td><?php echo $donnees['proportion_enseignants_mutualisant_pratique_numerique_points']; ?> sur 15</td></tr>

			<tr><th>Identification dans l'établissement d'une personne chargée de la veille et de la curation du numérique à destination des enseignants</th><td><?php echo $donnees['identification_personne_veille_numerique_pour_enseignants_mod']; ?></td><td> <?php echo $donnees['identification_personne_veille_numerique_pour_enseignants_points']; ?> sur 15</td></tr>

			<tr><th>Formation des parents aux usages des services numériques</th><td><?php echo $donnees['formation_parent_usages_numeriques_mod']; ?></td><td><?php echo $donnees['formation_parent_usages_numeriques_points']; ?> sur 15</td></tr>

			<tr><th>Formation des parents aux usages responsables des services numériques</th><td><?php echo $donnees['formation_parents_usages_responsables_numeriques_mod']; ?></td><td><?php echo $donnees['formation_parents_usages_responsables_numeriques_points']; ?> sur 15</td></tr>

	</table>

	<table>
			<th><h4>Nombre de points</h4></th><th><h4>Palier</h4></th></tr>
			<tr><td><?php echo $donnees['nb_points_total']; ?> sur 175</td><td><?php echo $donnees['palier_form'] ; ?> sur 10</td></tr>
	</table>

		<?php
		}
	 }
else
{
	?><h2>Aucune notation effectuée sur cet établissement</h2><br>
	<?php
}
}
	}
?>
 	</div>
</section>
<?php
include('pied_de_page.php');
?>